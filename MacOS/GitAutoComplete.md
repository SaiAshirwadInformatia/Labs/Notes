# Git AutoCompletion in MacOS

In bash in Mac OS X, you can use <kbd>TAB</kbd> to autocomplete file paths. 
Wouldn’t if be nice if you could do the same with git commands and branch names?


You can. Here’s how.


## Download Git Bash script

```
curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash
```

## Add Git Bash script on bash startup

Edit `~/.bash_profile` and append below lines

```
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi
```

## Run in current bash

```
. ~/.bash_profile
```

Now, lets test, type `git ` and space and hit <kbd>TAB</kbd>, you will see 
all git operations supported as below

```
Rohans-MacBook-Pro:KanopiWifiApp rsakhale$ git 
add                  format-patch         remote 
am                   fsck                 repack 
annotate             gc                   replace 
apply                get-tar-commit-id    request-pull 
archive              grep                 reset 
bisect               help                 revert 
blame                imap-send            rm 
branch               init                 send-email 
bundle               instaweb             shortlog 
checkout             interpret-trailers   show 
cherry               log                  show-branch 
cherry-pick          merge                stage 
citool               mergetool            stash 
clean                mv                   status 
clone                name-rev             submodule 
commit               notes                subtree 
config               p4                   svn 
describe             pull                 tag 
diff                 push                 verify-commit 
difftool             rebase               whatchanged 
fetch                reflog               worktree 
filter-branch        relink               
```