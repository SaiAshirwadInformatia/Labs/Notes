# Sync IMAP Mails

Migrating from one email system to another can be challenging and frustrating. Below, are the steps how to move between IMAP systems. The difference between IMAP and POP is that with IMAP all your messages and folders stay on the server and can be accessed from any email program that supports IMAP.

![Thunderbird to Gmail migrate](images/imap-to-google-thunderbird.gif)

Imagine a situation where you would like to migrate to another server and you have tons of emails and you don't know how to do that. The method that we offer is very simple although might take some time depending on the amount of messages you need to move. We describe here how to use Thunderbird - free email application to move your email to a Gmail account.

## Email migration steps

1. Create a Gmail account with Google
1. Enable IMAP in your Gmail account
1. Download and install Thunderbird
1. Add your Gmail account in Thunderbird
1. Add your current IMAP account (that you will be transferring from) in Thunderbird
1. In Thunderbird you will see a folder called "Local Folders" you can copy everything there if you'd like to have a local copy of all your messages and folders. Otherwise, proceed to the next step
1. Make sure you can connect to both accounts and synchronize them with Thunderbird.
1. When the synchronization is finished, complete the following steps:
	1. Select all emails and folders you'd like to migrate
	1. Right click and select Copy to > Gmail Inbox
1. The process will take some time depending on the amount of messages and folders. After it's done you'll see all your messages/folders in your Gmail account.
1. That's it. Start using Gmail.
