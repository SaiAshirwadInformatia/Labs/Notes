# Emails

Electronic mail is a method of exchanging digital messages between people using digital devices such as computers, tablets and mobile phones; email first entered substantial use in the 1960s and by the mid-1970s had taken the form now recognised as email. [Read more](https://en.wikipedia.org/wiki/Email)

Sai Ashirwad Informatia provides authorised [GSuite](http://saiashirwad.pw/google_apps.php) accounts for Rs. 185 / month plus taxes.

Leverage the power of Google's Machine Intelligence to build a smarter team.

## Get Gmail @yourcompany.com
	
Set up Gmail for your domain name - get 30GB of space, ad-free inboxes, easy migration tools and unlimited groups.
Includes spam-protection and backups

## Schedule meetings in a breeze
	
The intelligent Google Calendar auto-suggests the best times and meeting rooms for your team. It also syncs with Gmail, Drive and Hangouts.

## Join Video Calls from Anywhere
	
With the all-new Google Hangouts - there's no need to download or add plug-ins - invite anyone, join from anywhere, share screens even without an account