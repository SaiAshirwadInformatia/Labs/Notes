# Introduction

This is an Open Source Notes Book licensed under MIT by team Sai Ashirwad Informatia to maintain several common activities at one place, give their team a good read on various topics that they work on.

This site is supposed to maintain a good knowledge base not just for Sai Ashirwad Informatia Team, but for everyone.

We are open to contribution on our topics here with simple guideline below.

## Contribute

1. Fork https://gitlab.com/SaiLabs/Notes
1. Contribute your updates in a branch
1. Submit merge request