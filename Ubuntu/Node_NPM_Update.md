# Update Node.JS and NPM

On Ubuntu updating Node.JS and NPM is very easy, fire the commands over terminal and you are done.

## Update Node

```
sudo npm clean cache -f
sudo npm install -g n # install n updating package
sudo n stable # ask n updating package to get node to latest version
```

## Update NPM

```
sudo npm install -g npm # installs latest NPM
```