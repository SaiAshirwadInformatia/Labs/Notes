# Android Wireless Debugging

Below are quick steps to enable your wireless debugging for Android Application Development

## Pre-requisite

1. Stay connected via USB
1. Connect to your WIFI network (computer and mobile device both)
1. Ping DeviceIP (must be have ping to your device)

## Steps to run on terminal

1. run `adb kill-server`
1. run `adb usb`
1. run `adb tcpip 5555`
1. run `adb connect {YourDeviceIP}`
1. run `adb devices (must be see two device names , one of them is by deviceIP)`
1. Unplug USB cable
