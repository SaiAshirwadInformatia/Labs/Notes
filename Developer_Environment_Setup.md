# Developer Environment Setup

## Local Server Environment

Usually a developer should always have a local server environment for prompt 
testing the code locally for robust application development.

When working in any company, always try maintaining your entire thing into one 
single folder known as **Work/Labs** and within that maintain folders as below
- **Resources** - This should contain project folders and every other resource related to the project
- **Tools** - All applications required for development should be installed here
- **Workspace** - All code should reside here like PHP, Java, StaticSites, Android

### _Windows WAMP Setup_ (PHP Development)

#### Steps

1. Download WAMP 64Bit/32Bit - http://www.wampserver.com/en/
1. Download VC Libraries/Binaries required to run wamp application
    - http://www.microsoft.com/en-us/download/details.aspx?id=30679
1. Install wamp in **Labs/Tools** folder
1. Add PHP Path in your Environment Settings
    - Right click on Computer > Advanced System Settings > Environment Variables
    - Create new variable in user variable as "PHP_HOME" and set the path till 
    PHP7 directory
    - Update **PATH** named variable, if not present create new **PATH** variable
    in User variables and give value as `%PATH%;%PHP_HOME%;`

### _Windows Jekyll Setup_ (Static Site Development)

1. [Jekyll](http://jekyllrb.com/) is built on Ruby and hence requires Ruby (2.2.6) installed - https://rubyinstaller.org/downloads/
2. Also download DevKit For use with Ruby 2.0 and above (x64 - 64bits only) and extract in Tools folder
3. This should automatically also add **Ruby** in your path variables, run below commands
    - `gem install bundler`
    - `gem install jekyll`

### _Windows Hexo Setup_ (Static Site Development)

1. [Hexo](https://hexo.io/) is built on Node and hence requires NodeJS preinstalled - https://nodejs.org/en/download/
2. Run command as `npm install hexo-cli -g` now Hexo is available for your Static Site Development

### _Windows GitBook Setup_ (Static Site Development)

1. [GitBook](https://www.gitbook.com/) is built on Node and hence requires NodeJS preinstalled - https://nodejs.org/en/download/
2. [GitBook-CLI](https://github.com/GitbookIO/gitbook-cli) helps to run GitBook on your local machine, install with below command
    - `npm install -g gitbook-cli`

## Development Tools

1. Git for Windows
1. PostMan
1. Grammarly Chrome Extension
1. Mobile/Responsive Web Designer Tester Chrome Extension
1. AdBlock Chrome Extension

## Good to have Tools

1. OpenSEO Stats Chrome Extension
1. BuiltWith Chrome Extension
1. Momentum Chrome Extension