# Install Kloxo-MR

### Pre-install (follow for fresh install)

```bash
cd /

# update centos to latest version
yum update -y
# install some packages like package-cleanup, etc
yum install yum-utils yum-priorities vim-minimal subversion curl zip unzip -y
yum install telnet wget -y

cd /
```

### Install Kloxo-MR

By installing this forked project with your existing Kloxo (6.1.x) no data will be lost, please make sure to run `sh /script/update` first.

#### Follow for Fresh Install of Kloxo-MR7
    
```bash
# move to /
cd /tmp

# remove old rpm
rm -f mratwork*

# install rpm (read Warning)
rpm -ivh https://github.com/mustafaramadhan/kloxo/raw/rpms/release/neutral/noarch/mratwork-release-0.0.1-1.noarch.rpm

# move to /
cd /

# update
yum clean all

yum update mratwork-* -y

yum install kloxomr7 -y

sh /script/upcp
```

#### Follow for updating KloxoMR 6.5 to 7

```bash
yum replace kloxomr --replace-with=kloxomr7 -y
    
# '-y' mean force
sh /script/upcp -y
sh /script/mysql-optimize --select=upgrade
```

#### Follow for updating Kloxo Official 6.1.19 to Kloxo-MR7

Need running 'sh /script/backup-patch' in Kloxo Official (6.1.12) before backup data.

**In Kloxo offical run**

```bash
cd /script
wget https://github.com/mustafaramadhan/kloxo/raw/dev/kloxo/pscript/backup-patch  --no-check-certificate
sh ./backup-patch
```

Need remove certain rpms in Kloxo Official after 6.1.12 (testing in 6.1.19)

**remove packages from Kloxo Official**

```bash
yum remove kloxo* -y
```
    
Since 6.5.0.f-20130701, Kloxo-MR using MySQLi API instead MySQL API. Need update mysql 5.0.x to 5.1.x or above for Centos 5.

**Check mysql version with**

```bash
mysql -V|awk '{print $5}'
```

**Update with**

```bash
yum replace mysql --replace-with=mysql55' (ask to forum for update to MariaDB)
```

Make sure all MySQL databases in MyISAM storage-engine (especially upgrade to version 6.5.0) with execute:

**convert to MyISAM storage-engine**
    
```bash
sh /script/mysql-convert --engine=myisam
```

Trouble with 'rpm -Uvh':

**In CentOS 32bit may trouble with:**

```bash
# install rpm (read Warning)
rpm -Uvh https://github.com/mustafaramadhan/kloxo/raw/rpms/release/neutral/noarch/mratwork-release-0.0.1-1.noarch.rpm
```

**and then use**

```bash
# move to /
cd /tmp

# remove old rpm
rm -f mratwork*

# install rpm (read Warning)
rpm -ivh https://github.com/mustafaramadhan/kloxo/raw/rpms/release/neutral/noarch/mratwork-release-0.0.1-1.noarch.rpm

# move to /
cd /

# update
yum clean all

yum update mratwork-* -y

yum install kloxomr7 -y

sh /script/upcp -y
sh /script/mysql-optimize --select=upgrade
```