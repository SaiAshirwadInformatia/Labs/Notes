# Missing Scripts Folder

This happened to us couple of time during auto update of Kloxo-MR.

Thanks to Mustafa, he promptly guided below as the fix to recover missing or corrupt scripts folder of Kloxo-MR on your server.

## Create the scripts folder symbolic link

```
ln -sf /usr/local/lxlabs/kloxo/pscript /script
```

## If symbolic link fails, re-install Kloxo-MR

Your data is not erased during re-install, so go ahead and try

```
'yum reinstall kloxomr7 -y
```

Source: 
- https://forum.mratwork.com/kloxo-mr-technical-helps/script-folder-is-missing/msg56232/#msg56232
- https://forum.mratwork.com/kloxo-mr-technical-helps/script-folder-is-missing/msg56627/#msg56627
