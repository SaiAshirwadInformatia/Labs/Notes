# Fix Token Mismatch Issue

After reading multiple discussions on forum, it was though to identify the right solution. I have been updating the kloxo panel so many times and running different set of scripts, still that didn't help.

This issue is caused due to bad cookies which is not able to store the token based data on server side.

## Quick Fix

- Open incognito browser
- Login to your Kloxo-MR panel
- Logout
- Open regular browser
- Login to your Kloxo-MR panel

This works perfectly.