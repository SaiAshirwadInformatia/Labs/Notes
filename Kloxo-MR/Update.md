# Update Kloxo-MR

Mustafa Ramadhan actively works on Kloxo-MR development and does continous releases with excellent new features and bug fixes, so make sure you either enable Auto Update or manually update your Kloxo-MR.

## Enable Auto-Update Kloxo-MR7

1. Open your Kloxo-MR panel `http://{IP}:7778`
1. Navigate to **General Settings** icon available at bottom box under **Advanced** category
1. Tick the check box **Auto Update** and save

This will enable Auto Update which runs cron in background to check if any update is available and safely does the updation activity.

## Update Kloxo-MR7 Manually

Run below commands on your server console

```bash
cd /
yum clean all
yum update
sh /script/cleanup
sh /script/restart-all
```