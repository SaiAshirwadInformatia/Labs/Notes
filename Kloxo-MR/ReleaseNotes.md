# Releases Notes


### 7.0.0.b-2016121602.mr (Fri Dec 16 2016)

- add renew cron in fix-cron-ssl
- move ssl renew cron from crontab to /etc/cron.d/letsencrypt_renew and startapi_renew
- always run acme.sh-installer in cleanup
- always remove renew ssl cron from crontsb in acme.sh-installer

### 7.0.0.b-2016121301.mr (Tue Dec 13 2016)

- fix for delete 'main' ftpuser

### 7.0.0.b-2016121201.mr (Mon Dec 12 2016)

- make using '/usr/bin/contab' instead 'contab' in fixcron
- convert '--all--' to '*' om crontab
- select '--all--' automatically if not choose options in complex cron (handle 'can_not_be_null' error)

### 7.0.0.b-2016121101.mr (Sun Dec 11 2016)

- fix setup-afterlogic (change 'setting.xml' to 'setting.xml.php')
- fix etc_suphp.conf.tpl (related to secodary php)
- remove php*.fcgi and then use php.fcgi.tpl inside phpini__synclib.php

### 7.0.0.b-2016121002.mr (Sat Dec 10 2016)

- mod smtp in 'switch programs' (disable/enable include 'send')
- add $sgbl global for handle path in weblib.php
- disable delete php error log in fixlogdir (because using all-in-one error.log)
- fix phpm-installer (using detect phpXYz-cli instead phpXYz; need for php 7.0+)
- fix phpm-installer (related to version detect and exclude nginx dependencies)

### 7.0.0.b-2016120401.mr (Sun Dec 04 2016)

- fix grep logic for dnsnotify (need by djbdns)
- fix encrypt mail (rename tlshosts to tlshosts.old)
- increase '__for_phpm__upload_max_filesize' to '64MB' in php.ini.base
- disable letsencrypt-auto install in cleanup

### 7.0.0.b-2016120101.mr (Thu Dec 01 2016)

- fix listen ssl for nginx (trouble in IPv6)
- mod set.web.lst (related to nginx)

### 7.0.0.b-2016113001.mr (Wed Nov 30 2016)

- disable header logic in apache domains.conf.tpl (because trouble with hiawatha in proxy)
- add ossec log in 'log manager' (need atomic.repo)
- mod lxgpath logic for lxguard
- back to add list in hosts.deny, tcp.smtp and spamdyke blacklist_ip for lxguard

### 7.0.0.b-2016112001.mr (Sun Nov 20 2016)

- fix backup process with include detect unbackup.lst
- move backup temp from /tmp to serverfile

### 7.0.0.b-2016111301.mr (Sun Nov 13 2016)

- fix no_need_token file path
- add 'LimitInternalRecursion 100' to domains.conf.tpl of apache
- set to '/' for blank ftpuser path
- fix double mysqldb icon for client
- fix validate_ipaddress
- add unbackup.lst to list unbackup clients

### 7.0.0.b-2016100902.mr (Sun Oct 09 2016)

- fix domains.conf.tpl for 'ns' (make like 'a')
- increase 'nice' value for webtraffic
- change time for fix-cron-ssl from per-month days to per-week
- move session_start to top of login index.php
- add 'go back to php login' for token_not_match message

### 7.0.0.b-2016100303.mr (Mon Oct 03 2016)

- back to use http/30080 for ssl proxy (because sometimes weird for https/30443)
- fix setInstall for acme.sh and startapi.sh in lib.php
- disable 'performance_schema=on' in server.cnf (to reduce memory usage) in installer.php
- fix static_files_expire_text in nginx
- back to use https/30443 for ssl proxy but disable custom header for apache in proxy
- back to 'http/30080' for nginx (need 'proxy_set_header X-Forwarded-SSL on' for https)

### 7.0.0.b-2016093001.mr (Fri Sep 30 2016)

- fix installer.php (add 'libcurl-devel' install beside 'cult-devel')
- remove change 'releasever' in mratwork.repo in install process
- fix phpm-config-setup (related to zend_extension path)

### 7.0.0.b-2016092901.mr (Thu Sep 29 2016)

- disable open_file_cache in nginx (trouble with add/update file in apps)
- increase limit conn from 25 to 50
- mod restart for spawn-fcgi

### 7.0.0.b-2016092802.mr (Wed Sep 28 2016)

- add 'clear cache' for pagespeed in 'webserver configure'
- add 'clearcache-pagespeed' script
- mod blowfish_secret for phpmyadmin
- mod/fix nginx related to gzip
- mod proxy config in nginx related to pagespeed
- add gzip for lighttpd
- fix lighttpd related to gzip and prettyurls
- fix proxy for nginx related to 'Accept-Encoding'

### 7.0.0.b-2016092701.mr (Tue Sep 27 2016)

- fix create '/root/.ssh' in writeAuthorizedKey function in sshauthorizedkey__synclib.php
- fix download_from_
	603	---------- * functions in lib.php

### 7.0.0.b-2016092502.mr (Sun Sep 25 2016)

- add 'sendmail to ban' feature (still no fix script)
- fix phpm-config-setup (related to zen_extension)
- fix Sendmailban if select file instead dir
- fix data in lxguardhit table
- mod file_put_between_comments to remove 'blank' newlines
- mod block IP only using 'null routing' (without hosts.deny and spamdyke blacklist_ip)

### 7.0.0.b-2016092301.mr (Fri Sep 23 2016)

- fix fcgid2.conf
- fix error_log for nginx (bug if using stats_log.conf)
- fix init.d for spawn-fcgi

### 7.0.0.b-2016092101.mr (Wed Sep 21 2016)

- fix httpry logrotate
- fix syslog logrotate
- fix session_start in login_inc.php
- fix if using 'default' skin (set as 'feather' skin)
- fix 'rpm -qa' logic
- fix web server conf.tpl (related to static_files_expire)

### 7.0.0.b-2016091702.mr (Sat Sep 17 2016)

- mod remove 32bit apps in 64bit OS in installer.php
- move 'token_not_match' handle from inc.php in login path to index.php in lib/php path
- fix kloxo logrotate
- fix phpm-config-setup (related to ioncube/sourceguard installer)

### 7.0.0.b-2016091601.mr (Fri Sep 16 2016)

- add fix-configs-files script
- merge docroot, dirindex and configure_misc (www and https redirect) to webbasics
- fix microcache_insert_into if insert 0 (reset) value
- add pretext for docroot and microcache_insert_into

### 7.0.0.b-2016091501.mr (Thu Sep 15 2016)

- make header and expire set per-domain
- fix logic for set 'header' in domains.conf.tpl
- add 'disable pagespeed' in 'web features'
- fix defaults.conf.tpl in apache (remove 'header' code)
- mod default_index.php (for message for 'token_not_match' and 'blocked')
- mod phpm-config-setup (related to ioncube and sourceguardian binary)
- mod restart to always copy php files in login
- use session instead query for blocked login
- add time remain if blocked login
- fix time to blocked login

### 7.0.0.b-2016091302.mr (Tue Sep 13 2016)

- mod default_index.php (for 'token_not_match' and 'login_error')
- mod cp, webmail and stats always use 'front-end' in web proxy
- fix domains.conf.tpl for nginx and lighttpd (because wrong using commenting)

### 7.0.0.b-2016091201.mr (Mon Sep 12 2016)

- fix session path for kloxo-php-fpm.conf
- mod/fix ioncube-installer
- add sourceguardian-installer
- mod/fix panel login
- mod mmail/mmail__qmaillib.php
- disable 'xinetd restart'
- disable 'fix awstats'

### 7.0.0.b-2016090802.mr (Thu Sep 08 2016)

- fix lxguard (if remove blocked IP)
- set to 'whitelist' also remove IP connection in lxguard
- fix php-fpm.init and php-fpm.init.base (related to PHP_INI_SCAN_DIR)
- fix set.database.lst
- add set.mysql.lst

### 7.0.0.b-2016090702.mr (Wed Sep 07 2016)

- remove database.lst and add set.database.lst
- prepare unbackup certain users (add unbackup.lst)
- add error page for 400 and 502
- mod url for mratwork.com from http:// to // in error pages and default_index
- disable webhandler and webmimetype
- mod validate_prefix_domain (add stats)
- mod validate_server_alias (remove __base__)
- remove stats_dir_protect under 'web for' in hiawatha (because moving to stats.)
- move proxy_temp_path and fastcgi_temp_path from /temp/nginx to /var/cache/nginx
- fix getAndUnzipSkeleton in weblib.php
- prepare fastcgi for 'secondary php' (beside suphp and fcgid; TODO)
- fix defaults.conf.tpl for hiawatha (related to error pages)

### 7.0.0.b-2016090501.mr (Mon Sep 05 2016)

- fix/mod sysinfo script
- fix validate_server_alias (related to wildcards)
- disable 'custom error' (TODO)

### 7.0.0.b-2016090305.mr (Sat Sep 03 2016)

- move 'domain.com/stats' to 'stats.domain.com'
- fix all web configs (related to 'stats' address)
- add 'remark' in cleanup (related to 'stats' address)
- mod 'web selected' text under client
- add setCopyIndexFileToAwstatsDir function in lib.php
- fix installMeTrue in web__lib.php (related to fixweb)
- add 'stats' in 'SAN' for add letsencrypt and startapi ssl
- fix apache and hiawatha (related to startprotext)
- fix hiawatha (related to always use php-fpm for awstats)
- add missing fixdnsaddrecord script
- mod fixssl also update ssl in kloxo database
- mod fixssl also check update_ssl in /etc/cron.d
- add fix-cron-ssl for running ssl in every month
- fix redirect stats in nginx
- fix stats url (related to not use permalink)

### 7.0.0.b-2016090101.mr (Thu Sep 01 2016)

- set no unzip skeleton if index.html exists in document root
- fix shell_access under client (no change setting and just appear)
- fix menu in 'simplicity' (related to 'ftp session')
- add fix-mysql-tmp (prepare to change tmp dir)
- add ioncube-installer (prepare change installing without rpm; still only for 'multiple php')

### 7.0.0.b-2016083001.mr (Tue Aug 30 2016)

- use '127.0.0.1' instead 'mail.domain.com' for domain setting for rainloop
- use mailqueue script instead direct qmHandle
- fix lxbackuplib.php (for delete old backup files)
- mod mailqueue
- mod allow_url_include_flag to off as default
- remove unused php in bin
- add 'advanced php configure' icon/menu
- mod 'web selected' and 'php selected' info in 'web features'
- make possible hostname with _ (underscore) in dns setting/template
- add missing fixftpuserclient.php
- rename fixsimpldocroot.php to fixsimpledocroot.php
- add fixdnsaddrecord/fixdnsaddrecord.php
- add expire in header_base.conf in apache
- fix 'cache_expire' and move from domains.conf.tpl to defaults.conf.tpl in hiawatha

### 7.0.0.b-2016082601.mr (Fri Aug 26 2016)

- add fix-rainloop-domains (add domains ini automatically in rainloop)
- change fix to add/del-rainloop-domains and include in add/del domain
- back to use $obj->was() in fix
- fix phpini_synclib.php related to phps

### 7.0.0.b-2016082402.mr (Wed Aug 24 2016)

- add enablephp logic in domains.conf.tpl in apache
- fix fix-qmail-assign.php related to 'remote mail'
- remove unused global object in lib.php
- fix upload page (disable
	586	---------- * in css)
- mod mem-usage
- add download_from_remote and download_from_scp in lib.php
- back to use previous fix-qmail-assign.php

### 7.0.0.b-2016081802.mr (Thu Aug 18 2016)

- fix 'webmail for parked' in domains.conf.tpl for nginx
- add missing 'webmail for redirected' in domains.conf.tpl for nginx
- add missing 'dirindex' in nginx

### 7.0.0.b-2016081704.mr (Wed Aug 17 2016)

- more accurate add exclude to /etc/yum.conf in cleanup process
- add warning in counter-start.inc if lxphp.exe not exists
- fix add 'exclude=' in /etc/yum.repos (no add if not exits)
- merge cleanup code to cleanup.inc
- move 'urgent' portion from cleanup to fix-urgent
- move detect lxphp.exe from counter-start.inc to cleanup.inc

### 7.0.0.b-2016081602.mr (Tue Aug 16 2016)

- change 'schedule backup' based on scavenge to cron (file inside /etc/cron.d)
- mod db_get_value to possible passing with array args
- add setCronBackup in cleanup
- add execute 'fix-cron-backup' in postUpdate in lxbackup.php
- disable schedulebackup.php (always return null)
- rename backup_main to restore_main in restore.php
- disable 'program_interrupted' stage (still using original 'doing')
- enable hidden 'restore from ftp' (with mod and fix)

### 7.0.0.b-2016081504.mr (Mon Aug 15 2016)

- fix schedulebackup.php
- fix appear in 'index manager'
- make simple for dirindex logic in weblib.php
- mod again for schedulebackup.php related to time logic

### 7.0.0.b-2016081403.mr (Sun Aug 14 2016)

- implementing custom web config with customs dir
- change backup file to 'kloxomr70-scheduled'
- fix mebackup related to remove 'engine'
- add 'time to backup' feature for clients
- use pdns.sql with compatible to 3.4 version
- add 'parked domain' in 'san' of letsencrypt
- make domain able access via 'ip/domainname'
- re-chown '/var/bogofilter' dir
- fix nginx domains.conf.tpl
- reupload hiawatha domains.conf.tpl (because wrong file that using lighttpd)
- mod backup time only possible by admin only
- change default time backup (6 for admin and 18 for clients)
- mod setup.sh/installer.sh related to install mysqlclient

### 7.0.0.b-2016081301.mr (Sat Aug 13 2016)

- fix webserver conf.tpl (especially for nginx)
- add install 'yum-presto' in cleanup

### 7.0.0.b-2016081102.mr (Thu Aug 11 2016)

- change 'usage:' note for backup/restore
- change prefix backup from 'kloxo-scheduled-7.0' to 'kloxomr70-scheduled'
- make slim web server tpl (remove duplicates entries)
- disable 'rotate' in httpry and kloxo logrotate
- add 'php module status' in 'menu' for simplicity skin
- fix error warning for 'webstatisticsprogram', 'extrabasedir' and 'webmail_system_default' if not exists
- remove unused declare in defaults.conf.tpl of hiawatha
- fix fixssl for program ssl (related to broken-link files)

### 7.0.0.b-2016081001.mr (Wed Aug 10 2016)

- add 'chkconfig on' for bind in list.transfered.conf.tpl
- change some icons (especially for start/stop/restart/enable/disable)
- implementing enable/disable php module

### 7.0.0.b-2016080801.mr (Mon Aug 08 2016)

- add update-afterlogic, update-rainloop and update-roundcube (update without 'yum')
- mod sysinfo (add info for stats program)
- change set.mysql.lst to set.database.lst
- delete set.nginx.lst (set.httpd.lst still exists and need for detect apache 2.2 or 2.4)
- mod component list (base on set.list)
- fix domains.conf.tpl for hiawatha (related to indexfile for domain)
- add smtp_relay column in servermail table
- fix sshterm-applet path
- fix weblastvisitlib.php (related to 'Time' exists or not)
- use index.lst as 'default' indexfile for web
- disable chmod for '/var/bogofilter' (because not exists)
- mod phpm-config-setup (more accurate and detect for _unused.nonini)
- prepare for 'php module status' (for active/inactive module)
- mod domains.conf.tpl for hiawatha (stats_dir_for only declare if enablestats)
- fix phpm-config-setup (activate all various mysql modules)

### 7.0.0.b-2016080201.mr (Tue Aug 02 2016)

- fix/mod resource appear (like 'memory_usage')
- mod getGBOrMB to process TB beside MB and GB
- add 'getNumericValue' (related to 'isQuotaGreaterThanOrEq' and 'isQuotaGreaterThan')

### 7.0.0.b-2016080101.mr (Mon Aug 01 2016)

- disable 'request_header_access Proxy deny all' for squid (not work)
- mod/fix image appear in file manager
- mod upload_overwrite_warning message

### 7.0.0.b-2016073101.mr (Sun Jul 31 2016)

- set overwrite always 'on' for upload file
- add redirect (using js) in upload file
- mod installer.php

### 7.0.0.b-2016073003.mr (Sat Jul 30 2016)

- fix microcache value when set to '0' will be change to '5' (as default value)
- implementing 'progress bar' in upload process (need support html5 for browser)
- fix 'upload' (related to implementing 'progress bar)
- change 'overwrite' options to warning in upload file;
- mod js, css and form in upload file
- disable overwrite in upload process
- change warning color

### 7.0.0.b-2016072801.mr (Thu Jul 28 2016)

- change db_schema format from serialize to php array
- fix phpm-config-setup (related php-fpm.ini for phpXYs)
- mod width and height for textarea for file edit
- change back to use microcache_insert_into (especially for hiawatha)

### 7.0.0.b-2016072602.mr (Tue Jul 26 2016)

- increasing burst from 25 to 250 for limit_req in nginx
- mod mod_evasive.conf
- fix servermail (related to spamdyke blacklist_ip and dns_blacklists)
- change 'server mail settings' to 'mail server settings'
- move create log dir for httpry from lib.php to httpry-installer script
- mod httpry-installer related to httpry exists or not (no exists in CentOS 5)
- mod installer.php (related to disable iptables)
- mod disable BanOnMaxReqSize for hiawatha

### 7.0.0.b-2016072301.mr (Sat Jul 23 2016)

- move http_proxy handle from php-fpm*.conf and proxy*.conf to header_base.conf in nginx
- add blocked mechanism in nginx for httpoxy vul
- fix 'include' for header_base.conf for apache

### 7.0.0.b-2016072203.mr (Fri Jul 22 2016)

- add fix for httproxy vul in panel (where using hiawatha)
- mod setInitialServices for webserver (fix in install process)
- fix httproxy vul in squid (possible) and Varnish (no need for ATS)
- fix httproxy vul for reverseproxy in hiawatha-proxy

### 7.0.0.b-2016072106.mr (Thu Jul 21 2016)

- fix webserver related to httpoxy (Vulnerability Note VU#797896)
- fix defaults.conf.tpl of lighttpd (replace to httpoxy vul)

### 7.0.0.b-2016072104.mr (Thu Jul 21 2016)

- fix stats configs; mod httpry.init
- fix lighttpd alias
- move log url to stats_log.conf for nginx and hiawatha
- fix switch for web servers
- move partial code from setAllWebserverInstall to setAllInactivateWebServer and setActivateWebServer
- extract skeleton to httpdocs panel
- fix image urls of error pages
- fix installMeTrue in web__lib.php
- fix wrong var in setActivateWebServer in lib.php

### 7.0.0.b-2016071902.mr (Tue Jul 19 2016)

- mod stats config to customize (in weblib.php)
- add random password for stats in create website
- disable always stats dir protect
- mod httpry.init

### 7.0.0.b-2016071802.mr (Mon Jul 18 2016)

- fix pagespeed.conf (related to deflate)
- add error handler in lighttpd
- fix default_index.php if not panel login page
- fix webserver switch
- fix error pages (related to image links)
- fix activate spawn-fcgi
- fix/optimize webserver switch

### 7.0.0.b-2016071703.mr (Sun Jul 17 2016)

- fix pagespeed.conf for apache
- fix uninstall process (just inactived) for webserver
- add httpry
- add 'rpms' (beside 'backuper') as exclude user backup
- fix select file in restore process
- add httpry in restart-syslog
- add mod_evasive.conf for apache
- add 'httpry' in 'log manager'
- fix restart-list.inc
- mod setInstallHttpry in lib.php

### 7.0.0.b-2016071402.mr (Thu Jul 14 2016)

- mod lighttpd, nginx and hiawatha configs to handle ddos attack
- add startapi.sh log in 'log manager'
- fix all webservers installing
- add 'net.ipv4.tcp_synack_retries' in sysctl.conf in install process
- fix 'php_ini_scan_dir' for 'php branch' in php-fpm.init

### 7.0.0.b-2016071301.mr (Wed Jul 13 2016)

- disable easyinstaller in domain icon
- fix acme.sh-installer and startapi.sh-installer for missing log dir
- disable driver_app declare in fixdns.php

### 7.0.0.b-2016071102.mr (Mon Jul 11 2016)

- move create lighttpd logdir from fixweb to defaults.conf.tpl
- mod/fix sysinfo.php; add 'microcache' in 'web features'
- move 'X-Hiawatha-Cache' from add header in php file to domains.conf
- move microcache declare from globals to domains
- add microcache column in 'web' for kloxo database
- prepare php modules activate
- change lxguard rotate from 3 to 1 month
- add blackhole blocked for lxguard (possible no need hosts.deny and tcp.smtp to blocked)
- add header for 'X-Hiawatha-Cache' in apache (importance for hiawatha-proxy)

### 7.0.0.b-2016070304.mr (Sun Jul 03 2016)

- fix nolog in lib.php
- fix disable_functions in php-fpm (make php-fpm.sh using php-fpm.ini instead php.ini)
- fix php-fpm.ini (related to __extension_dir__)
- add set.ftp.lst
- add ftp in 'component' list
- fix phpm-updater (related to php-fpm.ini)

### 7.0.0.b-2016070204.mr (Sat Jul 02 2016)

- re-enable 'component' button (with more accurate and fast process)
- change 'Component' class from 'Lxdb' to 'Lxclass' extends
- remove 'component' table from kloxo database
- change from 'Component Info' to 'component' for __desc in 'component' class
- fix set.php.lst (remove duplicate)
- add 'type' column for Component list
- make sure no duplicate for component list

### 7.0.0.b-2016070102.mr (Fri Jul 01 2016)

- add 'W' (warning) input form (implementing for StartAPI SSL)
- add setPhpUpdate (update for branch and multiple)
- include setPhpUpdate in cleanup
- mod php.sh.base (change $
	557	---------- * to "$@"; fix '-r' issue)

### 7.0.0.b-2016063002.mr (Thu Jun 30 2016)

- add warning in StartAPI for issue, re-issue and revoke
- fix/mod acme.sh, startapi.sh and letsencrypt-installer

### 7.0.0.b-2016062904.mr (Wed Jun 29 2016)

- add StartAPI SSL (alternative for Let's Encrypt)
- fix stats_awstats.conf for nginx
- fix acme.sh-installer
- mod switch-apache (sync to setAllWebserverInstall in lib.php)
- add missing __desc_upload_v_link in sslcertlib.php
- fix switch-apache (related to rpm detecting)
- fix startapi.sh-account

### 7.0.0.b-2016062804.mr (Tue Jun 28 2016)

- mod phpm-installer (make devel also installing)
- add 'use pagespeed' in 'switch program'
- use 'optimize' .conf for installing pagespeed
- fix setAllWebserverInstall (missing spawn-fcgi and fcgiwrap)
- Fix inactivating webserver appear in setAllWebserverInstall
- fix stats dirprotect (must separated for awstats and webalizer)

### 7.0.0.b-2016062702.mr (Mon Jun 27 2016)

- fix syslog logrotate
- prepare pdns 3.4
- fix nginx listen files
- fix 'web features' appear (related to 'php selected')
- mod redirect.php
- move header_ssl for lighttpd from defaults to domains.conf.tpl

### 7.0.0.b-2016062502.mr (Sat Jun 25 2016)

- prepare easyinstaller (successor of installapp; possible change their name)
- fix/mod fixssl.php; mod sysinfo.php
- move 'Strict-Transport-Security' to header_ssl
- add 'X-Support-By Kloxo-MR 7.0' in header_base
- change max-age from 604800 to 2592000 for 'Strict-Transport-Security'
- add 'php-fpm type' in 'php configure'
- enable 'extrabasedir' for php.ini (espacially for php-fpm conf)
- add info 'login as' before 'click help'
- add blocked IP in spamdyke for smtp (beside in hosts.deny and tcp.smtp)
- disable CSRFToken in create_xml (because double)
- add 'chkconfig httpd on' in setup.sh (because inactive)
- add '--nocron' in acme.sh-installer
- move .db_schema to from '/file' to '/file/sql'
- change header 'X-Support-By' to 'X-Supported-By'
- reduce start, minspare and maxspare of pm in php-fpm

### 7.0.0.b-2016062202.mr (Wed Jun 22 2016)

- mod add-debug and remove-debug; fix debug if commands.php have LF
- change parse_mail_log to parse_smtp_log and mailLogString to smtpLogString for lxguard
- change identify from 'vpopmsil' to 'vchkpw-smtp' for detecting smtp in maillog
- set 'invaliduser' and 'nopassword' as 'fail' (beside original 'fail') for smtp detect in lxguard
- add LF for default ssl files (make sure no '-----END CERTIFICATE----------BEGIN CERTIFICATE-----')
- fix sysinfo related to 'pop3'

### 7.0.0.b-2016062102.mr (Tue Jun 21 2016)

- fix remove 'engine' in mysql dump for backup process
- change from if to switch for zip scripts in linuxfslib.php
- back to add process/detect for tgz till tar.xz (already fix in prevous issue)
- mod run files for qmail able to customize
- fix lxguard for exclude localhost IP
- change '= fail' to '!= success' for count_fail in lxguardincludelib.php

### 7.0.0.b-2016062004.mr (Mon Jun 20 2016)

- prepare change courier-imap and dovecot in 'switch programs'
- disable 'component' button
- fix sslipaddress__synclib.php for .ca file
- change using 'MariaDB' instead 'MariaDB-server' for install process
- mod mailincoming driver (for courier-imap and dovecot; ready to action)
- add mailoutgoing driver (for qmail; ready to action)
- change mailincoming to pop3/imap4 and mailoutgoing to smtp
- disable imap4_driver (make simple to uae pop3_driver for pop3 and imap4)
- mod sysinfo.php (to detect pop3/imap4 and smtp)
- add missing 'authlib' in pop3__courierlib.php
- fix default slavedb driver in installer.php
- fix login in setSyncDrivers for setting driver
- change save_xinetd_qmail to save_control_qmail function
- fix declare of smtp_driver in driver_define.php
- mod message of changeDriverFunc function
- remove slave_save_db process in setWatchdogDefaults
- fix logic in setSyncDrivers and add slave_save_db inside
- make short error message in lx_exception_handler
- mod dprint info for lx_core_lock in lib.php
- fix passing for slave for setSyncDrivers in lib.php
- add add-debug and remove-debug script
- fix remove engine in backup dump file
- disable detect new tgz till tar.xz in lxshell_zip_core (backup process problem)

### 7.0.0.b-2016061901.mr (Sun Jun 19 2016)

- add cache_expire for hiawatha
- fix redirect (like /webmail) for nginx
- prepare for implementing dovecot as courier-imap alternative
- prepare for using pop3 from courier-imap instead qmail-toaster
- add remote mail for lxguard (beside ssh and ftp; not include webmail login)
- prepare change courier-imap and dovecot in 'switch programs'
- disable 'component' button; fix sslipaddress__synclib.php for .ca file
- change using 'MariaDB' instead 'MariaDB-server' for install process

### 7.0.0.b-2016061502.mr (Wed Jun 15 2016)

- mod sysinfo.php
- fix nofixconfig (disabled because not used)
- change webserver model (install all web server together)
- add setAllWebserverInstall function (execute in cleanup or change web server)
- add tgz, tbz2, txz and p7z in lxshell_zip_core function (prepare for zip selected in 'file manager')
- mod kloxo.init (remove for hiawatha init remove)
- mod nsd configs
- remove reload in nsd restart (because include init for restart)
- mod always use useLocalConfig (pure and proxy)

### 7.0.0.b-2016061303.mr (Mon Jun 13 2016)

- make the same dirprotect rule for all stats
- make the same url for all stats
- fix secondary dns in nsd (set restart dns equal to reload and restart)
- fix php-fpm restart (make stop, delete sock files and start)
- add 'notify-retry 5' for slave dns in nsd
- change default ssl with CN as 'Kloxo-MR' and expire until 100 years
- delete program ssl in /file/ssl
- delete fix_self_ssl function

### 7.0.0.b-2016061301.mr (Mon Jun 13 2016)

- add 'secondary php' info in sysinfo.php
- fix '/.well-known' dir (related to create letsencrypt ssl)

### 7.0.0.b-2016061203.mr (Sun Jun 12 2016)

- enable 'Strict-Transport-Security' for ssl with '1 week' age
- mod conf.tpl code for apache
- fix ssl for lighttpd (now running well; using socket instead scheme)

### 7.0.0.b-2016061202.mr (Sun Jun 12 2016)

- fix ssl for apache (no 'SSLCompression Off' for apache 2.2)
- mod SSLCipherSuite for apache (make more secure)
- add checking 'http_v2_module' for nginx (because no exists in nginx for CentOS 5)
- fix/mod file_exists logic in webserver configs

### 7.0.0.b-2016061105.mr (Sat Jun 11 2016)

- move fixweb and restart-web process from ssl .sh to sslcertlib.php
- reconstruct 'default' ssl file (using root ca from cacert.org)
- fix ssl for apache (using secure cipher)
- move static ssl text to ssl_base.conf for apache
- fix copy spawn-fcgi to sysconfig dir
- fix copy webserver init in web__lib.php
- mod defaults.conf.tpl and domains.conf.tpl for lighttpd (ssl still not work)
- add missing httpd24.init
- mod/fix lighttpd globals confs
- add 'rename' in 'tab' of 'file manager'
- change 'cron scheduled task' to 'cron task'
- add 'all clients' in 'tab' of 'all'
- add missing .ca and .csr of default and program ssl

### 7.0.0.b-2016061002.mr (Fri Jun 10 2016)

- fix update process for acme.sh-installer and letsencrypt-installer
- add 'column' for alter in db-structure-update.sql
- split permissions and ownership page in 'file manager'
- fix acme.sh-installer
- mod installer and remover for acme.sh and letsencrypt

### 7.0.0.b-2016060903.mr (Thu Jun 09 2016)

- fix sysinfo.php (related to spam apps)
- fix return page for 'remove' file in 'file manager'
- mod zip extract (different action for .tar.gz and gz; also for bz2 and xz)
- extract tar.gz with options extract to tar (also for bz2 and xz)
- change different name format for NewArchive (action from ZIP in 'file manager')
- rename coreFfilelib.php to coreffilelib.php
- action as 'insert into' instead 'update' in db_set_value if data not exists
- add delay 10 for stop/reload of php-fpm and phpm-fpm; add '\n' in merge ssl files to .pem in fixssl.php
- fix php_selected convert

### 7.0.0.b-2016060703.mr (Tue Jun 07 2016)

- better 'web features' appear (maybe change if implementing 'multiple webserver')
- back to enable setWebserverInstall and always copy init for webserver
- fix acme.sh-installer (related remove cron created by acme.sh)
- add acme.sh-remover

### 7.0.0.b-2016060603.mr (Mon Jun 06 2016)

- fix fixssl.php (ssl file must content '-----BEGIN' to next process)
- use cp instead ln in acme.sh.tpl and letsencrypt.sh.tpl
- list letsencrypt for acme.sh and certbot in 'log manager'
- mod sslcertlib.php (make acme.sh as priority if installed instead letsencrypt-auto)
- implementing callWithSudo (thanks smierke)
- fix acme.sh-installer and letsencrypt-installer
- fix acme.sh.tpl (use 'bogus_command' trick for return value)
- use copy ssl files instead symlink for acme.sh and certbot
- remove unused code; deleteSpecific always delete old ssl files for acme.sh and certbot together
- fix $extrabasedir return value in web__lib.php
- also copy dile config for acme.sh in /opt/configs
- disable sudo code in remotelib.php
- fix acme.sh.tpl (related to error message; include 'skip renew')

### 7.0.0.b-2016060505.mr (Sun Jun 05 2016)

- fix djbdns dir (no move from /home to /opt/configs)
- fix httpd.init
- disable setWebserverInstall (for custom web init)
- change webselector to webfeatures var
- fix setup.sh and installer.sh related to MariaDB installing
- fix acme.sh-installer (certain trouble in CentOS 5)
- mod/fix sysinfo.php
- fix sysinfo.php (fix 'php used' logic)
- change Alias/ScriptAlias to AliasMatch/ScriptAliasMatch in domains.conf.tpl for apache

### 7.0.0.b-2016060408.mr (Sat Jun 04 2016)

- mod fixssl (not fix program ssl if symlink)
- mod sslcertlib.php (set as symlink for program ssl if taken from domain ssl)
- fix/mod fixssl.php and sslcertlib.php (make simple logic using array; disable unused var)
- mod fixssl (also recreate ipaddress ssl)
- mod fixssl.php (no process for ipaddress ssl if symlink; like program ssl)
- fix fixlxphpexe (wrong logic for php target)
- fix phpm-fpm.init (if '/opt/configs/php-fpm/php*m' no exist)
- mod phpini__synclib.php (disable unused var)
- fix getMultiplePhpList (return blank array if empty)
- fix lloglib.php (related to letsencrypt log)
- fix pserverlib.php (disable no_fix_config)

### 7.0.0.b-2016060305.mr (Fri Jun 03 2016)

- fix djbdns conf.tpl and init
- mod fixssl (add fix for domains based on data from kloxo database)
- fix sslcertlib.php in deleteSpecific
- add fix.lst (make customize fix process list in cleanup)
- change getRpmBranchListOnList to getListOnList

### 7.0.0.b-2016060302.mr (Fri Jun 03 2016)

- fix fixlxphpexe (must input with prefix 'php')
- mod restart.inc (related to php-fpm)
- add help for set-kloxo-php; fix 'multiple php' install and remove list
- mod defaults.conf.tpl for nginx
- disable reset to fpm mode if running cleanup
- fix sslcertlib.php (related to deleteSpecific)
- fix acme.sh.tpl and letsencrypt.sh.tpl (related to run fixweb)

### 7.0.0.b-2016060105.mr (Wed Jun 01 2016)

- set no 'reinstall' letsencrypt-auto in cleanup
- make always copy phpm-fpm in fixphp
- make possible customize for 'spawn-fcgi' in defaults.conf.tpl of nginx
- fix phpm-fpm for fresh install (no domain created)
- add 'chown 755' for phpm-fpm
- use sed to change 'exclude=mysql51*' to 'exclude=mysql5*' for install process
- add fix 'exclude=mysql51*' to 'exclude=mysql5*' under IUS repo for mratwork.repo in cleanup script
- fix letsencrypt-installer
- add restart-web in postUpdate in weblib.php

### 7.0.0.b-2016054002.mr (Mon May 30 2016)

- fix/mod phpm-fpm.init (enable 'multiple php' but no install phpXYm; php52m service start)
- fix domains.conf.tpl for nginx (related to stats)
- set timeout together fastcgi and proxy in nginx conf (no need fixweb for switch between them)
- mod phpm-fpm.init

### 7.0.0.b-2016052907.mr (Sun May 29 2016)

- fix wrong delete conf for php-fpm if client deleted
- mod restart.inc (stop unwanted services)
- fix client__synclib.php related to delete php-fpm user
- mod nginx (can handle cgi via spawn-fcgi + fcgiwrap; but awstats still not work)
- fix nginx for proxy location

### 7.0.0.b-2016052903.mr (Sun May 29 2016)

- mod postUpdate function in phpinilib.php to use set-php-fpm
- delete ssl also delete file inside /etc/letsencrypt
- mod getInitialPhpFpmConfig to execute set-php-fpm also use_phpXY.flg
- mod acme.sh-installer and letsencrypt-installer to add '-O master.zip' in wget process
- mod set-php-fpm to use add-restart-queue instead restart-php-fpm
- add start-php-fpm and stop-php-fpm script
- make simple logic for install php for 'multiple php'
- add feature for remove php for 'multiple php'
- change to use suphp for handle cgi (because no need cgi module)
- mod nginx proxy conf for only passing to apache only for php, pl, py, rb and cgi
- fix phpini__synclib.php (mkdir for 'php/php-fpm.d' if not exists also in client level)
- change 'perm' to permission', 'ren' to 'rename' and 'dn' to download in 'file manager'
- make clickable for 'owner' with as the same as 'permissions' link
- add 'spawn-fcgi' in 'restart-list.inc'
- use suphp instead using cgi module for execute cgi (like perl in awstats) in apache
- fix awstats in hiawatha and stats dirprotect
- mod lightpd php-fpm and awstats conf
- fix set for hiawatha webalizer directory
- mod fixweb tp always overwrite httpd.conf in /etc/httpd/conf

### 7.0.0.b-2016052507.mr (Wed May 25 2016)

- add fix and restart web in acme.sh.tpl and letsencrypt.sh.tpl
- mod php-fpm.init.base and php-fpm.init.base
- mod nginx related to ssl
- mod ssl_base.conf for nginx (add missing 'ssl on;')
- add missing add-restart-queue script

### 7.0.0.b-2016052503.mr (Wed May 25 2016)

- separated php-fpm and phpm-fpm init in multiple php-fpm
- fix execute getInitialPhpFpmConfig if phpm-fpm not exists in /etc/init.d
- mod php-fpm.init.base
- mod restart.inc
- mod set-php-fpm
- always copy phpm-fpm.init to /etc/rc.d/init.d in cleanup process

### 7.0.0.b-2016052501.mr (Tue May 24 2016)

- add missing 'listen.owner' and 'listen.group' in 'default' pool of php-fpm

### 7.0.0.b-2016052409.mr (Tue May 24 2016)

- change setInitialPhpFpmConfig to getInitialPhpFpmConfig (because return active 'php-fpm' value)
- fix php_used logic with using getInitialPhpFpmConfig
- change php-fpm.init.base from customize to basic code
- set phpm-installer always execute phpm-config-setup
- fix set-php-fpm for php_target; add 'phpm-fpm' in 'services' list
- set no execute set-php-fpm if flag file exists for 'php used'
- add change flag for set-php-fpm
- fix merge files in acme.sh.tpl and letsencrypt.sh.tpl (bash bug?)
- fix getInitialPhpFpmConfig logic (related to active php detect)
- fix getInitialPhpFpmConfig again (related to 'custom_name')

### 7.0.0.b-2016052404.mr (Tue May 24 2016)

- enable php52m in 'multiple php'
- set to 1/3 of maxchildren value for php52m (because only 'static' instead 'dynamic'/'ondemand')
- convert 'php branch' to 'php' in getPhpSelected
- use 'php' as default and no impact if enable 'multiple php' for set-php-fpm
- enable 'php52m' option in 'php selector'
- fix set-php-fpm (reduce double logic related to 'multiple php')
- fix restart-list.inc (missing phpm-fpm in list_services and list_all)
- fix setInitialPhpFpmConfig logic
- mod setInitialPhpFpmConfig for phpXYm
- add logic for php-fpm in restart.inc

### 7.0.0.b-2016052303.mr (Mon May 23 2016)

- implementing 'multiple php' for php-fpm (except for php52m because still trouble)
- 'multiple php' using 'phpm-fpm' init instead 'php-fpm' init
- make 'return' for all function commandlinelib_old.php
- move pool configs for 'php branch' from /etc/php-fpm.d to /opt/configs/php-fpm/conf/php/php-fpm.d
- fix fixphp related to php-fpm
- add missing php<52/53>-fpm-default.conf.tpl
- fix phpm-fpm (for disable php52m)
- add missing 'php in list of all php in phpm-fpm

### 7.0.0.b-2016052206.mr (Sun May 22 2016)

- set http/2 for nginx
- fix getCleanRpmBranchListOnList function (for the same value)
- fix os_set_quota
- add init_set for memory_limit for sqlitelib.php
- fix letsencrypt-installer for set cron
- fix ~lxcenter.conf.tpl
- fix letsencrypt-installer
- upload missing letsencrypt-cron.sh
- mod acme-cron.sh
- disable install acme.sh
- fix copy letsencrypt config
- fix defaults.conf.tpl for nginx (related switch_*.conf)
- mod phpm-config-setup

### 7.0.0.b-2016052103.mr (Sat May 21 2016)

- use letsencrypt-auto instead acme.sh (bug?)
- use '--webroot' for letsencrypt
- add http/2 feature for apache
- add merge and symlink info to log for letsencrypt
- mod pure-ftpd.init
- install acme.sh and letsencrypt-auto together
- prepare 'keep-alive' setting for apache
- fix install_if_package_not_exist function
- fix changeport.php
- fix letsencrypt-installer script

### 7.0.0.b-2016052101.mr (Sat May 21 2016)

- mod install_if_package_not_exist
- fix acme-cron.sh
- mod acme.sh.tpl
- fix/mod acme.sh-installer (related to add cron)
- mod fix-yum; acme.sh.tpl also create symlink to /home/kloxo/ssl
- remove unwanted code in phpinilib.php
- mod 'memory_limit' to '-1' in kloxo-php-fpm.conf and php.ini.bsse
- patch for HTTPPort from '80' to '60080' in acme.sh (because using 'standalone' instead 'webroot')
- fix fixssl (also handle letsencrypt ssl)
- fix acme.sh.tpl (related to symlink)
- prepare phpini__synclib.php (related to 'multiple php-fpm')
- change 'web / php selector' to 'web options'
- add 'timeout' beside 'web selector' and 'php selector' (prepared)
- change default index files sort
- change 'microcache' time from 10s to 5s
- fix acme.sh.tpl (related to error exit)
- fix domains.conf.tpl for hiawatha
- fix 'fastcgi pass' for '/__kloxo' in lighttpd
- fix passing timeout value (bug?) in nginx
- fix include declare in domains.conf.tpl for lighttpd

### 7.0.0.b-2016051801.mr (Wed May 18 2016)

- fix kloxo logrotate
- fix acme.sh.tpl (log for error)
- fix install_if_package_not_exist function in lib.php
- re-enable setCheckPackages in cleanup
- add lxjailshell install in installer.php

### 7.0.0.b-2016051704.mr (Tue May 17 2016)

- fix acme.sh-installer (for CentOS 5)
- fix/mod restart process
- add ocsp stapling for apache and nginx
- change '--Use PHP Branch--' to '--PHP Branch--' in serverweb
- prepare for add 'timeout' for web
- change ssl ready for client (beside admin and domain)
- fix menu (related to ssl); prepare spam.lst
- mod packer.sh
- disable ssl stapling (not work especially in Apache)
- fix php-fpm restart process

### 7.0.0.b-2016051401.mr (Sat May 14 2016)

- fix acme.sh-installer (related to /root/.acme.sh dir and cron job)
- fix data issue for sslcert (importance for ssl update feature)
- fix/mod nomodify appear
- fix ssl for text and file upload process

### 7.0.0.b-2016051305.mr (Fri May 13 2016)

- merge acme-cron.sh and acme-pem.sh to acme-cron.sh
- use /usr/bin/acme.sh (need symlink) instead /root/.acme.sh/acme.sh
- remove letsencrypt-auto if exists
- fix/mod acme.sh-installer
- merge acme.sh-installer and acme.sh-setting to acme.sh-installer
- fix/mod fixsslpath

### 7.0.0.b-2016051303.mr (Fri May 13 2016)

- change ssl path; disable pkill in hiawatha restart process
- disable Strict-Transport-Security header (trouble if other apps not use valid ssl)
- add username in sslcert table
- ssl in admin only for 'self assign' and in website for 'upload', 'letsencrypt' and 'link'
- adjustment for list appear for ftpuser, mailforward, mailinglist dan ddatabase
- fix .pem (key+crt instead crt+key)
- change error appear from text input to textarea
- mod warning if not FQDN qualified in install process
- using acme.sh instead letsencrypt-auto for letsencrypt ssl
- add missing acme.sh-installer
- change letsecrypt log
- fix change from letsencrypt-auto to acme.sh
- split acme.sh-installer to acme.sh-installer and acme.sh-setting
- fix acme.sh.tpl
- add missing fixsslpath script (move to new ssl files path)
- add letsencrypt-remover (because using acme.sh)
- fix acme.sh-installer
- fix acme.sh-setting
- fix fixsslpath
- fix letsencrypt-installer

### 7.0.0.b-2016050403.mr (Wed May 04 2016)

- fix MaxRequestSize for hiawatha (2047 x 1024)
- fix sslcertlib updateform
- prepare 'all' list for ssl
- mod/fix letsencrypt-installer

### 7.0.0.b-2016050402.mr (Wed May 04 2016)

- add 'Access-Control-Allow-Origin:*' header in webserver configs
- fix mysql-optimize.php for 'error message'
- mod description for 'CSR' of 'self assign ssl'
- remove 'Copy of' gif
- mod/fix 'MaxRequestSize' for hiawatha
- mod mem-usage script
- fix fetchmailticket.sh
- fix cleanspamdyke.php (change xinetd to ftp)
- disable cgi module in apache (security reason)
- add/fix watchdog for ftp
- fix wddx config for php 5.4
- fix restart-services
- mod letsencrypt.sh.tpl
- ready for letsencrypt ssl
- mod/fix nomodify appear
- fix letsencrypt-installer
- enable header_base if .ca file exists
- disable RequiredCA in hiawatha (trouble for https access)
- combine .key, .crt and .ca to .pem
- fix header_base.conf format
- ready for 'link' ssl
- fix header_base.conf for nginx

### 7.0.0.b-2016040705.mr (Thu Apr 07 2016)

- add webserver 'header' to make more secure website
- make webserver include file use 'customize rule'
- fix acme-challenge include logic in apache
- fix add header in lighttpd
- move add header to https portion in webserver
- fix header_base.conf for webserver
- fix db-structure-update.sql
- fix lighttpd conf.tpl
- fix freshclam restart
- fix/rename headerbase to header_base param in webserver tpl
- fix db-structure-update.sql (related to frontpage)
- fix freshclam inactivated
- disable 'Strict-Transport-Security' header

### 7.0.0.b-2016040502.mr (Tue Apr 05 2016)

- fix symlink for chkconfig in cleanup
- make maximize MaxUploadSize and MaxRequestSize for hiawatha
- disable domain related for icons under customer (the same way for admin and reseller)
- disable cgi module for apache
- change x-httpd-php link for suphp
- fix run for smtp-ssl
- fix db-structure-update.sql (for backward compatibility)
- mod sslcertlib.php
- fix setInitialServer
- mod nomodify display
- fix warning for phpm-installer
- fix ~lxcenter.conf for lighttpd (no need errorloghack for latest version
- mod defaults.conf.tpl and domains.conf.tpl for lighttpd (use http-scheme instead server-socket for ssl)
- add validate for 'my name' in 'server mail settings'
- fix secure setup-rainloop.php

### 7.0.0.b-2016032701.mr (Sun Mar 27 2016)

- make sure restart mean 'restart -y' in lxserverlib
- update process make restart as stop, pkill and start
- mod restart processes (remove start if pid not exists)
- fix syncserver in getUserList() at web__lib.php
- make stop upcp if hoatname not FQDN qualified
- make symlink for chkconfig to /usr/bin
- fix redirect in nginx
- fix display in 'php used' if phpXYm not installed
- add symlink for chkconfig in cleanup (fix for running chkconfig in cron)
- fix redirect cp, webmail and kloxo in hiawatha and nginx
- always force install php54s in install process
- fix 'not_ready_to_use' for 'enable apache 2.4'
- disable 'no_fix_config'
- change application.ini to application.ini.php and default admin password for rainloop
- mod set.php.lst; mod mysql restart process
- mod php-fpm.conf.tpl (for php 5.2)
- mod db-structure-update.sql
- mod sslcertlib.php
- mod phpm-config-setup (related to mysql modules)
- mod mailqueue script
- fix for select 'php-fpm' for 'php type'
- remove handling perl module in apache
- fix MaxRequestSize for hiawatha

### 7.0.0.b-2016030202.mr (Wed Mar 02 2016)

- mod sysinfo.php
- add rar and 7z extract in 'file manager'
- make root able extract to current dir; fix set-kloxo-php
- add install rar and 7z in installer.php
- add-zips script for add additional compression apps

### 7.0.0.b-2016022601.mr (Fri Feb 26 2016)

- fix uninstallMeTrue for httpd
- fix 'log manager' path in 'address'

### 7.0.0.b-2016022403.mr (Wed Feb 24 2016)

- remove 'COLUMN' and set NULL value for ALTER for db-structure-update.sql
- mod sysinfo (prepare for multiple web server)
- fix defaults.conf.tpl if httpd 2.4 not installed

### 7.0.0.b-2016022302.mr (Tue Feb 23 2016)

- fix fix-qmail-assign.php (account prefix as the same as domain prefix)
- fix phpm-installer (change priority to phpXYu, phpXY and then phpXYw)
- add z-memcached in phpm-config-setup

### 7.0.0.b-2016022202.mr (Mon Feb 22 2016)

- fix shexec and move to sbin in kloxo path
- change 127.0.0.1 to localhost for proxy_fcgi in apache
- change absolute path to relative for kloxo path
- mod 'php type' list with depend on apache version
- mod shexec function
- fix to copy for .fcgi file
- mod 'rule' for enable 'web / php selector'
- add set-kloxo-php in cleanup (that mean use php-fpm until implementing shexec)
- add proxy_fcgi for 'php type' in httpd 2.4
- fix db-structure-update.sql
- fix defaults.conf.tpl related to httpd 2.4 mpm
- add fixskeleton in cleanup
- mod note for raw-restore
- mod help info in fix.inc

### 7.0.0.b-2016021403.mr (Sun Feb 14 2016)

- mod/fix apache configs (relate to mod24u modules)
- use httpd.init from apache 2.4
- fix hiawatha.conf
- prepare for use hiawatha-monitor
- add param for gzip of nginx
- add 'multiple_php_already_installed' in phpinilib.php
- fix 'switch programs' (related to apache24)
- fix defaults.conf.tpl for detect httpd24
- fix pserver/pserverlib.php related to web server
- mod/add mod24u_fastcgi in installMeTrue in apache
- mod apache for use the same 'php type' modules (disable proxy_fcgi in apache 2.4)

### 7.0.0.b-2016021202.mr (Fri Feb 12 2016)

- fix menu for 'all_mailinglist'
- change MinSSLversion to MinTLSversion (related to hiawatha 10.1)
- change RequireSSL to RequireTLS (related to hiawatha 10.1)

### 7.0.0.b-2016021005.mr (Wed Feb 10 2016)

- fix phpini related to add/mod .htaccess
- fix phpini related to user detect
- mod/fix pure-ftpd if running cleanup and fixftp
- fix list in 'multiple php already installed'
- fix acme-challenge.conf include position in apache

### 7.0.0.b-2016020801.mr (Mon Feb 08 2016)

- change hiawatha reload to restart in process
- mod letsencrypt.sh.tpl
- fix lighttpd 'default' ssl
- fix phpm-all-install and phpm-all-setup
- mod restart-list.inc (add perl-fastcgi)

### 7.0.0.b-2016020503.mr (Fri Feb 05 2016)

- fix syslog logrotate
- fix acme-challenge.conf in nginx
- fix sslcert for self-assign
- add replace_to_space function
- change install mratwork repo from 'wget' to 'rpm -Uvh'
- add install-hiawatha-addons script
- prepare to use perl-fastcgi
- fix acme-challenge path
- change webroot-path of letsencrypt
- fix acme-challenge alias
- add letsencrypt.sh.tpl
- mod openssl.sh for domain
- fix throw in sslcertlib.php

### 7.0.0.b-2016020101.mr (Mon Feb 01 2016)

- make possible spamdyke use custom spamdyke.conf template
- fix defaults.conf.tpl for nginx related to chown log and temp dir
- mod hiawatha related to reverseproxy (fix awstats issue)

### 7.0.0.b-2016013001.mr (Sat Jan 30 2016)

- remove double location declare for nginx
- copy pbp-fpm_standard*.conf to php-fpm_wildcards*.conf for nginx
- back to enable the same hostname for multiple ip for 'A record'
- fix warning for phpm-config-setup and phpm-installer
- fix stats dir protect in hiawatha
- back to disable proxy_cache_use_stale in nginx-proxy

### 7.0.0.b-2016012701.mr (Wed Jan 27 2016)

- add php70m for suphp in 'secondary php'
- make simple fix-chownchmod
- mod how-to-install.txt (use 'rpm -Uvh' instead download and 'rpm -ivh' for mratwork rpm)
- fix phpm-all-install and phpm-all-setup for --help
- make branchlist for custom possibility

### 7.0.0.b-2016012103.mr (Thu Jan 21 2016)

- cleanup domains.conf.tpl for apache
- move gzip from lxcenter.conf to gzip.conf in globals for nginx
- change nginx user from nginx to apache
- prepare for tengine config
- prepare for pagespeed plugins in tengine
- fix page error for nginx
- fix installatron-install
- cleanup mem-usage
- hidden chown process in defaults.conf.tpl for nginx
- fix/mod fixweb.php
- fix chown in defaults.conf.tpl for nginx

### 7.0.0.b-2016011904.mr (Tue Jan 19 2016)

- disable detect 'uname -m' in fixphpini.php because already exists in phpinilib.php
- mod sysinfo script
- add 'enable ssl/tls' in pure-ftpd
- change lxshell_return to exec for chkconfig
- change 'php configure' for domain to 'web selector' but 'php selected still disable
- remove 'multiple php ratio'
- make simple rpm add/remove for apache
- change 'blocked ip' and 'allowed ip' to 'blocked login' and 'allowed login'
- remove '--nolog' in fix scripts
- add 'validate_filename'
- use sh script instead rpm for installatron-install
- mod phpm-config-setup for copy www.conf
- add php53-fpm-global.conf.tpl
- fix defaults.conf.tpl for hiawatha related to phpselected
- fix sysinfo.php
- fix/set default value for webselected and phpselected

### 7.0.0.b-2016011401.mr (Thu Jan 14 2016)

- fix fix-chownchmod related to apache:apache
- add openssl.sh.tpl (prepare create cert based on sh instead php)

### 7.0.0.b-2016011301.mr (Wed Jan 13 2016)

- fix install/reinstall httpd24
- fix httpd.init

### 7.0.0.b-2016011202.mr (Tue Jan 12 2016)

- move basic ssl part to ssl_base.conf in nginx
- change 'listen default' to 'listen default_server' in nginx
- change nginx ssl_ciphers in ssl_base.conf
- fix paths in nginx config templates

### 7.0.0.b-2016011106.mr (Mon Jan 11 2016)

- fix php-fpm.init.base related to 'default' php
- fix/mod 'php configure' for web related to 'php selected'
- fix nginx configure related to phpselected
- fix default.conf for php-fpm
- fix php-fpm.init.base
- fix fixweb if httpd not installed
- fix nginx configs related to missing var_phpselected for 'web for'
- fix nginx configs related to php-fpm ssl
- fix detect httpd version/type
- fix detect phpselected

### 7.0.0.b-2016011101.mr (Mon Jan 11 2016)

- fix make-slave script
- add 'acme-challenge' for web configs (prepare for letsencrypt)
- use port 30443 in web configs if web access using https
- message info to warning set IP assign to domain if server only have 1 IP
- set fix-chownchmod to except apache:apache ownership
- prepare for multiple php for use different pid and using special init (phpm-fpm.init)
- use new function for 'multiple php' list
- prepare php53-fpm-pool.conf.tpl for 'multiple php'
- change 'common name' entry type in create self-assign ssl
- fix webconfig related to phpselected
- fix switch programs (related to init)

### 7.0.0.b-2016010501.mr (Tue Jan 05 2016)

- add https redirect beside www redirect
- fix initial pure-ftpd config

### 7.0.0.b-2016010404.mr (Mon Jan 04 2016)

- add php70 for apache setHandler
- change php_rc from dir to direct to php.ini file path
- change use initd instead xinetd for pure-ftpd (prepare for CentOS 7)
- add pure-ftpd in service list
- set Quiet and ReallyQuiet as yes for webalizer
- add 'reverse' to 'make-slave' script
- fix inform and set 'chmod 755' for pure-ftpd init
- change disablephp to reverse enable php in domains.conf.tpl for apache
- fix 'bind' in pure-ftpd.conf

### 7.0.0.b-2016010201.mr (Sat Jan 02 2016)

- no list for ruid2 and itk if php (only php-cli) rpm not install in php-branch
- fix/add extrabasedir in php-fpm
- prepare for letsencrypt
- include install net-tools (ifconfig app) because not include in CentOS 7 (still in progress)
- add getdriver script

### 7.0.0.b-2015122501.mr (Fri Dec 25 2015)

- change 'port configure' to 'port and redirect configure'
- add possible redirect panel to certain domain/ip

### 7.0.0.b-2015122202.mr (Tue Dec 22 2015)

- possible install php70 (need enable webtatic repo)
- implementing maldetect (installer and fix 'log manager')

### 7.0.0.b-2015122102.mr (Mon Dec 21 2015)

- prepare to 'ssl link' for web
- fix ssl for hiawatha and kloxo-hiawatha
- fix default.pem and program.pem
- fix htmllib to display help link
- translate for information list text
- mod sysinfo (need '-y' to run 'fix-service-list')
- add self rootCA
- add letsencrypt-installer and cli.ini.tpl (implementation still in progress)
- add tldextract.php
- set 'secondary php' for fcgid only under proxy because trouble under apache standalone
- back use .pem instead -all.pem (prepare for letsencrypt)
- mod sysinfo

### 7.0.0.b-2015121102.mr (Fri Dec 11 2015)

- fix domains.conf.tpl for apache (missing extra linebreak in location of blockips)
- fix spamdyke value after update

### 7.0.0.b-2015121001.mr (Thu Dec 10 2015)

- disable expire in kloxo-hiawatha (fix compatibility issue for hiawatha 9 and 10)

### 7.0.0.b-2015120903.mr (Wed Dec 09 2015)

- add 'upgrade' option in mysql-optimize
- remove 'Allow from all' for Location in apache
- disable 'UseGZfile' and change 'IgnoreDotHiawatha' to 'UseLocalConfig' (prepare for Hiawatha 10.x)
- change 'display_startup_errors' from 'on' to 'off' for php.ini
- change default dmarc value for 'percentage_filtering' ('50' to '20') and 'receiver_policy' ('quarantine' to 'none')
- fix 'dns blacklists' appear for mail
- disable exception for restore (possible restore without detect client)
- disable fixdomainkey from fixmail-all (resolve cloudflare trouble)
- mod 'mem-usage'
- mod restart.inc
- mod/fix set-hosts
- mod set-php-branch
- fix change permissions and ownership for file in 'file manager'
- fix phpini warning after client created
- mod uinstall web driver (no need uninstall mod_*)
- disable static files expire for kloxo-hiawatha until v10.1 release

### 7.0.0.b-2015103001.mr (Fri Oct 30 2015)

- make fix and restart to customize
- set switch dns and web without overwrite init file (trouble for httpd for init overwrite)
- mod httpd.init

### 7.0.0.b-2015102301.mr (Fri Oct 23 2015)

- add 'perl' in mailqueue script (no worry if qmHandle set with 755 or not)
- fix and mod httpd init
- fix web__lib.php

### 7.0.0.b-2015100801.mr (Thu Oct 08 2015)

- fix httpd init (especially for httpd24 from ius)

### 7.0.0.b-2015092401.mr (Thu Sep 24 2015)

- fix httpd.init for different pid file location for httpd and IUS httpd24
- add note for clientbaselib.php for commandlinelib.php
- fix php-fpm children for change resource plan

### 7.0.0.b-2015091201.mr (Sat Sep 12 2015)

- mod web__lib.php where create php-fpm for current user if not exists

### 7.0.0.b-2015083001.mr (Sun Aug 30 2015)

- add 'try_files $uri =404' for security issue
- add note in apache config where mod_ruid2 not work for mod_userdir
- add new phpmailer for prepare send with attachment
- mod commandlinelib.php (use lx_array_merge instead array_merge and change error message
- add commandlinelib_old.php
- mod mem-usage

### 7.0.0.b-2015081601.mr (Sun Aug 16 2015)

- fix commandlinelib.php related to 'add' and set only admin/auxiliary permit for certain commands
- fix default_index.php for login page
- add note for cpu-time, cpu-usage and mem-usage

### 7.0.0.b-2015081101.mr (Tue Aug 11 2015)

- move SSLCompression from httpd.conf to ssl.conf
- add page param in default_index.php and login_inc2.php
- fix mem-usage where use smem rpm

### 7.0.0.b-2015080401.mr (Tue Aug 04 2015)

- back to use php-fpm reload
- add 'aio threads' and 'directio 4m' for nginx
- add '--daemonize' for php-fpm start
- increase pm.process_idle_timeout to 20s for php-fpm
- add missing 'text_spf_redirect' column for mmail table in kloxo database

### 7.0.0.b-2015073001.mr (Thu Jul 30 2015)

- add cpu-time, cpu-usage and mem-usage script
- fix restart.inc related to php-fpm pkill
- add MaxServerLoad and CustomeHeader for 'X-Frame-Options:sameorigin' in hiawatha
- disable maxserverload but give notes for hiawatha

### 7.0.0.b-2015072602.mr (Sun Jul 26 2015)

- enable chroot for php-fpm
- disable 'SPF record' for dns
- split dkim for dns (especially for bind)
- set fcgid for httpd 2.2
- fix uninstallMeTrue for web
- remove unused code in setup/installer.sh
- back to disable chroot for php-fpm (still trouble)

### 7.0.0.b-2015072103.mr (Tue Jul 21 2015)

- use restart instead graceful for apache
- use restart for php-fpm
- add ProxyRequests and etc for mod_proxy_fcgi
- remove fix-outgoingips in add and delele domain in mmail__qmaillib.php
- fix commandlinelib.php (thanks for noamr@beyondsecurity.com for this security issue)
- fix restart.inc
- fix switch web server (related to hiawatha)

### 7.0.0.b-2015071402.mr (Tue Jul 14 2015)

- mod note for .htaccess.tpl
- add spf redirect
- add DocumentRoot in web/webmail redirect (to prevent "Got error 'Primary script unknown\n'" in httpd24)
- add ErrorLogFormat in httpd24.conf (add domain info)
- mod commandlinelib.php for temporary fix where only permit for admin or aux

### 7.0.0.b-2015071002.mr (Fri Jul 10 2015)

- fix sysinfo (forget reset $out before detect hiawatha service on or off)
- fix/mod htaccess.tpl for fcgid infoadd inform in clearallowedblockedip.php
- fix fix-outgoingips.php for handle IPv6
- fix update-ckeditor

### 7.0.0.b-2015070802.mr (Wed Jul 08 2015)

- mod gettraffic (add try-catch for web and mail stats)
- change 'check_vhost_docroot=false' for suphp (trouble for ~user url if 'true')
- add update-ckeditor script
- disable 'no_fix_config' in 'switch programs' (because only 'defaults' level executed)
- disable 'AddDefaultCharset' in apache and also tengine

### 7.0.0.b-2015070503.mr (Sun Jul 05 2015)

- mod fix-outgoingips.php to handle IPv6
- change php5.fcgi to php.fcgi
- make possible secondary php using fcgid beside suphp
- fix php.ini path for php.fcgi
- add info for secondary php using fcgid
- fix cp path for fcgid

### 7.0.0.b-2015070303.mr (Fri Jul 03 2015)

- remove 'proxy_set_header Host' from proxy configs in nginx (trouble with httpd 2.4)
- use 'proxy_set_header Host $host;' for nginx proxy
- mod/fix switch-apache
- fix ip detect for OpenVZ issue

### 7.0.0.b-2015070203.mr (Thu Jul 02 2015)

- set 'HostnameLookups Off' in httpd/httpd24.conf
- disable 'lbmethod_heartbeat_module' module for httpd24
- copy latest run script from qmail-toaster rpm
- mod switch-apache for install mod24u_session also
- add fix-yum script
- add 'port: 53' in nsd.conf (watchdog always think not running without it)
- partial fix for cron
- change 'port: 53' to 'ip@53' + add localhost in nsd.conf

### 7.0.0.b-2015070101.mr (Wed Jul 01 2015)

- add fix error 'Directory / is not owned by admin' in defaults.conf.tpl of apache
- fix info for SetHandler in .htaccess
- fix/adjust session_path in php.ini.base

### 7.0.0.b-2015063003.mr (Tue Jun 30 2015)

- add checking /var/log/named in list.transfered.conf.tpl of bind
- mod info in fixtraffic; fix ip detected (use 'ip addr' instead 'ifconfig')
- mod better ip detect
- still continue if 'database_user_already_exists' for database in restore process

### 7.0.0.b-2015062702.mr (Sat Jun 27 2015)

- always check /var/lib/php/session in fixphp
- mod 'sed' in certain scripts
- fix default init.conf for apache
- mod defaults.conf.tpl in apache
- mod nsd to add 'ip-address' in /etc/nsd/nsd.conf
- mod default.vcl for handle ssl port in varnish
- add 'serverips' in dns config
- remove getAllIps() in web/web__lib.php
- change 'Always need selected' to 'Select to execute' in desclib.php
- back to use 'interface_template.dump' in createDatabaseInterfaceTemplate()
- remove getIPs_from_ifconfig() and add getIPs_from_ipaddr and use it in os_get_allips()
- add update-phpmyadmin for update phpmyadmin for panel
- fix/remove \\n after {$end} in list.transfered.conf.tpl of nsd

### 7.0.0.b-2015062501.mr (Thu Jun 25 2015)

- fix sysinfo (using for detecting php-cli instead php because httpd 2.4 issue)
- fix sysinfo (detect hiawatha for web server)
- mod defaults.conf.tpl of hiawatha (fix cgihandler for php)

### 7.0.0.b-2015062401.mr (Wed Jun 24 2015)

- always copy suphp.conf to /etc every execute fixweb
- disable 'ProxySet enablereuse=on' for mod_proxy_fcgi (fix error 503 issue)
- increasing timeout for web configs (importance for long process like big size file upload)
- Change SSL to TLS in parameter to hiawatha

### 7.0.0.b-2015062301.mr (Tue Jun 23 2015)

- mod messagelib.php for .httaccess info for secondary php
- change uninstall from 'rpm -e --nodeps' to 'yum remove' for web server

### 7.0.0.b-2015062202.mr (Mon Jun 22 2015)

- add 'Click Help for more info' in every pages
- switch-apache also add/remove use_apache24.flg
- fix deleteDir related to stats data

### 7.0.0.b-2015062104.mr (Sun Jun 21 2015)

- mod conf.tpl for add timeout for mod_fastcgi and mod_proxy_fcgi
- change session path for phpXYs
- Remove 'ProxyTimeout' and enough using 'ProxySet timeout' for mod_proxy_fcgi
- add 'use apache 2.4' in 'switch programs'
- special handling in 'webserver configure' if enable apache 2.4
- move additional httpd module from lib.php to web__lib.php
- fix/mod for handling install/uninstall for httpd/htttpd24

### 7.0.0.b-2015062006.mr (Sat Jun 20 2015)

- change LogLevel from warn to error in httpd24
- no change for disable in 00-optional.conf
- set max=25 (as the same as ThreadsPerChild value) for proxy of mod_proxy_fcgi
- add stop server before switch to other version in switch-apache script
- fix/mod custom error pages
- back to add set.httpd.lst and set.nginx.lst
- combine remoteip and rpaf to rpaf.conf
- change %h to %a in LogFormat at httpd24.conf (mod_remoteip issue)
- add htcacheclean in restart-web and restart-all
- mod create /etc/sysconfig/httpd in defaults.conf.tpl

### 7.0.0.b-2015061902.mr (Fri Jun 19 2015)

- mod defaults.conf.tpl for httpd for handle httpd24u configs
- mod mod_proxy_fcgi setting for optimize performace
- add /var/run/php-fpm dir if not exists (deleted if php rpm removed)
- add switch-apache script for change apache 2.2 to 2.4 and vice-versa

### 7.0.0.b-2015061802.mr (Thu Jun 18 2015)

- change addhandler to sethandler in httpd (for security reason)
- set load module if not load in certain modules in httpd
- prepare secondary php using mod_fcgid
- httpd 2.4 ready to use with php-fpm (using mod_proxy_fcgi
- still trouble with secondary php with suphp)
- remove ThreadStackSize from ~lxcenter.conf (trouble with httpd 2.4)
- mod htaccess.tpl for using secondary php with httpd 2.4 compatible

### 7.0.0.b-2015061701.mr (Tue Jun 16 2015)

- move php-fpm restart before web server
- remove unwanted code in mmail
- remove ThreadStackSize and MaxMemFree from ~lxcenter.conf (trouble with httpd 2.4)
- mod and optimize httpd code related to 'define' module
- remove 'suPHP_AddHandler x-httpd-php52' from suphp.conf
- disable 'SSLMutex' from ssl.conf (trouble with httpd 2.4)
- remove double php5.fcgi; mod sysinfo for 'php used'
- back to disable chroot in suphp because not work

### 7.0.0.b-2015061103.mr (Thu Jun 11 2015)

- fix mysql-convert.php if cnf not exists
- fix appear for kloxo process if using cgi
- fix php-fpm process with always use restart instead reload
- mod define.conf because already exists in httpd 2.4
- fix display for cronlib; mod lib.php to execute all webmail setup script
- add memcached restart in restart-all and restart-web
- add fixcron script
- update setup script (need '-y' to force install)
- add width in 'go' of list

### 7.0.0.b-2015060902.mr (Tue Jun 09 2015)

- prepare httpd config for handle dual version
- fix fix-qmail-assign.php if no domain
- remove useless code in fixskeleton.php
- display webmail process with change exec to system
- fix php-fpm state if use php-fpm or not
- remove unwanted code in watchdog script
- fix copy process of php-fpm configs
- fix kloxo.init if not start process
- add 'set-kloxo-php cgi' in setup.sh/installer.sh

### 7.0.0.b-2015060901.mr (Tue Jun 09 2015)

- fix sysinfo.php if no php-type declare
- back to use restart instead stop+pkill+start because trouble if panel using php-fpm
- fix kloxo.init if no kloxo_php_active
- set panel use php-cgi instead php-fpm (less memory usage in idle state)

### 7.0.0.b-2015060802.mr (Mon Jun 08 2015)

- mod php-fpm where reload=restart
- restart script using stop+pkill+start instead restart
- mod setting for mod_proxy_fcgi (prepare httpd24)
- execute fixlxphpexe only if start kloxo service
- mod fixmail-all for fix me, defaultdomain and defaulthost also
- fix phpm-extension-installer
- disable charset to utf-8 in nginx

### 7.0.0.b-2015060501.mr (Fri Jun 05 2015)

- mod fix-outgoingips.php to possible manual modified
- add default charset to utf-8 in apache, nginx and php (no need for hiawatha)
- mod selectshow for file manager

### 7.0.0.b-2015053102.mr (Sun May 31 2015)

- change php-branch script to set-php-branch
- fix ip address detect (mostly related to OpenVZ problem)
- fix gateway detect for ip address

### 7.0.0.b-2015052801.mr (Thu May 28 2015)

- add mailqueue script for execute qmHandle
- mod setup-*.php where exit if no index.php for application directory

### 7.0.0.b-2015052703.mr (Wed May 27 2015)

- fix afterlogic setup (remove settings.xml if exists)
- reconstruct domains.conf.tpl for assigned IP
- change 'setup-php-fpm' to 'set-php-fpm' in certain files
- fix ipalloc (set only for non-reverseproxy)

### 7.0.0.b-2015052501.mr (Mon May 25 2015)

- mod sysinfo and add disk space info
- fix set-php-fpm if select php branch

### 7.0.0.b-2015052403.mr (Sun May 24 2015)

- mod installatron-install
- remove wrong file
- more info in sysinfo script
- change switch-php-fpm to set-php-fpm
- uninstall nginx also include tengine
- add changeport beside defaultport script for panel
- add kloxo-to-kloxomr7 script
- add hostname info in sysinfo script
- remove tengine from nginx uninstalled because based on rpmbranchlist

### 7.0.0.b-2015052102.mr (Thu May 21 2015)

- set no status for php if kloxo using cgi instead php-fpm
- set no initial phpini for web and domain (just for pserver and client)
- fix multiple_php_flag
- add space before '>' in VirtualHost (prevent for "directive missing closing '>'")
- disable phpini in client login because still not work

### 7.0.0.b-2015051903.mr (Tue May 19 2015)

- fix image path for error pages
- change suphp52.conf to suphp2.conf (because 'secondary php' for 'multiple php' instead 'php 5.2' only)
- add warning if hostname not match to FQDN
- rename suphp52.conf to suphp2.conf

### 7.0.0.b-2015051802.mr (Mon May 18 2015)

- fix fixphpini.php if phpini for pserver not exists
- note in nsd4.conf if declare ip must include 127.0.0.1
- fix phpinilib.php where fixphpIniFlag() also execute setUpInitialValues()
- disable all code related to installapp
- fix set_login_skin_to_simplicity for background
- mod alert from 200 to 100 from top
- mod mysql-convert and mysql-optimize also read '--help'

### 7.0.0.b-2015051801.mr (Mon May 18 2015)

- fix mysql-convert.php for tokudb and add note for reboot
- mod phpm-updater if symlink to phpXYm-cli exists

### 7.0.0.b-2015051701.mr (Sun May 17 2015)

- remove defaultValue for skin (appear error in debug)
- check phpini setting only under simplicity skin (still trouble in feather skin)

### 7.0.0.b-2015051601.mr (Sat May 16 2015)

- fix detect phpini for server, admin and clients
- set default value for skin

### 7.0.0.b-2015051502.mr (Fri May 15 2015)

- nginx init using tengine.conf instead nginx.conf if exists in /etc/nginx/conf
- fix dns setting, especially for third ns or more or sub ns

### 7.0.0.b-2015051501.mr (Fri May 15 2015)

- fix check_if_port_on for socket file and add detected by service status
- fix php.ini alert always appear in client login

### 7.0.0.b-2015051202.mr (Tue May 12 2015)

- fix set-hosts; add 'fastcgi_request_buffering off;' and 'proxy_request_buffering off;' to nginx
- add '#reuse_port on;' to nginx (work with tengine) if enable
- add detect pid for watchdog
- add restart-php-fpm
- restructure file inside /file dir and adjusment call for new location
- add '::' for 'Hostname' beside '0.0.0.0' in hiawatha
- fix copy apache configs

### 7.0.0.b-2015051101.mr (Mon May 11 2015)

- add ipv6 support for nginx
- fix hiawatha for ip assign to domain
- add 'update all' for 'switch programs'
- add 'customize' stamp in ~lxcenter.conf and detected by 'webserver configure'
- fix copy php-fpm files; change 'alert' appear from 350 to 100px from top

### 7.0.0.b-2015050901.mr (Sat May 09 2015)

- fix mpm calculation in ~lxcenter.conf.tpl
- fix installWebmailDb.php for install webmails
- use xml.php instead xml if exists for afterlogic
- set default of 'php used' as 'php54m' in php.ini of website
- fix getAnyErrorMessage for handling phpini for server and client
- add function db_del_value(); add return value for delRow() in sqlitelib.php
- remove copy php.ini using php.ini.base process

### 7.0.0.b-2015050703.mr (Thu May 07 2015)

- set phptype_not_set and phpini_not_set only (without _pserver and _client)
- if phpini not set in pserver, click phpini in admin will go to phpini in server
- move detect phpini not set in pserver from setUpInitialValues to initPhpIni
- add missing phpm-all-setup

### 7.0.0.b-2015050604.mr (Wed May 06 2015)

- fix validate_mail (add max domain ext from 6 to 16 like validate_domain_name)
- add 'stamp' for ~lxcenter.conf to identify 'apache optimize' selected
- remove '--no change--' in 'webserver configure' (automatically change to built-in '--select on--')

### 7.0.0.b-2015050603.mr (Wed May 06 2015)

- Panel possible using php-cgi instead php-fpm (less memory usage)
- add set-kloxo-php for switch php-fpm or php-cgi for panel
- overhaul change for phpXYm and phpYXs setting
- add process to update phpXYm and phpXYs in cleanup
- entering short IPv6 format automatically convert to long format
- add jk_init.ini (prepare for jailkit implementation)
- add kloxo_sqlite.sql (prepare change to sqlite from mysql for kloxo database)
- mod fixlxphpexe (install php54s if not exists) and set-kloxo-php (default as 'fpm')
- mod separate var for mem and timeout between phpXYm and phpXYs

### 7.0.0.b-2015050401.mr (Mon May 04 2015)

- make simple report for fix-outgoingips.php, fixdnschangeip.php and fixdnsremoverecord.php
- make hiawatha/nginx always report to all logs
- remove/disable proxy_cache_use_stale in nginx
- fix display for favorites in feather skin
- possible using php-cgi instead php-fpm in panel (less memory usage) if exists 'kloxo_use_php-cgi' file
- change log path for panel to /var/log/hiawatha

### 7.0.0.b-2015050301.mr (Sun May 03 2015)

- move resetQmailAssign() from lib.php to fix-qmail-assign.php (make slim lib.php)
- add fix-outgoingips
- add and delete domain in mmail also execute fix-outgoingips
- add fix-outgoingips to fixmail-all

### 7.0.0.b-2015050201.mr (Sat May 02 2015)

- mysql-convert.php also handle tokudb storage-engine
- change disablephp to enablephp and add enablessl and enablessl in webserver configs
- disable 'create ...' in logrotate
- disable reading IP from ifcfg (because need 'perfect' format) and use read 'ifconfig' and 'ip' only

### 7.0.0.b-2015043001.mr (Thu Apr 30 2015)

- mod without set to myisam for mysql in install process (minimize trouble for update from Kloxo or Kloxo-MR 6.5)

### 7.0.0.b-2015042903.mr (Tue Apr 28 2015)

- add 'include' for spf

### 7.0.0.b-2015042902.mr (Tue Apr 28 2015)

- mod admin.sql
- add 'enable_spf_autoip' column in mmail table of kloxo database
- make possible remove a__base__ record (prepare for aaa__base__ for IPv6)
- add 'automatic add IP' in SPF record; enable 'update all' for 'limit'
- change 'a record' parameter from ttype_hostname_param to ttype_hostname

### 7.0.0.b-2015042901.mr (Tue Apr 28 2015)

- add '--client' for fixdnsremoverecord
- fix message for setup horde; add missing setup-rainloop
- disable 'proxy_cache_use_stale' for nginx

### 7.0.0.b-2015042803.mr (Tue Apr 28 2015)

- move webmail setup to sh script from lib.php
- fix inactivate iptables
- fix display private._domainkey

### 7.0.0.b-2015042801.mr (Tue Apr 28 2015)

- fix chown and chmod in 'file manager' (missing path declare)
- fix sslcertlib for 'nname' under client

### 7.0.0.b-2015042703.mr (Mon Apr 27 2015)

- fix fixdnsremoverecord.php
- more info for fixdnschangeip and fixdnsremoverecord
- change argument names for fixdnsremoverecord
- back to use previous code for dnsbaselib because impossible to use hostname with double underline
- add proxy in nginx for ssl
- fix dnsbaselib.php (especially in 'ns record')

### 7.0.0.b-2015042601.mr (Sun Apr 26 2015)

- back to use old trick because 'chkconfig off' and 'chkconfig --del' not enough for iptables after reboot
- set 'webmail apps' as '--choose--' instead 'select one'
- delete unwanted fixdns.php in /bin/misc
- hidden certain process in installer.php

### 7.0.0.b-2015042503.mr (Sat Apr 25 2015)

- mod 'txt record' possible record name with '_'
- change 'chkconfig off' to 'chkconfig --del' for iptables
- mod fixdnschangeip
- add fixdnsremoverecord script (still experimental)
- add setup-tht script (thehostingtool billing; still experimental)
- fix fixdnschangeip and fixdnsremoverecord logic
- more info for setup-tht

### 7.0.0.b-2015042401.mr (Fri Apr 24 2015)

- fix mysql-convert for MariaDB
- mod more space for 'txt record value' in add/update dns
- fix mysql-convert

### 7.0.0.b-2015042301.mr (Thu Apr 23 2015)

- mod note for phpm-installer; inactivate iptables in install process
- fix dmarc in 'txt record' in dns
- auto-convert if in 'txt record' found '__base__' or '<%domain%>' in domains.conf.tpl
- change mail_feedback in dmarc add/update from 'admin@domain.com' to 'admin@__base__'

### 7.0.0.b-2015042104.mr (Tue Apr 21 2015)

- add phpm-all-install for install php branch for phpm; add sendmail-limiter.sql
- move all .sql to /file/sql
- mod inactive iptables in install process
- separated var name for standard and start dirprotect for nginx
- fix sendmail-wrapper
- fix 'exclude_all_others' title
- replace sendmail-wrapper file because wrong file

### 7.0.0.b-2015042003.mr (Mon Apr 20 2015)

- fix cronlib.php (add db write in postAdd)
- fix 'file manager' if file with space
- change minimum ttl from 1800 to 3600 (RFC 2308)
- fix strpos logic in simplicity menu
- make sure re-install will be to user default services
- add vpopmail.sql (just for archive)
- set minimum ttl in dns config template from 1800 to 3600
- list all IPs (include hostname IP) in dns syncCreateConf
- mod sendmail-wrapper (possible add addons script instead specific for sendmail-limiter only)

### 7.0.0.b-2015041901.mr (Sun Apr 19 2015)

- back to use previous ffile__common because problem not the code but user level
- change session path for client to /home/kloxo/client//session

### 7.0.0.b-2015041602.mr (Fri Apr 17 2015)

- mod update.sql (remove ftp from watchdog and other mods)
- add smtp, imap and pop fname records in dns template
- remove restart/kill service from kloxo and syslog logrotate
- change tls_protocol from ssl23 to tls1 in imapd-ssl and pop3d-ssl
- back to use 'setdriver' in the end of install process because certain os template need it
- mod update.sql (use 'add column if not exists' and 'change column if exists')

### 7.0.0.b-2015041504.mr (Wed Apr 15 2015)

- back to separate logic for spamassassin and bogofilter in maildrop
- make possible delete for file with space (still not work for copy-paste)
- mod message for 'invalid password'

### 7.0.0.b-2015041503.mr (Wed Apr 15 2015)

- mod 'add watchdog' message
- mod sendmail-wrapper to possible ban subdir under pwd
- add missing 'default_port_ftp' translate
- mod suphp config for handle php51m-php56m
- mod description for 'secondary php'
- mod description for htaccess.tpl

### 7.0.0.b-2015041302.mr (Mon Apr 13 2015)

- fix hostname and server_alias validate with possible 1 alpanumeric char
- fix sendmail-limiter
- remove ns and change ftp from 'cname' to 'a' record for base dns template
- use try-catch for create cronjob
- disable password change for localhost (but not work)
- prepare using session_login instead record to 'loginattempt'
- fix phpm-extension-installer if directory not exists
- change fail login from 'error' to 'login_fail' log
- fix prepare session_login code

### 7.0.0.b-2015041104.mr (Sat Apr 11 2015)

- always combine bogofilter and spamassassin code in .maildroprc file
- no appear for 'multile php ratio' if disable 'multiple php enable'
- fix ssl name for upload ssl

### 7.0.0.b-2015041103.mr (Sat Apr 11 2015)

- fix 'default' drivers in install process
- change php ratio from '0:6:0:0:0' (5.3 as default) to '0:0:6:0:0' (5.4 as default)
- fix synchronize driver in cleanup process
- fix changedriver
- fix setSyncDrivers again

### 7.0.0.b-2015041001.mr (Fri Apr 10 2015)

- change SSLCipherSuite (use from Mozilla recommendation) for apache
- use maxpar instead minpar for calculate MaxClients in apache
- remove pure-ftpd log for kloxo.logrotate (because pure-ftpd logrotate exists)
- also change ssl_ciphers for nginx; prepare php-fpm chroot
- make appear default timezone in 'php configure' for admin
- mod 'date' appear for lxguard in begin newer than end
- redirect to 'password' page if password as 'admin'
- add 'net.ipv4.tcp_syncookies = 1' and 'net.ipv4.tcp_max_syn_backlog = 2048' in sysctl.conf to prevent server as flood
- change changedriver to setdriver in the end of install process
- add setdriver script
- fix detect driver in lib.php
- remove setdriver in install process
- disable/no need 'default' driver value in install process
- rename validate_password_add to validated_password
- possible change ssh root password in 'ssh configure'
- fix missing to put sshd_config 'template' content
- truncate 'information' list data if > 20 chars
- add title in 'information' list data
- change 'ftp user' to 'username' in 'information' list data

### 7.0.0.b-2015040701.mr (Tue Apr 07 2015)

- make hidden changedriver in install process
- add rename mod_*.conf to .nonconf for httpd module configs
- read log if hitlist.info not exists for lxguard

### 7.0.0.b-2015040601.mr (Mon Apr 06 2015)

- mod sysinfo (add 'also as webserver' for hiawatha)
- prepare watchdog if change change port for ftp
- possible change port and action in watchdog update
- mod error pages to match latest index.html (small logo appear)
- add help for watchdog add
- also record ssh success access
- add changedriver to default value for end of setup.sh (in install process)

### 7.0.0.b-2015040402.mr (Sat Apr 04 2015)

- make possible change pure-ftpd port 21 to other (via 'ftp configure')
- change min and max port for passive ftp from 30000-50000 to 45000-65000
- change syslog from end to begin in list of restart-all

### 7.0.0.b-2015040401.mr (Sat Apr 04 2015)

- use closeallinput instead closeinput (source code taken from hypervm)
- fix kloxo.c related to execl

### 7.0.0.b-2015040302.mr (Fri Apr 03 2015)

- change arg in lxguard_main
- add for remove access.info and hitlist.info in lxguard.php
- fix lxguard_main for handle ssh and ftp log
- mod fixlxguard with delete access.info and hitlist.info before execute lxguard_main

### 7.0.0.b-2015040201.mr (Thu Apr 02 2015)

- create fixlxguard script
- separated process for ssh and ftp for lxguard because different log
- add parse_ssh_log for handling ssh log parse
- optimize process for lxguard for reading log
- fix lxguard logic if certain files not exists
- add declare missing static data in pserverlib dan lxguard
- add '-p' in webalizer process
- fix merge_array_object_not_deleted if not multi-dem array
- add 'since' param for lxguard_main
- fix bind log dir permissions in installer process
- fix phpm-config-setup and php-extension-installer for handling extension conf

### 7.0.0.b-2015032901.mr (Sun Mar 29 2015)

- fix 'edit mx' (must priority 10; old code trouble for multiple mx)
- fix resetQmailAssign (virtualdomains must content without remote type)

### 7.0.0.b-2015032801.mr (Sat Mar 28 2015)

- fix mysql-convert (using server.cnf instead my.cnf in /etc/my.cnf.d)
- click webmail apps will be appear in new tab/windows

### 7.0.0.b-2015032701.mr (Fri Mar 27 2015)

- fix zend_extension path in phpm install/update process
- add sysstat install in install process

### 7.0.0.b-2015032603.mr (Thu Mar 26 2015)

- fix sendmail-wrapper (according to latest qmail-toaster)
- make phpm-installer faster process (don't care dependencies installed or not)
- change phpm-installer logic to found dependencies
- mod prefork.inc.tpl with default values
- prepare tpl in phpcfg
- prepare sendmail-wrapper for sendmail-limiter
- mod diskquota check with nice and ionice
- mod installer.php with remove smail and add ncdu (realtime du)
- mod/fix phpm-extension-installer and phpm-installer
- viruscan possible using customize dir target
- make possible use phpm-extension-installer for 'standard' php beside phpm
- fix detect uname in phpm-extension-installer
- more info in phpm-extension-installer process
- use counter in phpm-extension-installer and phpm-installer

### 7.0.0.b-2015032202.mr (Sun Mar 22 2015)

- fix phpm-installer (missing like 15-*.ini)
- separated logic for mysql and mariadb installing
- mod uninstall spamassassin must exclude simscan and ripmime
- enable 'virus scan' will be install clamav and clamd
- disable 'virus scan' will uninstall clamav and clamd only
- no uninstall clamav and clamd if disable 'virus scan' because possible to used by other purpose

### 7.0.0.b-2015032101.mr (Sat Mar 21 2015)

- warning in nsd4.conf where need declare IP in server with multiple IP
- fix/mod watchdog list
- back to use default syslog.logrotate (add add rsyslog repo in mratwork.repo)
- possible restart syslog if using syslog-ng
- add phpm-updater
- add bench script
- fix fixrepo (wrong var)
- mod soft-reboot (identify initrd for kdump)

### 7.0.0.b-2015032001.mr (Fri Mar 20 2015)

- fix logic for .ca if exists
- fix info in phpm-installer
- add again 'spf record' in dns because bind may warning without it
- fix crftoken to csrftoken
- change 'ionice -c 2 -n 7' to 'nice -n +10 ionice -c3' backup/restore process
- use call fixrepo instead direct login in installer.php
- fix fixrepo logic
- mod raw-backup and add raw-restore
- fix/change isCRFToken to isCSRFToken in coredisplaylib.php

### 7.0.0.b-2015031501.mr (Sun Mar 15 2015)

- remove 'engine' declare from database dump in backup and restore process
- set all error log as error.log in php-fpm conf
- change ripmime-toaster to ripmime reference
- change postmaster to admin in mail feedback of dmarc
- create nsd or other dir in /etc if not exists

### 7.0.0.b-2015031301.mr (Fri Mar 13 2015)

- fix hostmaster insert in dns (especially for 'old' setting)
- add insert totalinode_usage in client table of kloxo database
- change email in domainkeys from postmaster to admin

### 7.0.0.b-2015031202.mr (Thu Mar 12 2015)

- prepare inode quota
- fix/update paths in createDir
- fix service list for mydns and yadifa
- disable declare other object in mailaccount (make trouble in backup/restore)

### 7.0.0.b-2015031201.mr (Thu Mar 12 2015)

- change unzip_with_throw to unzip in restore process
- change all php error log to php-error.log
- add libgearman in php dependencies of phpm-installer
- separate zip process for mysql and home in raw-backup
- more logs report

### 7.0.0.b-2015030901.mr (Mon Mar 09 2015)

- add httpd24u in set.web.lst
- mod sendmail-wrapper (implementing to qmail-toaster)
- fix/add for list and extract of tar.gz, tar.bz2 and tar.xz
- fix mysql-optimize script
- add viruscan script (for /home at this time)
- add hostmaster in 'general settings' in 'manage dns'
- implementing dmarc beside spf in 'email auth'
- make blank for rcpthosts because move to morercpthosts

### 7.0.0.b-2015030101.mr (Sun Mar 01 2015)

- mod mysql-to-mariadb and mariadb-to-mysql to make possible change without enable repo
- enable spamdyke and clamav will be installing their rpm
- fix bind in install process
- fixmail-all also execute 'qmailctl cdb'
- fix detecting mariadb if installed

### 7.0.0.b-2015022101.mr (Sat Feb 21 2015)

- use mariadb instead mysql in install process (trouble with mysql after IUS release MySQL56u)
- install spamdyke if enabled
- fix pure-ftpd removed in install process
- add user list in raw-backup
- add 'performace_schema=on' in set-mysql-default (also use by install process)

### 7.0.0.b-2015021801.mr (Wed Feb 18 2015)

- add closeallinput.c (taken from hypervm 2.1 beta)
- remove libmysqlclient together with mysql in setup.sh

### 7.0.0.b-2015021501.mr (Sun Feb 15 2015)

- fix cron in install process (CentOS 5 using vixie-cron but CentOS 6 using cronie)
- add version-full script (as the same as 'version --vertype=full')
- fix bind (create missing /var/log/named)

### 7.0.0.b-2015021001.mr (Tue Feb 10 2015)

- fix tpl for bind
- fix default.pem (move private key to top)
- only use morercpthosts (make blank rcpthosts)
- 'TXT record' accept underline chars (importance like _dmarc)

### 7.0.0.b-2015020401.mr (Wed Feb 04 2015)

- mod and add info to redirect to ssl in default_index.php
- set httpd to disable SSLv3 support
- add ChallengeClient in hiawatha for better DDos protect
- add badsendmailfrom file for sendmail-wrapper
- php54+ use mysqlnd install mysql module
- mod soft-reboot
- add change-root-password script

### 7.0.0.b-2015012701.mr (Tue Jan 27 2015)

- fix wrong logic for mysql service
- add soft-reboot (not work in openvz container)
- change error log level in nginx from warn to error
- fix unzip for zip filename with space
- add chmod for stats dir in fix-chownchmod (nginx issue)
- prepare encrypt and decrypt for form passing data (emulate https for http scheme)
- re-enable backup log for 'Setting parent of' process
- change from php 5.3 to 5.4 as default php for panel and website
- prepate for also using webtatic php-branch (phpXYw beside phpXY/phpYXu)

### 7.0.0.b-2015011402.mr (Wed Jan 14 2015)

- fix 2 doublequote in installer.php
- use 'rndc-key' instead 'rndckey' for bind
- create /var/log/named if not exists in cleanup process

### 7.0.0.b-2015011302.mr (Tue Jan 13 2015)

- change gwan to monkey
- fix bind related to rndckey
- fix hiawatha for execute cgi in cgi-bin dir
- change phpini from after to before phptype
- fix telaen in cleanup process
- fix libcurl in CentOS 6 (install curl-devel in install process; php-common need libcurl)
- fix telaen for replace configs

### 7.0.0.b-2015010905.mr (Fri Jan 09 2015)

- move domain ssl from /home//ssl to /home/kloxo/client//ssl
- fix to not use quote in langkeywordlib.php (not appear in quote exist)
- not permit to create subdomain as webmail/mail/lists/cp/default/www
- also not permit for lists subdomain
- move domain ssl dir using fixweb instead cleanup (because still trouble)
- fix domain ssl path in apache domains.conf.tpl
- fix wrong validate function name
- mod keyword related to 'no permit as subdomain'

### 7.0.0.b-2015010802.mr (Thu Jan 08 2015)

- fix/mod phpm-installer (related to zend_extension path)
- fix web conf.tpl (related to ip for domain)
- mod info in phpm-installer process

### 7.0.0.b-2015010601.mr (Tue Jan 06 2015)

- better report for sendmail-wrapper
- fix hiawatha-proxy config (remove useToolkit if using ReverseProxy)

### 7.0.0.b-2015010401.mr (Sun Jan 04 2015)

- back to enable morercpthosts in spamdyke.conf
- make morercpthosts as the same content of rcpthosts (easy implementing)
- set installatron opening in new tab/window
- new install also include php gd and ioncube (need by installatron)
- mod installatron-install

### 7.0.0.b-2014122901.mr (Mon Dec 29 2014)

- reduce proxy_cache from 1000m to 100m (fix for upload issue)
- move panel redirect logic to redirect.php
- process for redirect_to_ssl and redirect_to_hostname for panel
- mod pdns.sql to accept pdns 3.4.x
- pending update pdns 3.3.1 to 3.4.1 (trouble with rpm compile)

### 7.0.0.b-2014122501.mr (Thu Dec 25 2014)

- fix quota if select unlimited (quota exceed appear in openvz)

### 7.0.0.b-2014122401.mr (Wed Dec 24 2014)

- set apache, hiawatha and domains always listen '*' IP
- separated validate_server_alias (add '*' to validate) from validate_hostname_name
- fix logrotate for kloxo and syslog

### 7.0.0.b-2014122102.mr (Sun Dec 21 2014)

- use tar.gz instead zip in raw-backup
- changeport.php also create port-ssl and port-nonssl file (used by redirect in login)
- panel redirect to ssl only need create redirect-to-ssl in login dir (no need custom-inc2.php)
- enable 'Redirect...' in 'Port configure' will create redirect-to-ssl automatically
- use kloxobck instead kloxo.bck for kloxo database in raw-backup process

### 7.0.0.b-2014121901.mr (Fri Dec 19 2014)

- fix sendmail-wrapper report
- fix qmail restart
- enable and mod breadcomb in 'file manager'
- fix proxy_standard.conf in nginx (remove $proxy_port)

### 7.0.0.b-2014121601.mr (Tue Dec 16 2014)

- fix ezmlm for add ml
- mod to 'proxy_set_header X-Host $host:$proxy_port' for nginx-proxy

### 7.0.0.b-2014121301.mr (Sat Dec 13 2014)

- mod 'AccessLogFile=/dev/null' to 'AccessLogFile=none' for disable accesslog (need hiawatha 9.9.0-f.2+
- add sendmail-wrapper to report caller of php mail() in maillog (need update qmail-toaster)
- fix domains.conf.tpl for domain-based customize rewrite rule
- mod sendmail-wrapper
- correcting // to / for path in file manager

### 7.0.0.b-2014120802.mr (Mon Dec 08 2014)

- possible custom rewrite file (similar with php-fpm config in globals do) for nginx
- add 'path' values in php-fpm tpl
- prepare for inode quota
- add dns-blacklist-entry in spamdyke
- change 'Last login date' format in 'information'
- change '-' value to '0' in client list
- change/fix block_shellshock in hiawatha
- mod spamdyke where using spamdyke_rbl.txt as external rbl list

### 7.0.0.b-2014120301.mr (Wed Dec 03 2014)

- use re-process instead show warning in add mail account
- set z-index for logo to -9999 (mean always in below)
- fix nginx to use .ca file if exists;
- add errorlog and accesslog (to /dev/null) for cp and webmail of every domains in hiawatha

### 7.0.0.b-2014112003.mr (Thu Nov 20 2014)

- fix default.conf.tpl in apache
- fix client list appear
- trim username and password for whitespace in login process
- fix telaen webmail

### 7.0.0.b-2014111701.mr (Mon Nov 17 2014)

- add message for 'session_timeout'
- set inode to 1/100 and convert to integer

### 7.0.0.b-2014111601.mr (Sun Nov 16 2014)

- disable convert 'rndc-key' to 'rndckey' in list.transfered.conf.tpl because back to use 'rndc-key'

### 7.0.0.b-2014111401.mr (Fri Nov 14 2014)

- add script for remove /home/httpd/*/httpdocs (also conf dirs)
- set createDir() not include create httpdocs dir
- fix createDatabaseInterfaceTemplate()
- mod fix.inc and restart.inc
- mod skin-set-for-all to possible to select skin
- mod defaults.conf.tpl of hiawatha to off accesslog
- back to use 'rndc-key' instead 'rndckey' for bind
- fix for master-slave for detect rpm in component__rpmlib
- remove OldUpdate() from ipaddress__redhatlib
- make possible detect ip with 'bootproto=dhcp'
- modified ip detect with ifcfg- and ifconfig command

### 7.0.0.b-2014110701.mr (Fri Nov 07 2014)

- disable config-mysql in spamdyke.conf (because not used)
- add utf-8 charset in error pages
- short 'for help' in scripts

### 7.0.0.b-2014110602.mr (Thu Nov 06 2014)

- fix mysql restart process
- fix spamdyke.conf
- remove 'spam status' from mail login
- prepare dhcp ip detect
- fix image url for error pages
- change from 'default domain' to 'contact email' in client list

### 7.0.0.b-2014110202.mr (Sun Nov 02 2014)

- fix and add 'IgnoreDotHiawatha' for proxy in hiawatha configs
- fix dns related to parked/redirect domain
- fix process also for 'none' driver
- optimize fixdns for speed
- combine syncAddFile() to createConfFile() in dns__lib
- getDnsMasters() include parked/redirect domains
- fix webmail redirect in hiawatha

### 7.0.0.b-2014102901.mr (Wed Oct 29 2014)

- fix ftpuser (wrong declare quota) and mod detect status

### 7.0.0.b-2014102703.mr (Mon Oct 27 2014)

- make logo image smaller (from height 75 to 50) and add margin padding
- mod lxguard raw connection list
- set session dir to chmod 777 for user-level
- set 'backuper' user as 'special' client (will not including in backup)
- install process also remove epel.repo
- mod help message for phpini

### 7.0.0.b-2014102701.mr (Mon Oct 27 2014)

- fix mysql restart process
- move cp from default.conf.tpl to domains.conf.tpl in apache
- mod sql related to watchdog list
- mod check_if_port_on() for accept unix socket
- add 'telnet' installing in install process

### 7.0.0.b-2014102603.mr (Sun Oct 26 2014)

- make only one time process in fix-chownchmod if the same docroot

### 7.0.0.b-2014102602.mr (Sat Oct 25 2014)

- mod set-hosts with using 'hostname -i' instead 'ifconfig' to detect 'primary' IP
- accept php56m in 'php ratio' at 'php configure'
- mod to create sock dir before copy php-fpm configs
- fix 'default' ~lxcenter.conf
- remove fix_chownchmod from web__lib because change approach in fix-chownchmod script
- change chmod from 750 to 751 in createUser()
- remove maradns from setInitialAllDnsConfigs()
- change from 'none' to default value in 'empty' driver in slavedb

### 7.0.0.b-2014102504.mr (Sat Oct 25 2014)

- add php56m for 'multiple php'
- mod phpm-config-setup for accept php56m
- add setEnableQuota (still manual add usrquota/grpquota to /etc/fstab)
- mod to list all ip list in dns; mod restore help
- mod nsd configs (using createRestartFile)
- mod nsd configs (createRestartFile for master and slave list also)
- disable setEnableQuota (until auto-edit /etc/fstab ready)

### 7.0.0.b-2014102402.mr (Fri Oct 24 2014)

- set pureftpd tp disable sslv2 and sslv3
- mod Autoresponder list
- mod skeleton.zip (make smaller logo)
- fix redirect to ssl in panel
- mod help message for php configure
- prepare 'web select' (beside 'php select' for 'multiple php')

### 7.0.0.b-2014102302.mr (Thu Oct 23 2014)

- fix docroot for cp in apache; use '' instead '' in apache
- fix Autoresponder list
- fix sp_specialplay if login via mailaccount
- add 'password' in mailaccount login
- fix menu in mailaccount login
- make simple setInitial for web, webcache and dns (prepare fix script for including)

### 7.0.0.b-2014102301.mr (Thu Oct 23 2014)

- fix for update from Kloxo 6.1.19+
- fix setSyncDrivers() (add try-catch; importance if update from 6.1.x/6.5.x)

### 7.0.0.b-2014102203.mr (Wed Oct 22 2014)

- set error page as the same as default page appear
- set logo with transparent background in panel
- setDefaultPages() including process error pages and remove setCopyErrorPages()
- make shorter message in error pages
- enable custom error in apache

### 7.0.0.b-2014102103.mr (Tue Oct 21 2014)

- fix bind with rndc-key (add sed in list.transfered.conf.tpl)
- fix reversedns ns update
- disable 'spf record' because deprecated (RFC7208)
- Add setSyncDrivers() to make sure driver data is synchronize between slavedb and table
- set click phpini warning will be going to php configure in server-level

### 7.0.0.b-2014102004.mr (Mon Oct 20 2014)

- fix named.options.conf in bind (use 'rndckey' instead 'rndc-key')
- prepare shexec (sh script for passing all php exec process)
- prepare reversedns
- move process to remove rndc.conf from master to transfer tpl (for bind)
- fix if dns zonelists not exists
- fix dns zonelists (use 'echo' instead 'cat'; the same effect but 'cat' will be warning)

### 7.0.0.b-2014101902.mr (Sun Oct 19 2014)

- fix dns configs (need db write in dbactionAdd and dbactionDelete)
- mod dns libs (as the same as web libs 'style')
- rm_rec use do_exec_system instead exec
- fix fix-qmail-assign (forget \n for rcpthosts and virtualdomains)

### 7.0.0.b-2014101802.mr (Sat Oct 18 2014)

- fix domains.conf.tpl for apache (because not make '127.0.0.1' in proxy)
- mod error page for nginx
- cleanup php-fpm.conf for nginx
- remove fix-chownchmod in user-level
- file manager able to change permissions and ownership
- fix document path in ffilelib.php; make simple text in .htaccess
- os_get_group_from_gid()
- mod restart-all and restart-web (make apache in last position)
- fix ip define in apache
- add missing cp redirect in lighttpd
- add missing cp redirect in nginx
- remove error page declare in proxy in nginx (moving to generic.conf)

### 7.0.0.b-2014101705.mr (Fri Oct 17 2014)

- fix user-assign also fix rcpthosts and virtualdomains for qmail
- may trouble with hugh domains amount because not using morercpthosts
- mod file_permissions in file manager (todo: file_ownership)

### 7.0.0.b-2014101703.mr (Fri Oct 17 2014)

- enable fix-chownchmod in user-level with right detect
- fix detect pserver in server-level phpini
- fix multiple_php_list if not exists
- fix detect for 'webserver configure' (related to 'fix-chownchomod')

### 7.0.0.b-2014101701.mr (Fri Oct 17 2014)

- remove TODO from fix-chownchmod.php for client-based fix
- fix fixweb.php (missing quote for lighttpd)
- fix defaults.conf.tpl for apache for ~lxcenter.conf exception
- disable user-based fix-chownchmod.php in panel (still not work)

### 7.0.0.b-2014101601.mr (Thu Oct 16 2014)

- mod fix-chownchmod to make possible for user
- add 'webserver config' for user (for 'fix-chownchmod')
- set all ssl to disable SSLv2 and SSLv3 (only enable for TLS1.0+)
- mod domain.conf.tpl related to 'Define'
- hidden nsd identity; hidden 'breadcombs' for file select
- change digest_alg value from 'SHA1' to 'SHA256'
- prepare symlink of __backup dir to their user path

### 7.0.0.b-2014101504.mr (Wed Oct 15 2014)

- add missing list.transfer.conf.tpl for mydns
- fix httpd configs related to mod_define work
- only overwrite ~lxcenter.conf if exists their custom file
- fix for missing define ip and port for proxy

### 7.0.0.b-2014101502.mr (Wed Oct 15 2014)

- add php_defines in php-fpm of php 5.2
- fix php session path for server
- fix timezone selected in php
- fix nsd configs because using root as user
- fix yadifa because no keys and xfr dirs

### 7.0.0.b-2014101403.mr (Tue Oct 14 2014)

- move htaccess.tpl from phpini/tpl to apache/tpl
- set htaccess.tpl only have info related to secondary php
- move php_value and _flag from .htaccess to prefork.inc
- set include prefork.inc to apache domain configs
- set nginx configs to no limit_conn for localhost
- add deny localhost for BanListMask in hiawatha
- set serverlimit = MaxClients for prefork and itk
- fix fixphp script related to .htaccess

### 7.0.0.b-2014101402.mr (Tue Oct 14 2014)

- move blocksize from setQuota to syncNewquota and os_set_quota for calculate quota
- change inode as 1/10 total blocksize to total disk space
- change initial value of client php from static to server php value
- mod restart process of qmail
- set maxclients = minspareservers
	334	---------- * 5 in prefork/itk and maxclients = minspareservers
	333	---------- * threadsperchild

### 7.0.0.b-2014101303.mr (Mon Oct 13 2014)

- add directory toolkits for hiawatha (implementing .hiawatha)

### 7.0.0.b-2014101302.mr (Mon Oct 13 2014)

- yadifa running well now (have bug in 1.0.3 related to '_' char)
- mod domains.conf.tpl in nsd (using by bind, nsd and yadifa)
- mod domains.conf.tpl for nsd (again)
- NOTE: yadifa 1.0.3 have bugs for double-quote in TXT and unsupport SPF record

### 7.0.0.b-2014101206.mr (Sun Oct 12 2014)

- set watchdog for __driver_ to use restart with '--force'
- roundcude in cleanup also process update sqls
- update yadifa configs (running but still no read zones)

### 7.0.0.b-2014101204.mr (Sun Oct 12 2014)

- fix directory permissions for master and slave configs
- back to use restart instead reload in bind because slave problem
- fix and make simple change 'php-type'
- add itk.conf as 'emulate' httpd module
- remove restart in set_phpfpm in change php-type
- warning if php-type not php-fpm and select phpXYm in 'php used'

### 7.0.0.b-2014101106.mr (Sat Oct 11 2014)

- using restart --force in 'command center'
- fix bind configs (related to 'share' domains configs with nsd and yadifa)
- mod yadifa configs (still in progrss)

### 7.0.0.b-2014101103.mr (Sat Oct 11 2014)

- back to user root as user in nsd because too much permissions denies
- add yadifa as dns server
- make more info 'reload' nsd
- use the nsd domain configs for bind, nsd and yadifa (because the same structure)

### 7.0.0.b-2014101102.mr (Sat Oct 11 2014)

- move 'apache optimize' calculation from apache-optimize.php to ~lxcenter.conf.tpl
- mod help for apache optimize
- fix equation of apache optimize

### 7.0.0.b-2014100905.mr (Thu Oct 09 2014)

- force using kloxomr7 packages in install process
- fix install process related to thirdparty for kloxomr7
- fix installer related to vpopmail password
- remove lxphp.sh

### 7.0.0.b-2014100902.mr (Thu Oct 09 2014)

- fix setQuota() because wrong refer to getFSBlockSize() instead getFSBlockSizeInKb
- remove restart in cleanup-simple
- fix restart related to scavenge

### 7.0.0.b-2014100807.mr (Wed Oct 08 2014)

- fix mariadb-to-mysql convert
- fix mariadb in service list
- fix blank 'php configure'
- fix error if reversedns driver lib not enable
- fix topbar_right related to status color

### 7.0.0.b-2014100804.mr (Wed Oct 08 2014)

- remove force-restart
- add '--help' in restart
- add '--force' to running 'real restart' instead custom restart process
- set setup/installer/cleanup using 'restart' with '--force'
- fix list.master/slave.conf.tpl of dns config
- add getFSBlockSizeInKb() to detect filesystems blocksize
- fix setQuota (depend on blocksize)
- set inode as 1/10 of blicksize in setquota

### 7.0.0.b-2014100802.mr (Wed Oct 08 2014)

- cleanup code (appear warning/error in debug mode)
- fix restart dns (because no prevent for execute rndc)
- fix lighttpd configs (missing pid)
- mod ips, dns master and dns slave depend on server
- remove getIpfromARecord() in dnslib.php (because move to lib.php)
- mod getAllClientList() depend on server
- fix/mod kloxo.init related to reload
- add kloxo, lighttpd, mysql, nginx and xinetd in restart process
- add force-restart script
- fix in dns__lib.php (bug in domain appear)
- mod redirect in hiawatha config

### 7.0.0.b-2014100709.mr (Tue Oct 07 2014)

- fix execute 'rndc reload' if running 'restart-all'
- mod restart and process script
- fix hiawatha.init
- fix ftpuser (use 'useradd' instead 'useradd' + 'usermod' in fix process)
- add php-fpm and mod hiawatha and httpd restart process
- add restart-syslog
- add restart-syslog to restart-all
- mod to watchdog to inteprete '__driver_' to 'restart-'

### 7.0.0.b-2014100706.mr (Tue Oct 07 2014)

- fix upcp (related to php52s/php53s)
- make short text for 'Copy all contents...'
- fix nsd because nsd.db must under nsd:nsd
- fix nsd4.conf (need add control-interface and port for certain OS environment)
- modified nsd restart process
- move mydns.sql (because wrong place)

### 7.0.0.b-2014100704.mr (Tue Oct 07 2014)

- fix 'restart' process in nsd configs
- fix mydns tpl (still not work)
- more customize for restart process
- fix/mod restart scripts
- back to use 'restart' instead graceful/condrestart in restart process
- fix nginx back in webcache (need overwrite default.conf in /etc/conf.d)

### 7.0.0.b-2014100701.mr (Tue Oct 07 2014)

- prepare for mydns (need testing)
- mod reserved.lst because certain dirs move to /opt/configs
- disable removeotherdrives process until better approach (problem with proxy and without class)

### 7.0.0.b-2014100601.mr (Mon Oct 06 2014)

- disable maradns (possible change to mydns)
- reconstruct dns configs (all with the same 'model')
- fix pdns in service list

### 7.0.0.b-2014100502.mr (Sun Oct 05 2014)

- set list.transfer as latest process in fixdns
- mod djbdns to able as master and slave dns
- set return not exists dnsslave_tmp dir
- fix 'warning' if not exists slave file in djbdns
- fix dns configs if djbdns not exists
- fix pdns configs if slave not set

### 7.0.0.b-2014100404.mr (Sat Oct 04 2014)

- use 'nsd-control' instead 'nsd' service for update domains
- fix directory chown of bind
- add log in bind
- fix rndc config
- use 'text' instead 'raw' format for bind slave files
- fix 'restart' dns service (check service exists or not)
- make simple for dns record display
- remove rndc.conf if exists
- fix entry for mx record

### 7.0.0.b-2014100402.mr (Sat Oct 04 2014)

- fix initial nsd in cleanup process
- add 'fix-missing-admin' script
- fix copy nsd.conf in cleanup process
- use php instead bash for fix-missing-admin
- change maindomain_nun, pserver_num and vps_num to unlimited
- fix cname/fcname in dns configs (forgot end dot)

### 7.0.0.b-2014100303.mr (Fri Oct 03 2014)

- remove ddns (old var) from domains.conf.tpl in dns configs
- use strpos instead cse (custom function) in domains.conf.tpl in dns configs
- remove fixphpfm (because include in fixphp)
- fix dns__lib.php (missing '{' in after 'else')
- fix list.slave.conf.tpl in nsd

### 7.0.0.b-2014100206.mr (Thu Oct 02 2014)

- add monkey in web and knot in dns list
- mod appear ns record if original data is '__base__'
- fix uninstall maradns
- mod nsd configs for accept nsd 4.1.0
- fix nsd configs (nsd-control from v4 not work if no setting for remote-control
- fix nginx configs (related to port)

### 7.0.0.b-2014100202.mr (Thu Oct 02 2014)

- fix dns configs to handle 'old' data in 'ns records'

### 7.0.0.b-2014100201.mr (Thu Oct 02 2014)

- fix httpd configs related to php-type
- fix pdns when installed

### 7.0.0.b-2014100110.mr (Wed Oct 01 2014)

- replace wrong domains.conf.tpl for pdns
- mod domains.conf.tpl for pdns
- back to use 'work' domains.conf.tpl of pdns

### 7.0.0.b-2014100108.mr (Wed Oct 01 2014)

- fix curl execute in panel (add CURLOPT_SSLVERSION and CURLOPT_SSL_CIPHER_LIST)
- remove certain useless files
- mod dns configs and data entry to accept 'delegate' dns for subdomain to other server
- fix urltookit to urltoolkit in defaults.conf.tpl of hiawatha
- also add block_shellshock urltoolkit for hiawatha for panel
- fix dns configs if using more than 2 ns
- fix dns configs if delete ns and then create new on

### 7.0.0.b-2014100103.mr (Wed Oct 01 2014)

- fix trimming input data (with use new trimming function)
- fix validate_hostname (with accept '__base__')
- fix phpinilib.php error in debug related to 'multiple_php_ready'
- fix web/web__lib.php error in debug for add domain

### 7.0.0.b-2014093002.mr (Tue Sep 30 2014)

- add block_shellshock urltoolkit for hiawatha
- add 'screen' in install process
- change timezone will be symlink instead copy file
- default php timezone based on /etc/localtime (after symlink)
- fix validate for 'ns records'

### 7.0.0.b-2014092904.mr (Mon Sep 29 2014)

- fix missing nolog var in tmpupdatecleanup.php
- set 'recursive no' in bind
- merge validate_ipaddress and validate_ip_address
- fix validate_ipaddress
- prepare 'new' demo (admin just set 'as demo' for certain client)
- remove indexcontent.php
- fix demoinit.php

### 7.0.0.b-2014092902.mr (Mon Sep 29 2014)

- fix bind (related to named/rndc.conf location)
- fix trim for input (no action for multidimensional array)
- fix delete .flg files in generallib.php

### 7.0.0.b-2014092804.mr (Sun Sep 28 2014)

- disable print_r in mailtoticket.php (only for debug purpose)
- use general validate functions in lib.php instead validate directly in dnsbaselib and weblib
- trim inputs in add/update functions (declare in lxclass.php)

### 7.0.0.b-2014092803.mr (Sun Sep 28 2014)

- make simple where delete all slave dns before re-create
- ready for bind, nsd and pdns
- fix bind tpl script
- fix pdns (for awhile always delete before add/update)

### 7.0.0.b-2014092702.mr (Sat Sep 27 2014)

- fix allow-transfer for bind, djbdns and maradns
- fix add to sysctl.conf
- mod fix.inc to appear --target
- mod set-fs
- remove double declare for action in dns__lib.php
- mod README.md
- fix qmail info in sysinfo and component list in panel

### 7.0.0.b-2014092607.mr (Fri Sep 26 2014)

- add 'secondary/slave dns' feature (already testing for bind and nsd)
- add fixdns to dnsslavelib.php
- disable php-cgi checking in cleanup process
- mod bind and nsd for slave dns
- add 'blank' list.slave.conf.tpl for djbdns
- change createRestartFile to use 'restart' script instead specific driver
- modified validate_domain_name to accept bypass parameter
- hidden stop process for kloxo-wrapper

### 7.0.0.b-2014092602.mr (Fri Sep 26 2014)

- fix create new mailaccount
- mod note for phpm-installer
- fix bind related to allow-notify

### 7.0.0.b-2014092501.mr (Thu Sep 25 2014)

- fix user-logo.png link in 'user logo'
- make smaller height of logo images (from 60 to 40)
- add allow-notify for bind
- fix nsd related to different version
- fix ftpuser if 'docroot' not exists
- fix mailaccount related to 'garbage' account
- back to use 'long' keepalive for hiawatha for panel

### 7.0.0.b-2014092203.mr (Mon Sep 22 2014)

- fix execute set-mysql-default in install process in setup.sh
- fix xinetd (need install if not exists)
- decrease maxclients in apache mpm to 400 (max for serverlimit 16)

### 7.0.0.b-2014092103.mr (Sun Sep 21 2014)

- use the same source for timezone for php and system
- mod restore.php info
- add fixdnschangeip (possible change ip in dns)
- add missing fastcgi_cache_key in nginx
- change nginx.init based latest version
- fix web/web__lib.php for cp
- back to execute disable-mysql-aio in install process
- disable-mysql-aio only process for openvz only
- remove copy init file in defaults.conf.tpl for web

### 7.0.0.b-2014092002.mr (Sat Sep 20 2014)

- remove certain unwanted files
- move certain css class/id from each skin to common.css
- add certain css class
- fix for default timezone in php

### 7.0.0.b-2014091902.mr (Fri Sep 19 2014)

- move function.sh to ./sbin dir (the same dir for kloxo-wrapper.sh
- delete process.php (because for windows)
- mod installer.php for hostname ip
- create set_restart() and execute in dosyncToSystemPost() only
- possible use custom.kloxo.php (execute in function.sh)
- mod list.master.conf.tpl (for dns configs) related to 'restart'
- fix s.gif url from extjs.com to current url
- move certain style to css (make easy to customize for .css and js)

### 7.0.0.b-2014091704.mr (Wed Sep 17 2014)

- add mysql in services list
- create lxadmin dir in fixmail-all if not exists
- mod fixrepo; add set-fs (increase open files limit in sysctl and limit)
- add set-hosts (add declare hostname in /etc/hosts)
- fix install process (especially for update from lower version)
- disable 'yum clean all' in install process (make faster)
- fix set-hosts script (issue related to CentOS 6)

### 7.0.0.b-2014091702.mr (Wed Sep 17 2014)

- fix isRpmInstalled (taken from 6.5.0)
- move for create symlink for /script from installer.php to setup/installer.sh
- change skin-set-all-client to skin-set-for-all
- add execute skin-set-for-all in setup/installer.sh

### 7.0.0.b-2014091602.mr (Tue Sep 16 2014)

- fix installer.php (related to rpm installed)
- remove libmhash (because using mhash) in install process

### 7.0.0.b-2014091601.mr (Tue Sep 16 2014)

- fix serverweb (back to use 'old style')
- remove pdns install in PreparePowerdnsDb
- back to use lxshell_return in isRpmInstalled

### 7.0.0.b-2014091501.mr (Mon Sep 15 2014)

- add 'Header unset ETag' and 'FileETag None' in httpd.conf (according to yslow)
- fix djbdns configs if djbdns not install or portable
- dns configs created for all drivers now (like web driver do)
- fix dns and web configs for add/delete domains
- fix multiple_php_install
- fix getDirIndex in web
- fix isRpmInstalled
- mod php-branch script

### 7.0.0.b-2014091403.mr (Sun Sep 14 2014)

- remove default.ca/program.ca because relatef to lxlabs.com (expired)
- fix fixweb (fixweb for domain/client will NOT delete all domains configs)
- add Rainloop webmail to install in cleanup process

### 7.0.0.b-2014091402.mr (Sun Sep 14 2014)

- fix ssl issue in hiawatha (not work domain ssl with subjectAltName/v3_req)
- mod to not copy .ca from default.ca if .ca not exists

### 7.0.0.b-2014091401.mr (Sun Sep 14 2014)

- use always 'none' driver for serverweb
- set stats always protected
- add getDirIndex (nginx still in progress)
- fix in linuxfslib.php
- fix execbackupphp (use string instead array)

### 7.0.0.b-2014091204.mr (Fri Sep 12 2014)

- mod defaults.conf.tpl for apache
- fix missing SSLEngine on in defaults.conf.tpl for apache
- mod using 'static' password to random password for postmaster
- disable 'Reply-To' in smessagelib.php
- set possible running changeport.php in slave (use by kloxo.init)
- fix hiawatha configs related to ssl
- change lxshell_return to exec for rm in certain pages
- fix setCopyErrorPages
- mod hiawatha.conf.base
- move SSLCompression from virtualhost to httpd.conf (fix issue in centos 5)
- fix changeport.php (enable initProgram)

### 7.0.0.b-2014091103.mr (Thu Sep 11 2014)

- disable MinSSLversion in hiawatha (because not work)

### 7.0.0.b-2014091103.mr (Thu Sep 11 2014)

- mod packages branch list
- move setCopyErrorPages process to top setInitialServices
- mod hiawatha.conf.base (hiawatha for panel base config)
- add specific parameter for ssl in web configs
- fix wrong defaults.conf.tpl for nginx

### 7.0.0.b-2014091101.mr (Thu Sep 11 2014)

- fix missing process from copy dirprotect_stats.conf in nginx and lighttpd
- mod hiawatha configs (also add 'SecureURL = no')
- add and execute setCopyErrorPages in cleanup

### 7.0.0.b-2014091001.mr (Wed Sep 10 2014)

- optimize cleanup proses (make faster)
- back to use rm instead 'rm' because lxshell_ already add ''
- mod fixweb to delete 1x domains configs
- use graceful instead restart for httpd
- hiawatha possible running in normal install or 'portable' version
- add select 'timezone' for server/client level
- change io process from all 'nice' to 'ionice'
- fix installer.php (missing var in rm_if_exists)

### 7.0.0.b-2014090905.mr (Tue Sep 09 2014)

- disable chown/chmod in fixweb to make faster (using fix-chownchmod for this purpose)
- fix mysql-convert and mysql-optimize script
- add set.web.lst (but delete set.httpd.lst and set.nginx.lst)
- add vpopmail_fail2drop.pl to protect brute-force for mail

### 7.0.0.b-2014090903.mr (Tue Sep 09 2014)

- fix switch-php-fpm
- fix default_index.php
- change from static user_sql_manager.flg to 'include' sqlmgr.php for 'sql manager' url

### 7.0.0.b-2014090902.mr (Tue Sep 09 2014)

- fix kloxo.init and phpm-installer
- add alternative sql manager url

### 7.0.0.b-2014090802.mr (Mon Sep 08 2014)

- fix phpmyadmin link
- run 'upcp -y' will be set skin as 'simplicity' skin (to make sure update from 6.5.0)

### 7.0.0.b-2014090801.mr (Mon Sep 08 2014)

- fix kloxo.init (missing 'special' php.ini path)

### 7.0.0.b-2014090601.mr (Sun Sep 07 2014)

- change to use \\rm to 'rm' for temporary unalias for cp/mv/rm; fix kloxo port for thirdparty if not 7778/7777
- fix enable/disable perl in hiawatha
- fix ssl port in nginx
- enable secondary-dns in nsd (in progress)
- mod ip list not include hostname ip in dns (related to notify)
- input always trimming before validated
- fix php5.fcgi path
- make more info and short meassage in log (with remove path)
- remove fixservice in cleanup because always 'chkconfig on' for all services
- remove detect for custom php.ini in kloxo.init but add copy custom to php.ini in fixlxphpexe
- disable process 'disable-alias' in cleanup; mod clearcache

### 7.0.0.b-2014090503.mr (Fri Sep 05 2014)

- fix fixwebcache
- back to use notify insted restart in nsd
- remove double session.save_path in php53-fpm-pool.conf.tpl
- remove setup init in webcache in defaults.conf.tpl
- fix switch in dns
- fix syslog.conf for /var/log
- mod for accept for nsd 4.x in list.master.conf.tpl
- change \rm to rm in all sh script
- fix packer.sh
- fix dns install for enable 'chkconfig on'

### 7.0.0.b-2014090401.mr (Thu Sep 04 2014)

- change back \rm to rm because conflict with \r escape char (fortunely still work without \)
- remove watchdog for syslog (port 514 not work)
- fix initial value for php.ini (change null to '')
- fix public_html symlink in weblib
- fix disable log in rsyslog/syslog config
- fix get_kloxo_port
- fix fixwebcache
- back to use notify insted restart in nsd
- remove double session.save_path in php53-fpm-pool.conf.tpl
- remove setup init in webcache in defaults.conf.tpl
- fix switch in dns
- fix syslog.conf for /var/log

### 7.0.0.b-2014090301.mr (Wed Sep 03 2014)

- fix all cp/rm/mv to their 'temporal unalias'
- in program changedriver (need running dns-, webcache- and web-installer)
- fix all dir from /home to /opt/configs for all configs
- mod all dns_lib
- fix mailqueuelib if mail no exists
- mod to domainkey.txt always overwrite
- fix appear 'switch programs', especially for fresh install issue
- disable reversedns (because only ready for bind)

### 7.0.0.b-2014082703.mr (Wed Aug 27 2014)

- fix dns issue when switch
- fix fixdns and fixweb when using 'none' driver
- add unalias for cp/mv/rm in upcp

### 7.0.0.b-2014082701.mr (Wed Aug 27 2014)

- remove magic_quote and safe_mode from php-fpm config and php ini
- fix restore and switchserver
- mod fixwebcache; fix web conf.tpl
- fix url path in nginx (generic and awstats)
- fix for webcache switch related to web
- fix web__lib related to webdriverlist
- fix common.inc related to 'none' driver
- mod packer.sh
- fix fixlxphpexe
- back to use 'listen' include for nginx

### 7.0.0.b-2014082402.mr (Sun Aug 24 2014)

- fix defaults.conf.tpl logic for hiawatha.conf copy
- remove cp/mv/rm alias
- set default.conf.tpl for copy init and main conf
- fix defaults.conf.tpl
- mod setUpdateConfigWithVersionCheck()
- disable copy conf file in setCopyWebConfFiles() to /etc because move to defaults.conf.tpl

### 7.0.0.b-2014082301.mr (Sat Aug 23 2014)

- mod webserver init with add custom file inside /etc/sysconfig
- fix hiawatha init to possible using custom hiawatha.conf

### 7.0.0.b-2014082201.mr (Fri Aug 22 2014)

- prepare use mod_macro for apache
- set to use 'real' kloxo port in web configs instead use 7778/7777 only
- move /home/openssl to /opt/configs

### 7.0.0.b-2014082105.mr (Thu Aug 21 2014)

- set 'conflicts' for kloxomr-6.5.0 and 'obsoletes' for kloxomr-6.5.1

### 7.0.0.b-2014082104.mr (Thu Aug 21 2014)

- fix hiawatha-proxy
- moving cp. declare in nginx and lighttpd (like hiawatha do)

### 7.0.0.b-2014082102.mr (Thu Aug 21 2014)

- fix ReleaseNote
- fix missing mod_define in apache
- fix Update Info

### 7.0.0.b-2014082002.mr (Wed Aug 20 2014)

- mod packer.sh for accept kloxomr7

### 7.0.0.b-2014082001.mr (Wed Aug 20 2014)

- change version from 6.5.1 to 7.0.0
- change/move driver configs (web, webcache and dns) from /home to /opt/configs
- optimize web config to make faster fixweb and switch driver
- create all web configs together and switch web just modified 'defaults' portion
- use mod_define to make apache able set 'var'
- optimize speed for fixftpuser (2x)
- remove certain codes for windows os
- add webcache driver in service list
- fix list display if click sort
- add 'custom' php.ini for kloxo init
- add 'timewatch' for certain fix script

### 6.5.1.b-2014072901.mr (Tue Jul 29 2014)

- fix installing process related to phpm-installer

### 6.5.1.b-2014072801.mr (Mon Jul 28 2014)

- change system() to exec() in fixvpop.php
- fix hiawatha service when running cleanup if hiawatha as active webserver

### 6.5.1.b-2014072701.mr (Sun Jul 27 2014)

- just using phpm-installer for install all phpXYm/s
- mod/fix all php bin for general path
- change session path based on user
- validate username and password for add ftp user

### 6.5.1.b-2014070901.mr (Wed Jul 09 2014)

- auto chown 755 for pl/cgi/py/rb if upload or fix-chownchmod

### 6.5.1.b-2014070801.mr (Tue Jul 08 2014)

- fix update ssh script
- disable .htaccess fix in fixweb

### 6.5.1.b-2014070704.mr (Mon Jul 07 2014)

- check mod_perl in cleanup process
- back to use previous reverseproxy for nginx and hiawatha
- add keep-alive and reduce timeout to 90 in hiawatha-proxy
- change reverseproxy passing from all to except pl/cgi/py/rb/shmtl (because handle by cgi-wrapper)
- reduce timeout value in nginx

### 6.5.1.b-2014070601.mr (Sun Jul 06 2014)

- use hiawatha's cgi-wrapper instead apache's cgi in hiawatha-proxy
- mod/add index order (accept index.cgi also)
- using mod_perl instead mod_cgi for apache
- enable taint in perl for security reason

### 6.5.1.b-2014070402.mr (Fri Jul 04 2014)

- fix enable-cgi (also warning 'not implementing yet' in nginx)

### 6.5.1.b-2014070401.mr (Fri Jul 04 2014)

- fix/mod certain throw messages
- mod htmltextarea space
- enable/disable cgi for webserver
- use cgi-wrapper for hiawatha
- change openbasedir for 'apache' user
- use directly cgi-assign to perl instead perlsuexec for lighttpd
- fix lighttpd conf.tpl

### 6.5.1.b-2014063001.mr (Mon Jun 30 2014)

- remove 'output_buffering = 4096' and stay with '= off' in php.ini
- change throw value from array to string

### 6.5.1.b-2014062702.mr (Fri Jun 27 2014)

- fix throw for mailing list
- convert to string if message as array (related to getThrow)
- mod certain throw messages

### 6.5.1.b-2014062701.mr (Fri Jun 27 2014)

- use phpXYm for suphp in secondary php
- fix ssl reloop for nginx-proxy
- fix ckeditor for only save body content
- enable phar, intl, geoip, fileinfo and tidy module in php fix logrotate

### 6.5.1.b-2014062401.mr (Tue Jun 24 2014)

- add getThrow() for translate throw message
- update all throw message with getThrow; fix logrotate config

### 6.5.1.b-2014062102.mr (Sat Jun 21 2014)

- change addon-ckeditor/fckeditor to editor-ckeditor/fckeditor
- change addon-fckeditor to editor-*

### 6.5.1.b-2014062101.mr (Sat Jun 21 2014)

- remove obsolete for fckeditor and ckeditor
- remove fckeditor/ckeditor from core code
- install ckeditor if not exists when running cleanup
- add provide for fckeditor and ckeditor

### 6.5.1.b-2014062001.mr (Fri Jun 20 2014)

- add $this->write in postUpdate to make sure db write before next process
- prepare 'multiple php' (but still in progrss)

### 6.5.1.b-2014061903.mr (Thu Jun 19 2014)

- add try-catch in default_index.php
- fix fixlxphpexe
- also run rkhunter update in install process
- use ckeditor instead fckeditor if installed
- include ckeditor

### 6.5.1.b-2014061701.mr (Tue Jun 17 2014)

- fix 'phptype_not_set' alert
- increasing connection in nginx and hiawatha until 10x (for high-traffic)
- use 'new' permalink for lighttpd
- prepare 'new' ip validate

### 6.5.1.b-2014061502.mr (Sun Jun 15 2014)

- use callInBackground for lx_mail
- possible disable send notify for quota with flag
- add 'disable notify' for 'disable policy'
- add 'enable cron for all' in 'general settings'
- fix change 'webmail system default' (no need 2x click update again)
- increase types_hash_bucket_size in nginx for long domain name issue

### 6.5.1.b-2014061403.mr (Sat Jun 14 2014)

- add default.conf for nginx for prevent when using webcache in front of nginx
- prevent delete main ftpuser
- fix 'server alias' for wildcard
- fix web_lib.php related to nginx

### 6.5.1.b-2014061101.mr (Wed Jun 11 2014)

- mod scavenge send mail with Y/m/d format; mod djbdns tpl
- add some env to php53-fpm-pool.conf.tpl
- prepare phpm-fpm template
- mod kloxo.init to accept custom sh/conf/ini
- upload 'new' phpm-fpm template
- mod php-fpm-part.conf.tpl
- use symlink instead copy from pscript to /script
- mod php-fpm tpl to change php_value to php_flag for on/off
- change php_active to kloxo_php_active and adjustment for kloxo.init and fixlxphpexe

### 6.5.1.b-2014060802.mr (Sun Jun 08 2014)

- add enable gzip in hiawatha
- change X-Forwarded-For in nginx
- mod php-fpm.conf.tpl (fpm for php52)
- enable send mail for scavenge
- fix loqout in feather skin
- add expire in hiawatha for panel
- add php-fpm conf/init/sh in php52m/php52s
- fix scavenge.php

### 6.5.1.b-2014060401.mr (Wed Jun 04 2014)

- change CGIHandler from php to pl in hiawatha
- enable alias to cgi-bin in hiawatha; fix cron for client
- change root@ to admin@
- fix 'go' button in list
- set cursor to certain css tag
- mod htmllib related to cursor
- change 'idle-timeout' for 'FastCGIExternalServer' of mod_fastcgi from 180 to 90

### 6.5.1.b-2014053003.mr (Fri May 30 2014)

- fix header in lx_mail()
- mod to possible stmprelay/smarthost to outside smtp server
- mod to enable cron for users if exists '/usr/local/lxlabs/kloxo/etc/flag/enablecronforall.flg' file
- mod apache disable dirlist
- mod message for cron list
- fix nginx config (inactivate for 'disable_symlinks') because troube with static file path
- change sender from kloxo@ to root@

### 6.5.1.b-2014052401.mr (Sat May 24 2014)

- mod/fix lx_mail() to accept utf8 and html and remove pre from message

### 6.5.1.b-2014052103.mr (Wed May 21 2014)

- set /tmp dir as upload and save dir for phpmyadmin
- add 'max_input_vars' in php.ini and php-fpm config (set '3000' as default)
- add 'max input vars' in 'advanced php configure'
- send message as html and with utf8 charset

### 6.5.1.b-2014051901.mr (Mon May 19 2014)

- add www.conf (blank) for php-fpm because latest php always create this file
- fix crf_token
- delete unused certain php files

### 6.5.0.f-2014051301.mr (Tue May 13 2014)

- add '-pass-header Authorization' in domains.conf.tpl of apache
- fix syncserver in web__lib.php
- prettier php code in certain files

### 6.5.0.f-2014051201.mr (Mon May 12 2014)

- separate warning for php.ini to client and pserver
- feather skin also appear background image (but still not use)
- no convert for '\n' to '
\n' (fix ticket list appear)

### 6.5.0.f-2014051004.mr (Sat May 10 2014)

- change listen.mode from 0660 to 0666 in php-fpm pool
- mod fixmail-all

### 6.5.1.b-2014051002.mr (Sat May 10 2014)

- mod/fix phpm-installer
- mod installatron-install
- fixmail-all also fix chown and chmod of /home/lxadmin/mail
- fixmail-all also checking postfix user and change to vpopmail if exists
- fix/mod installer related to postfix user
- fix php-fpm related to php53/php54 release -28 (add listen.owner, .group and .mode);
- fix/mod installer (related to fix process) and fix-all

### 6.5.1.b-2014050502.mr (Mon May 05 2014)

- fix 'php used' in 'webserver configure' in master-slave environment

### 6.5.1.b-2014050501.mr (Mon May 05 2014)

- fix default_index.php for using login page
- add syncserver for input array (importance for pdns in master-slave environment)
- fix dns class lib (add __construct like web class lib do)
- fix php.ini warning if not set (server and admin level)
- fix 'switch program' fpr master-slave environment
- fix 'webcache' input array in web lib
- fix click in php.ini warning
- change restart httpd in latest step in restart-all/restart-web

### 6.5.1.b-2014043002.mr (Wed Apr 30 2014)

- set/enable phprc in php-cli (fix issue for missing .so in lxphp.exe)

### 6.5.1.b-2014043001.mr (Wed Apr 30 2014)

- set mywebsql as 'sql manager' if exists in 'httpdocs/thirdparty'
- fix installatron-install if webmin exists
- fix php53m-extension-installer (also remove full path of extension)
- fix phpm-config-setup with remove full path of extension

### 6.5.1.b-2014042902.mr (Tue Apr 29 2014)

- increasing MaxUploadSize to 2000 in hiawatha and MaxClients to 1000 in httpd
- fix custombutton not show icon in client
- make possible php54s beside php53s (resolve mariadb 10.0 issue; compile with mysqlnd for '--with-mysqli')

### 6.5.1.b-2014042803.mr (Mon Apr 28 2014)

- overwrite /etc/php.ini in upcp (prevent missing .so in lxphp.exe)
- remove postfix user before re-/install qmail-toaster in upcp
- add install yum-plugin-replace in upcp
- mod phpm-config-setup
- fixmail-all also remove postfix user and install qmail-toaster if not exists
- move 'default' php.ini (because wrong place)

### 6.5.1.b-2014042702.mr (Sun Apr 27 2014)

- move process update mratwork.repo to fixrepo and then execute in upcp (including change $releasever to OS version)

### 6.5.1.b-2014042701.mr (Sun Apr 27 2014)

- fix web config template (forgot open bracket for .ca logic)
- fix upcp for 'mratwork-' execute

### 6.5.1.b-2014042601.mr (Sat Apr 26 2014)

- still trouble with php for openssl (not perfect for SAN) and then mod to minimize problem
- little fix lighttpd config (still problem with ssl)

### 6.5.1.b-2014042401.mr (Thu Apr 24 2014)

- external apps access via post (like phpmyadmin)
- fix to mariadb may not work if /var/lib/mysqltmp not exists
- implementing SAN for domain-based ssl cert
- cleanup will be copy openssl.cnf.tpl to /home/openssl
- fix button appear for client login

### 6.5.1.b-2014042202.mr (Tue Apr 22 2014)

- fix reset-mysql-kloxo-password.php
- fix lighttpd conf.tpl related to .ca ssl
- change all lpform method to 'get'

### 6.5.1.b-2014042101.mr (Mon Apr 21 2014)

- disable implementing update/add/delete in get for 'csrf token' because too much exception

### 6.5.1.b-2014042008.mr (Sun Apr 20 2014)

- set 'cgi.rfc2616_headers = 0' in phpm series (because problem with mod_fastcgi)
- mod mysql-to-mariadb.php and mariadb-to-mysql.php
- change extension from mysql to mysqli in phpmyadmin_config.inc.php
- fix issue related to csrf for service action
- fix restore process for 'csrf token' (change 'get' to 'post')

### 6.5.1.b-2014042004.mr (Sun Apr 20 2014)

- add/delete domain-based cert also fixweb and restart-web
- disable php module flag and update in phpinilib.php
- disable setInstallPhpFpm when switch web (aka install web driver)

### 6.5.1.b-2014042001.mr (Sun Apr 20 2014)

- add no permit for add/delete/update with get instead post
- prepare domain-based ssl without assign to IP address
- fix delete in list related to 'csrf token'
- fix/change kloxo-php-fpm as root
- fix topbar_right (remove undefined param)
- fix certain form to post (related to 'csrf_token')
- implementing domain-based ssl certificate (delete still not remove files)
- mod web config related to domain-based ssl cert
- change using .crt to .pem in all web config
- fix delete cert also delete cert files
- change is__table to getClass (because the same purpose)
- fix sshconfig ini in show
- mod messagelib.php related to domain-based cert

### 6.5.1.b-2014041802.mr (Fri Apr 18 2014)

- set to make sure all add/delte/update in form must be under post and add csrf_token

### 6.5.1.b-2014041801.mr (Fri Apr 18 2014)

- mod property for 'limit' and 'custombutton'
- fix phpm-config-setup (related to php55m for symlink)

### 6.5.1.b-2014041704.mr (Thu Apr 17 2014)

- disable fix_self_ssl in update process
- enable for disable_functions in phpXYm php.ini
- add mysql56u in set.mysql.lst
- fix ftpuser if docroot not exists
- add/mod message in messagelib
- mod/fix phpini initialValue
- fix frame_left for client level (disable quickaction logic)
- mod/fix for handle update situation for mysql and php53s
- 'yum remove' for php53s also 'rpm -e --noscripts' for php53s-fpm

### 6.5.1.b-2014041602.mr (Wed Apr 16 2014)

- separate delete warning for customer and admin/reseller
- fix/mod for detect use yum and rpm in sh script

### 6.5.1.b-2014041601.mr (Wed Apr 16 2014)

- fix/mod 'nice' params
- mod csrf_token logic
- update and phpm install not work in panel (found yum problem in centos 6 64bit)

### 6.5.1.b-2014041504.mr (Tue Apr 15 2014)

- fix display issue related to token validate where only add/update/delete with 'post' method
- fix phpini because getlist for pserver only work under admin and then change to parent
- add warning in deletion list
- move deletion warning to top because possible not appear in long list

### 6.5.1.b-2014041501.mr (Tue Apr 15 2014)

- validate token able to disable
- fix add cron job (update still not work)

### 6.5.1.b-2014041402.mr (Mon Apr 14 2014)

- recompile with fix wrong domainlib.php file

### 6.5.1.b-2014041401.mr (Mon Apr 14 2014)

- remove mysql from mysql.lst and for only mysql55 beside mariadb
- mariadb-to-mysql will be convert to mysql55
- disable installapp reference in ddatabaselib
- set initPhpIni() only use values from setUpINitialValues()
- add missing 'selected' keyword
- update 'limit' will be update will execute fixphp
- fix domainlib issue (isOn change to '=== 'on') in client domain
- to make sure quota value in 'float' format
- fix preAdd() with add params
- fix 'checked' in display

### 6.5.1.b-2014041303.mr (Sun Apr 13 2014)

- fix php53s.ini for using special setting for panel

### 6.5.1.b-2014041301.mr (Sun Apr 13 2014)

- add isTokenMatch() for 'csrf token'
- change protect from 'remote post' to 'csrf token'
- fix htaccess.tpl and php.ini.tpl (add 'default value')
- fix issue if select 'multiple php' and user click 'php configure' in domain
- mod phpm-instaler/-config-setup
- to make sure '/tmp/multiple_php_install.tmp' owned by root

### 6.5.1.b-2014041203.mr (Sat Apr 12 2014)

- mod/fix langkeyword/message data
- mod multiselect list able to use keyword for title
- change redirect for 'change owner' from 'a=resource' to 'new client'

### 6.5.1.b-2014041201.mr (Sat Apr 12 2014)

- add ftpChangeOwner() because call by web__lib
- add db_set_value()
- fix do_remote_exec() with create remote object if not set (effect for 'change owner')
- fix ftpuser owner for 'change owner'

### 6.5.1.b-2014041104.mr (Fri Apr 11 2014)

- mod/update phpm-installer/-config-setup (all config parts move to -config-setup)
- mod 'searchall' button
- set 'php configure' not appear if not enable 'multiple php'
- fix preUpdate in objectactionlib.php
- add fix-yum-cache
- change from 300 to 30 for timeout in /etc/yum.conf

### 6.5.1.b-2014041101.mr (Fri Apr 11 2014)

- add more disable modules for phpm-installer (minimize trouble when using php52 as php-branch)

### 6.5.1.b-2014041003.mr (Thu Apr 10 2014)

- forgot submit mod lxclass.php (related to preAdd()/preUpdate())
- mod preUpdate in serverweblib.php (consistence with declare preUpdate in lxclass)

### 6.5.1.b-2014041001.mr (Thu Apr 10 2014)

- add new preAdd()/preUpdate() function to process before add()/update()
- install 'multiple php' runnind well now from panel and in background process (need a trick)
- mod lxa.js related to multiselct

### 6.5.1.b-2014040901.mr (Wed Apr 09 2014)

- move mratwork-
	196	---------- * detect from cleanup to upcp

### 6.5.1.b-2014040801.mr (Tue Apr 08 2014)

- fix directory protect for hiawatha (forgot add 'basic' in 'passwordfile' param)
- add log for phpm install process
- mod php5.fcgi
- prepare install 'multiple php' via panel
- fix switch-php-fpm
- add option in 'webserver configure' for switch-php-fpm ('multiple php install' still not work)
- add 'Fix - PHP' in 'command center'
- change form color

### 6.5.1.b-2014040601.mr (Sun Apr 06 2014)

- also disable magic_quotes, register_long_arraya and register_globalsin htaccess
- disable allow_call_time_pass_reference in php
- prepare add token in add/delete/update form
- add validate for 'server alias'
- better 'die' display
- mod xprint for display data of object, array or string
- implementing protect from 'remote post' instead using 'csrf token' (need more investigate)
- back to declare all php module in apache because selected module need fixweb every php-type changed
- fix mod_rewrite issue in apache
- fix chmod for php5.fcgi
- fix isRemotePost (change 'SERVER_NAME' to 'HTTP_HOST')
- fix directory declare in hiawatha related to directory protect
- fix phpm-installer (wrong declare for redis)

### 6.5.1.b-2014040405.mr (Fri Apr 04 2014)

- fix php53s-installer

### 6.5.1.b-2014040404.mr (Fri Apr 04 2014)

- enable unzip in docroot (no warning)
- back to set 'cgi.rfc2616_headers = 0' because trouble in wordpress in apache/-proxy
- back to not use ' -pass-header Authorization' in mod_fastcgi because must be 'cgi.rfc2616_headers = 0'

### 6.5.1.b-2014040403.mr (Fri Apr 04 2014)

- now install with php53s by default

### 6.5.1.b-2014040402.mr (Fri Apr 04 2014)

- remove pear channel update

### 6.5.1.b-2014040401.mr (Fri Apr 04 2014)

- back to use 'cgi.fix_pathinfo = 1' because many apps not work if 0
- magic_quotes, register_globals and safe_mode always disable (make simple for multiple php)
- remove double post_max_size
- php*-enchant also not enable (bug issue)
- set no options for magic_quotes, register_globals and safe_mode in panel
- mod kloxo.init
- mod sh script for php
- php52s and php53s only for panel and use m version for general purpose (use switch-php-fpm)
- symlink /usr/lib64/php to /usr/lib/php (make simple for 64bit)
- mod fixlxphpexe, cleanup, cleanup-simple and cleanup-nokloxorestart
- mod packer.sh

### 6.5.1.b-2014040201.mr (Wed Apr 02 2014)

- fix php.ini.tpl
- fixlxphpexe (better rm for symlink to make sure)
- when install in 64bit, phpm-installer only install 64bit dependencies

### 6.5.1.b-2014040109.mr (Tue Apr 01 2014)

- fix php.ini.tpl
- php52s/php53s install using 'standard' php52/php53u

### 6.5.1.b-2014040108.mr (Tue Apr 01 2014)

- fix run cleanup after update Kloxo-MR
- fix packer.sh

### 6.5.1.b-2014040106.mr (Tue Apr 01 2014)

- running fixphp include fix extension_dir path (crucial for multiple php system)

### 6.5.1.b-2014040105.mr (Tue Apr 01 2014)

- run fixlxphpexe include copy kloxo.init (guarantee always new)
- add missing kloxo-php-fpm.conf (special)

### 6.5.1.b-2014040104.mr (Tue Apr 01 2014)

- fix typo in php53-fpm-pool.conf.tpl
- disable 'customemode' option in 'appearance'
- fix php.conf to inactive if switch to php-fpm in httpd
- disable 'export PHPRC=' in all php script (not needed)
- change php-error.log name depend on multiple php-type
- always use 'extension_dir' in all php.ini to mininize wrong php modules loaded
- add switch-php-fpm to make possible one of 'multiple php' for single purpose
- add reset-mysql-kloxo-password (alternative for fix-program-mysql)
- correct double param in php53-fpm-pool.conf.tpl
- correct/mod extension_dir in php.ini.tpl/php.ini.tpl
- fix php52s-installer
- also fix php-fpm-pool.conf.tpl in future phpcfg for typo and double param

### 6.5.1.b-2014033004.mr (Sun Mar 30 2014)

- fix fix-qmail-assign
- running 'fix-qmail-assign' also create '.qmail-default' if not exists

### 6.5.1.b-2014033003.mr (Sun Mar 30 2014)

- disable 'PATH_TRANSLATED' in nginx config because set 'cgi.fix_pathinfo = 0' in php.ini
- fix url for 'driver' list
- mod related to 'createListAddForm' and 'addListForm' function
- fix related to 'click here to add' in 'printListAddForm'
- change color for active tab and breadcumb
- fix/mod fix-qmail-assign to more info
- add 'help' in 'feather' (simple skin already exists)
- fix-chownchmod also fix mail dirs/files

### 6.5.1.b-2014032907.mr (Sat Mar 29 2014)

- back to previous idea for phpXYm script
- fix extension_dir for phpXYm
- separated config-setup to file from phpm-installer
- add run phpm-config-setup to cleanup

### 6.5.1.b-2014032905.mr (Sat Mar 29 2014)

- fix/mod sshauthorizedkey
- fix appear for 'selectshow'
- set to 'no disabled' for 'admin'
- mod/fix script for phpXYm and phpXYm
- already appear 'multiple php' options but unfinished yet
- fix/mod 'StW' type display (using textarea instead table)
- make simple sshauthorizedkey code
- change 'multiple php enable' to 'multiple php ready'
- prepare for 'multiple php' template script
- back to php.ini under web but only for 'php selected' and enable 'multiple php'
- add/change 'alert' if php.ini not set under 'pserver'

### 6.5.1.b-2014032502.mr (Tue Mar 25 2014)

- fix detect php 5.4/5.5 which 'php -v' not work if using php 5.2/5.3 ini
- add 'try again' in 'alert' for fail to add domain
- remove 'disable installapp' in 'general'
- fix to setup roundcube 1.0.rc
- fix copy php-fpm.conf

### 6.5.1.b-2014032501.mr (Tue Mar 25 2014)

- fix image/font appear depend on button type
- change to lxguardhit appear different column for fail and success
- fix permissions (from 0600 to 644) for user-logo
- fix version check
- fix restart if hiawatha status as not running
- gen-hashed.sh (for qmail user assign (based on 'real' bash)

### 6.5.1.b-2014032301.mr (Sun Mar 23 2014)

- mod how-to-install.txt
- mod/change phpXY-installer
- create log file for escape segfault in install
- php.ini now based on user-level
- able setting php max children in resource plan
- mod fixweb/fixphp to adopt user-level php.ini
- install process with more info/message
- move php.ini/php5.fcgi from /home/httpd/ to /home/kloxo/client/
- fix fcgid to config adopt user-level php.ini
- max children appear 'unlimited' but intepret as '6'
- fix dns serial
- fix wrong text for load-wrapper calling
- php-fpm config include php configure from panel as 'php_admin_value'
- no admin/domain mode for admin (just appear admin mode)
- fix detect phpversion
- delete client also delete php-fpm config
- fix issue if dnssyncserver not array
- add is_cli() for debug purpose (different appear in cli and cgi mode)
- fix issue in sqlite.php if object not exist
- modified simplicity skin where breadcumb move from top to bottom of tab
- fix mailinglist where wrong url because as object
- fixweb and fixphp is separate process now
- fix fixlxphpexe script
- 2 type of multi-php (phpXYm and phpXYs)
- mod upcp and install scripts
- add options for install (-y/-force, --with-php53s/-53s and --remove-kloxo-database/-r)

### 6.5.1.b-2014031701.mr (Mon Mar 17 2014)

- fix cleanup (no need restart phpXs)
- change to use php52 instead php53 for install step
- fix kloxo.inits
- fix phpXs-installer
- installer have options with '--force'/'-y', '--remove-kloxo-database' and '--use-php53s'
- delete disable-mysql-aio and add set-mysql-default (with more options)

### 6.5.1.b-2014031602.mr (Sun Mar 16 2014)

- add missing default driver content
- phpXs-installer will be detect target php

### 6.5.1.b-2014031601.mr (Sun Mar 16 2014)

- fix dependencies for phpXs-installer
- cleanup also convert special php53s to php53s based on php53u
- fix and also convert php52s in cleanup

### 6.5.1.b-2014031501.mr (Sat Mar 15 2014)

- fix detect phpversion
- mod status appear in simplicity skin
- panel using php53u (via php53s-installer) instead special php53s
- add php*s-installer (prepare for multi-php)
- disable register_long_arrays in php 5.3+ (problem for 5.4+)
- possible execure php-cli with softlimit
- increase listen.backlog in nginx
- install able with '--force' and '--use-php52s' (default using php53s)

### 6.5.1.b-2014031203.mr (Wed Mar 12 2014)

- rename softlimit to softlimit.sample because something wrong under centos 6 64bit (bug?)

### 6.5.1.b-2014031202.mr (Wed Mar 12 2014)

- add 'proxy_set_header X-Forwarded-Protocol $scheme;' to nginx proxy.conf
- change menu file/dir for simplicity skin
- add soflimit in php*-cli.sh (automatically for lxphp.exe)
- change 'vm.vfs_cache_pressure' value 100 (tend to slow cached memory created)
- pending select php.ini from menu for client in simplicity skin

### 6.5.1.b-2014031002.mr (Mon Mar 10 2014)

- disable dtree for delete client
- fix for delete database username

### 6.5.1.b-2014031001.mr (Mon Mar 10 2014)

- make enable clientname as database username
- fix/mod image to font icon if select 'button_type' as font
- fix initial roundcube
- fix spamdyke config
- add -pass-header Authorization in mod_fastcgi of httpd
- fix for double userdir rule in nginx
- back to user 'default' restart for services
- delete domain also delete docroot if no other domain use the same docroot

### 6.5.1.b-2014030101.mr (Sat Mar 01 2014)

- disable UserDirectory in hiawatha (to purpose userdir but not work)
- disable 'enable_php_fastcgi' in resourceplan
- move php logic for topbar to their php
- add 'wait' in 'status' top bar when click add/update

### 6.5.1.b-2014022801.mr (Fri Feb 28 2014)

- fix tab bar for 'Client Processed Logs' page
- fix tab bar for 'lxguard' page
- fix font-icons in lxguard pages
- move left/right top bar of 'simplicity' to include_one with 'custom rule'
- fix icons in list if using 'image' as 'button type'

### 6.5.1.b-2014022701.mr (Thu Feb 27 2014)

- mod packer.sh with also remove .pid files
- make already appear tab bar
- mod dprint/dprint_r with 'pre'
- add xprint (like dprint without debug mode)
- add database without prefix name and also database username for admin
- fix userdir in httpd with make declare userdir outside 'default' virtualhost

### 6.5.1.b-2014022404.mr (Mon Feb 24 2014)

- add fixftp-all
- add 'standard command' like fixdns in 'command center'
- remove installapp
- fix quota/normal view with more detail
- add more detil in login history but bug still unreseolvced
- remove livesupport (unfinish work original Ligesh)
- fix list pagesize/pagenum
- use
	163	---------- * instead *.lxlabs.com certificate
- upload new certificate

### 6.5.1.b-2014022305.mr (Sun Feb 23 2014)

- fix userdir in httpd
- add fixftp-all
- add 'standard command' like fixdns in 'command center'

### 6.5.1.b-2014022303.mr (Sun Feb 23 2014)

- resubmit certain restart scripts because wrong files

### 6.5.1.b-2014022302.mr (Sun Feb 23 2014)

- mod clock also hour with 2 digit format
- fix all restart and cleanup scripts
- move clock js code to clock.js file
- all include, js and css ready for 'custom rule'
- set Kloxo-MR 6.5.1 as b (beta)

### 6.5.1.a-2014022204.mr (Sat Feb 22 2014)

- fix validate for new client
- set smaller menu, tab-bar and link-path in 'simplicity' skin
- more detail message and validate for add client and database

### 6.5.1.a-2014022202.mr (Sat Feb 22 2014)

- fix fix-userlogo (related to domainroot)
- fix all cleanups
- fix message and ticket (related to send mail) with add

- fix list if data is array (date in cron list still not fix)
- add clock in 'simplicity' top bar
 

### 6.5.1.a-2014022102.mr (Fri Feb 21 2014)

- fix userlogo and default_index.php
- add left-side userlogo in panel
- add 'ticket' in 'simplicity' top bar
- fix non-admin send ticket
- reduce menu text width

### 6.5.1.a-2014022101.mr (Fri Feb 21 2014)

- add alert when click status in 'simplicity' top bar
- change clientname length to 31 (according to centos and pure-ftpd)
- spawn-fcgi only auto-install under php52s in kloxo.init

### 6.5.1.a-2014022002.mr (Thu Feb 20 2014)

- fix display_init
- better status title display in 'simplicity' skin
- restore css for default because wrong css

### 6.5.1.a-2014022002.mr (Thu Feb 20 2014)

- remove double php53s-snmp in php53s-install
- add 'message' and 'status' in 'simplicity' top bar
- add db_get_count() in lib.php
- change "#" to "javascript://" for href in 'simplicity' index.php
- reduce menu container with (800 to 700px) to accept width 1024px display
- move 'status' bar from top-left to top-right

### 6.5.1.a-2014021901.mr (Wed Feb 19 2014)

- fix sysinfo.php
- add squid in webcache list
- change client/database name to 64 chsrs and database username to 16 chars
- vslidate for client/database name and database username
- warning if 'switch program' and 'php-type' not set
- change default php-type to php-fpm_worker
- back to use default web, dns and spam server like 6.5.0 version
- mysql_conver also include change to utf-8 charset in panel (like in command-line)
- change '--- none ---' to '--- No Change ---'
- use db_get_value for php-type value
- fix issue if log not create
- fix getRpmBranchInstalled() if list file not exists
- mod randomString()
- add convert_message() fot handling like '[%_server_%]' and implementing also for error/alert message
- fix drop menu align in simplicity skin
- mod changeport.php for compatible with 6.5.0 and 6.5.1
- kloxo stop also checking php52s and php53s running or not
- installer still using php52s (because less memory usage)
- installer also php53s install and restart services before finish

### 6.5.1.a-2014021502.mr (Sat Feb 15 2014)

- fix issue related to lxphp.exe
- fix restarts and kloxo.init
- set 755 for php*-cli.sh and php*-cgi.sh

### 6.5.1.a-2014021502.mr (Sat Feb 15 2014)

- fix php53s-install (replace php53s to php53s-cli to escape conflict with regular php)
- prepare for random prefix for databasename

### 6.5.1.a-2014021501.mr (Sat Feb 15 2014)

- fix nginx config related to disable_symlinks

### 6.5.1.a-2014021401.mr (Fri Feb 14 2014)

- fix php53s-install and restart-all script

### 6.5.1.a-2014021201.mr (Wed Feb 12 2014)

- add SymLinksIfOwnerMatch to apache config and equivalent param for other webservers
- change dynamic to ondemand for pm in php-fpm
- panel execute under php 5.2 or php 5.3 but install process still in php 5.2
- prepare jailed code (still disabled)
- run php53s-install if want running panel under php 5.3

### 6.5.1.a-2014020301.mr (Mon Feb 03 2014)

- update cron_task (make client only able to list and delete their cron)

### 6.5.1.a-2014020201.mr (Sun Feb 02 2014)

- fix install process (add looping to make sure kloxo database created)

### 6.5.1.a-2014013101.mr (Fri Jan 31 2014)

- kloxo service using spawncgi (make kloxo-phpcgi under lxlabs user like kloxo-hiawatha)
- disable perl until fix hardlinks issue related to perl
- mod permissions update display

### 6.5.1.a-2014012902.mr (Wed Jan 29 2014)

- fix some issues to make better update from Kloxo official

### 6.5.1.a-2014012901.mr (Wed Jan 29 2014)

- fix hiawatha config for dirprotect
- fix docroot where update not work

### 6.5.1.a-2014012802.mr (Tue Jan 28 2014)

- fix kloxo sql
- mod file list column

### 6.5.1.a-2014012801.mr (Tue Jan 28 2014)

- mod kloxo sql to using myisam as storage-engine
- fix ownership in filemanager

### 6.5.1.a-2014012702.mr (Mon Jan 27 2014)

- back to use tcp/ip instead sock

### 6.5.1.a-2014012701.mr (Mon Jan 27 2014)

- fix select-all in dns/mysql list
- fix docroot
- fix fastcgi (add ide-timeout)
- fix clearcache
- make update script as the same as cleanup
- fix nsd tpl
- use sock instead tcp/ip to access mysql in panel

### 6.5.1.a-2014011001.mr (Fri Jan 10 2014)

- fix mysql-aio issue in openvz
- add disable-mysql-aio script
- mod how-to-install.txt for additional step when update from Kloxo 6.1.12
- no add certain param in sysctl.conf if openvz

### 6.5.0.f-2014010701.mr (Tue Jan 07 2014)

- fix install problem in openvz (wrong detect centos version)
- also remove exim in convert-to-qmailtoaster
- add try-catch in default_index.php

### 6.5.1.a-2014010301.mr (Fri Jan 03 2014)

- mod again ionice (become not using '-n')
- fix hiawatha for proxy (404 and 504 error)

### 6.5.1.a-2014010101.mr (Wed Jan 01 2014)

- change ionice value
- detect hiawatha as web server when running restart-web/-all
- fix try-cache process in appear
- fix logic for nowrap in list table

### 6.5.1.a-2013122602.mr (Thu Dec 26 2013)

- fix hiawatha service after hiawatha update

### 6.5.1.a-2013122601.mr (Thu Dec 26 2013)

- fix mysql conflict because wrong detect centos 6
- fix web config for disable domain
- fix clearcache logic
- fix appear if restore from previous version

### 6.5.1.a-2013122002.mr (Fri Dec 20 2013)

- fix wrong logic of lxphp detect

### 6.5.1.a-2013122001.mr (Fri Dec 20 2013)

- add keyword text for updateall message and adjusment updateallWarningfunction js
- fix/mod certain infomsg
- change submit naming from frm_change to frm_button/frm_button_all and add frm_change hidden input
- add id for hidden input tags beside name
- fix all_client appear
- add warning to need add 'innodb_use_native_aio=0' in /etc/my.cnf to update to mysql to 5.5 if running cleanup
- cleanup process also fix if lxphp exist
- reupload abstract_012.jpg
- mod certain text in messagelib.php

### 6.5.1.a-2013121703.mr (Tue Dec 17 2013)

- fix install and cleanup related to mratwork.repo

### 6.5.1.a-2013121702.mr (Tue Dec 17 2013)

- fix logic for custom php-fpm in nginx

### 6.5.1.a-2013121701.mr (Mon Dec 16 2013)

- add/mod certain keyword/message
- fix 'webmail system default'
- mod message box (remove image)
- fix login page if 'session timeout' state

### 6.5.1.a-2013121603.mr (Mon Dec 16 2013)

- fix security bug for php-fpm (add open_basedir)
- mod php-fpm open_basedir

### 6.5.1.a-2013121601.mr (Mon Dec 16 2013)

- change kloxo-mr.repo to mratwork.repo via rpm and sdjustment in install and cleanup script
- change 'processed logs' to 'client processed logs' and 'stats configuration' to 'domain processed logs'
- fix error in debug file if 'property' not exist

### 6.5.1.a-2013121402.mr (Sat Dec 14 2013)

- fix tree appear in feather skin
- fix infomsg in 'Feather' skin
- fix certain infomsg
- remove useless code in display
- fix appear if no infomsg
- fix link in show
- split %client% to %client% and %loginas% in infomsg
- add certain infomsgs

### 6.5.1.a-2013121104.mr (Wed Dec 11 2013)

- no permit if docroot with '..'
- change colors for version in login page
- beside when add domain, validate docroot also in 'docroot update' and 'redirect docroot'

### 6.5.1.a-2013121102.mr (Wed Dec 11 2013)

- change to use jcterm instead sshterm-template for ssh access

### 6.5.1.a-2013120904.mr (Mon Dec 09 2013)

- fix spamdyke disable/enable (need update qmail-toaster also)
- fix tls issue in smtp
- update panel port also create .ssl.port .nonssl.port files in /home/kloxo/httpd/cp
- port in cp also change if panel port change

### 6.5.1.a-2013120901.mr (Mon Dec 09 2013)

- use 'post' instead 'get' if form have enctype
- change certain text (like 'show') to keywords
- fix htmledit appear and change height from 200 to 500
- switch to apache also install all necessary module (fix mod_fastcgi issue)
- prepare to change pure-ftp service from xinetd to init
- fix ie8 issue (possible)
- also change 'edit' beside 'html_edit' from 600 to 900px
- fix simplicity skin in IE8
- mod/add certain infomsg
- prepare qmail stmp run (but still include in qmail rpm)
- fix pagenum/pagesize list

### 6.5.1.a-2013120601.mr (Fri Dec 06 2013)

- update running cleanup-nokloxorestart instead cleanup
- add remark in messagelib.php for 'customize' var
- back to use action var instead get in form except for pagenum/pagesize in list

### 6.5.1.a-2013120503.mr (Thu Dec 05 2013)

- change 'maxuploadsize' in kloxo-hiawatha from 100 to 2000 (MB)
- fix/mod 'simplicity' menu
- help/infomsg now able to use full html tags (like ul/ol/p)
- fix/mod space in certain list
- fix dbadmin and skeleton reference
- mod certain infomsg with rich html (unfinish jobs)
- mod width to wrap percentage (from 100 to 25)
- finishing reformat help messages (some messages still 'No information')
- fix messagelib.php
- combine add and list for ipaddress and adjustment menu

### 6.5.1.a-2013120301.mr (Tue Dec 03 2013)

- note inside hiawatha.conf.base where able upload until 2GB if using hiawatha-9.3-2+

### 6.5.1.a-2013120201.mr (Mon Dec 02 2013)

- move html.php from lib to theme
- add findindexfile in kloxo-hiawatha
- fix menu related to login-as-cancel
- fix sitepreview (also hn_urlrewrite) related to access php file directly
- fix hiawatha default.conf.tpl
- add getDescription function beside get getKeyword
- mod 'Comments' to without textarea
- combine resourplan 'information' and 'account on plan'
- fix/move infomsg for resourceplan
- add squid.conf (missing in previous)

### 6.5.1.a-2013112902.mr (Fri Nov 29 2013)

- fix menu (wrong file 'version')

### 6.5.1.a-2013112901.mr (Fri Nov 29 2013)

- fix hiawatha config if site access with '?s=a'
- translate certain text messages
- reversedns only able access by admin
- delete certain useless files
- fix/mod port checking
- disable licensecheck
- menu appear tree 'style' when admin/reseller access their customer

### 6.5.1.a-2013112305.mr (Sat Nov 23 2013)

- fix update from panel (just enough running cleanup)
- mod 'ionice' from '-c3' (idle) to '-c2' (best-effort)

### 6.5.1.a-2013112304.mr (Sat Nov 23 2013)

- mod hiawatha log format to extended (the same as apache log format)
- fix getContent in ffilelib.php
- better appear list in weblastvisit
- fix toggleVisibilityByClass in lxa.js
- fix branch list functions
- mod to not appear 'Consumed Login' when select 'Login As'
- fix/mod menu and buttons
- fix naming js function (to toggleVisibilityById)
- add 'click here' for 'help' and 'logout'
- add 'Login As (Cancel)' in menu
- remove 'Home' in 'Backup/Restore' and 'Update' title

### 6.5.1.a-2013112201.mr (Fri Nov 22 2013)

- fix menu link in 'simplicity' skin (to using 'real link')
- fix ownwerahip in filemanager
- add php55u branch
- remove all '__m_message_pre' in add/update form and infomsg appear depend on variable in messagelib.php
- remove commonmessagelib.php because useless
- fix getRpmVersionViaYum
- mod toggleVisibility to make possible display all infomsg in 1 page

### 6.5.1.a-2013111901.mr (Tue Nov 19 2013)

- fix related to forcedeletedserver
- run cleanup when click update (that mean update kloxomr)
- better info in 'update home' and fix installed/check-update rpm
- using text instead image for 'mail disk usage'
- add domain from commandline no need 'domain owned' approve

### 6.5.1.a-2013111801.mr (Mon Nov 18 2013)

- fix too small font in certain pages
- make more space in drop menu
- set index.php in menu file only able access by display.php
- fix menu in 'simplicity' skin

### 6.5.1.a-2013111601.mr (Sat Nov 16 2013)

- menu in 'simplicity' skin ready to multi-languages
- fix/mod many aspects related to better appearance

### 6.5.1.a-2013111202.mr (Tue Nov 12 2013)

- set 'simplicity' tab slight bigger
- 'password' and 'server roles' tab always appear in 'server'
- change 'symbol' char to char number < 256 (make compatible for pc without unicode font)
- set 'block title' to centered (simple solution for weird certain pages like 'server roles' page)
- merge/reorganize buttons 'groups' (example: merge 'domain' and 'domain adm'/'administer' to 'domain')
- remove 'postmaster@...' from mailaccount in title because always postmaster from first domain
- change 'config' to 'configure' and 'config ...' to '... configure' in title

### 6.5.1.a-2013111002.mr (Sun Nov 10 2013)

- fix appearance php warning
- set isDefaultSkin() to 'simplicity'
- fix 'select folder' in 'ftp user'
- fix 'custombutton'
- fix set_login_skin_to_feather()
- remove unwanted files
- after running upcp always restart-all
- fix/mod install process (no need 'yes' answer
- auto restart-all)
-

### 6.5.1.a-2013110902.mr (Sat Nov 09 2013)

- add 'reverse-font' (metro-like) for 'button type' of 'appearence'
- fix apache issue when enable secondary php

### 6.5.1.a-2013110802.mr (Fri Nov 08 2013)

- set 3 options for 'show directions'
- back to use addondomainlib.php (combine list + add parked + add redirect still not work)
- mod 'vertical 2' and set higher width and height for buttons show (need because set bigger font)
- install already set /tmp to permissions 1777
- also check already hosted when add domain
- fix/mod web server config (permalink)
- fix error 500 issue in apache (not able set 'cgi.rfc2616_headers = 1' in php.ini)
- fix/mod add domain (add 'domain owner' option)
- set to hidden of 'infomsg' in 'feather' skin (also appear if mouseover to 'help' button)
- add 'button type' in appearance (button using font or image)
- fix display where height problem in content when using div-div and change to div-table

### 6.5.1.a-2013110502.mr (Tue Nov 05 2013)

- fix wrong style.css
- set font-size to bigger (9pt instead 8pt)
- change 'PHPMyAdmin' to 'SQL Manager' (prepare to using sqlite format for database)
- add dragdivscroll.js (horizontal mouse scrolling for buttons)
- remove graph column (trouble with bigger font; unnecessary)

### 6.5.1.a-2013110404.mr (Mon Nov 04 2013)

- fix space between part of content
- forgot submit for 'show direction'

### 6.5.1.a-2013110402.mr (Mon Nov 04 2013)

- fix/mod display
- add 'show direction' for appear where skin able select for 'buttons' direction

### 6.5.1.a-2013110302.mr (Sun Nov 03 2013)

- horizontal buttons flow instead vertical in 'simplicity' skin
- enable/disable compressing in php.ini
- to make sure, also install traceroute and util-linux (ionice)

### 6.5.1.a-2013110202.mr (Sat Nov 02 2013)

- fix infomsg issue in 'webserver config'
- make more bigger font size
- fix updateform in appearancelib.php
- add desc_addondomain_l declare (to prevent no object warning)
- fix many issue related to theme
- set drop menu to 'centered', fix width drop menu to 500px
- fix menu for resolution 1024
- add/mode background images
- fix background selected
- add brightness color function (use in the future)
- 'feather' skin still use icon images but 'simplicity' use symbol chars
- set smaller box in 'simplicity' skin
- embeded menu instead div caller
- delete gray version of background images
- no separate breadcomb with tab and content
- no need execute 'lxLoadBody()' js in 'simplicity' skin

### 6.5.1.a-2013103005.mr (Wed Oct 30 2013)

- change path from $os.inc to rhel.inc
- fix path for webcache driver
- change 'Domain Adm' to 'Administer' text
- change certain image icon to char font
- fix resourceclass width table
- fix infomsg issue in 'webserver config'
- make more bigger font size

### 6.5.1.a-2013103003.mr (Wed Oct 30 2013)

- remove 'add form' in each 'all' list
- fix background logic
- better confirm page (with background color)

### 6.5.1.a-2013103001.mr (Wed Oct 30 2013)

- fix path for drivers which move at previous
- res and naming adjustment for background

### 6.5.1.a-2013102903.mr (Tue Oct 29 2013)

- disable installapp update in scavenge
- move files related to driver
- fix sitepreview

### 6.5.1.a-2013102802.mr (Mon Oct 28 2013)

- fix dns config (wrong code submit)
- fix fixdns and fixweb

### 6.5.1.a-2013102801.mr (Mon Oct 28 2013)

- just fix html, css and js code for display
- add 'fs.aio-max-nr' and increase 'fs.file-max' value in install process
- make shadow effect for certain part

### 6.5.1.a-2013102601.mr (Sat Oct 26 2013)

- try other dropdown menu (like it)
- remove unwanted files
- fix some 'bad' display

### 6.5.1.a-2013102502.mr (Fri Oct 25 2013)

- move and rename header, bottom and lpanel.php to frame_ prefix and move to theme dir
- move functions related to lpanel from htmllib.php to frame_lpanel.php
- remove unwanted files/dirs
- add missing lst files
- fix packer process

### 6.5.1.a-2013102402.mr (Thu Oct 24 2013)

- restructure image/button/icon dirs
- remove unwanted files/functions
- fix nsd issue when select without domain exist
- mod os_create_default_slave_driver_db()
- fix mailaccount display
- remove content of login dir

### 6.5.1.a-2013102203.mr (Tue Oct 22 2013)

- resubmit install script because wrong 'version'
- automatically change to 'simplicity' when still using 'default' skin
- 'simplicity' as default with background image
- fix password dialog for login with 'default' password (like 'admin')
- change (add/remove) background images

### 6.5.1.a-2013102201.mr (Tue Oct 22 2013)

- remove 'default' skin but add color to feather
- move certain functions from lib.php to htmllib.php
- change default color from 'b1c0f0' to 'EFE8E0'
- restructure skin dirs
- reduce background image with to 1600 px

### 6.5.1.a-2013102103.mr (Mon Oct 21 2013)

- disable web and dns installed by default
- mod setup.sh/installer.sh to handle 3x running installer.php when kloxo database fail to created

### 6.5.1.a-2013102102.mr (Mon Oct 21 2013)

- fix skeleton.zip (previous with un-transparent logo)
- fix to make infomsg to center in feather/default skin
- move 'show/hide' button from tab to header
- fix js script for show/hide toggle

### 6.5.1.a-2013102101.mr (Mon Oct 21 2013)

- 'simplicity' near final
- adjustment for 'default' and 'feather' skin
- convert some table-base to div-base html codes (not final work)
- add base extjs script (importance for frame-based skin)

### 6.5.1.a-2013101901.mr (Sat Oct 19 2013)

- move all files in panel dir to theme dir amd adjustment link
- 'message inbox' as 'help' in simplicity psnel
- delete fckeditor _samples
- 'simplicity' skin able change background
- prepare for re-write display code

### 6.5.1.a-2013101703.mr (Thu Oct 17 2013)

- fix for simplicity panel (no need frame and no thick/thin skin)
- fix default slavedb driver

### 6.5.1.a-2013101701.mr (Thu Oct 17 2013)

- restructure panel dirs
- introduce simplicity panel (based on thin feather but with css menu)
- remove unwanted files (related to panel display)
- use simplicity as 'default' panel

### 6.5.1.a-2013101501.mr (Tue Oct 15 2013)

- select simple skin automatically redirect to display.php (no need frame-base again)
- fix issue in 'default' skin
- fix many bugs in interface

### 6.5.1.a-2013101203.mr (Sat Oct 12 2013)

- rename all .phps to .php
- move htmllib to panel dir
- integrate extjs, yui-dropdown and fckeditor without source, example and docs files
- disable install kloxomr-addon

### 6.5.1.a-2013101201.mr (Sat Oct 12 2013)

- fix tmpfs detect logic
- fix program appear in 'switch programs'
- add cache grace in varnish
- mod comment in hiawatha config
- add squid driver
- add message for hiawatha microcache
- restructuring files for drivers and lib categories (prepare for easy add driver)
- most link in panel already 'right-click' to open (one step to new theme)
- remove unused files

### 6.5.1.a-2013100801.mr (Tue Oct 08 2013)

- fix sysinfo to adopt hiawatha info
- fix hiawatha config for reverse-proxy
- disable 'microcache' in lighttpd because no different
- fix/optimize lighttpd.conf settting
- also enable 'microcache' nginx-proxy
- change remap.config setting for trafficserver
- set 'default value' for web/webcache/dns/spam driver because add 'none' driver
- fix web config for sure using 'php-fpm_event' as 'default' phptype
- warning in installer when '/tmp' set as 'tmpfs' (trouble with backup/restore)
- set max ip connection to 25 (like nginx config)
- use 'boosted' config for varnish
- prepare for squid web cache

### 6.5.1.a-2013100403.mr (Fri Oct 04 2013)

- use trafficserver 4 config for version 3 and 4 because running well
- fix webmail logic
- introduce 'none' driver for web, dns and spam (as the same as webcache model)

### 6.5.1.a-2013100302.mr (Wed Oct 02 2013)

- back to add .db_schema which importance for panel display
- all web server include 'generic' permalink
- change user as 'ats' instead 'root' for trafficserver
- enable 'debug' for trafficserver
- no include php imagick for install
- fix copy config for 'nsd' dns server
- restart qmail with 'stop
- sleep 2 start' instead 'restart'
- add missing file (db_schema)

### 6.5.1.a-2013100204.mr (Wed Oct 02 2013)

- fix httpd template if web cache enable
- fix web cache for 'none'
- fix 'userdir' logic in template of httpd
- fix dns and weh config
- change ats to root for minimize permissions issue for trafficserver
- remove 'debug' file
- fix 'default' web server in installing process
- fix 'default' configs copy for webcache server

### 6.5.1.a-2013093003.mr (Mon Sep 30 2013)

- fix varnish init and copy config
- mod mysql-convert.php

### 6.5.1.a-2013093001.mr (Mon Sep 30 2013)

- ready for testing varnish cache server
- add new 'class' as 'webcache'
- make simple 'removeOtherDrivers' function
- delete old config when switch web/dns to
- remove unused files
- restart qmail using 'restart' instead 'stop' and 'start'

### 6.5.1.a-2013092802.mr (Sat Sep 28 2013)

- hiawatha already work for redirect
- set enable gzip and fix urltoolkit setting for hiawatha
- using 'qmailcrl restart' instead 'qmailctl stop qmailctl start'
- make simple logic for webmail in web config
- fix webmail config (thanks for hiawatha with their strict path)
- change 'insert into' to 'insert ignore into' for sql to guarantee latest data
- remove 'old style' sql data and function

### 6.5.1.a-2013092606.mr (Thu Sep 26 2013)

- change 'please contact'
- mod to web config only need init.conf (ssl, default and cp) and each domain config
- fix 'text record' for pdns (thanks SpaceDust)
- change apache ip to 127.0.0.1 in proxy 'mode'
- cleanup also remove /home//conf/webmails
- fix dns uninstall
- use qmailctl instead service for stop/start qmail
- fix 'defaults' dir content remove
- remove unused function in web__lib.php and fix related to it
- fix lighttpd for running with 'new' config model

### 6.5.1.a-2013092403.mr (Tue Sep 24 2013)

- better changedriver message
- disable qmail restart inside tmpupdatecleanup.php
- disable robots to cp, disable, default and webmail dirs
- ready testing for hiawatha and hiawatha-proxy (still unfinish work)
- reformat dns config tpl
- fix webmail and cp for hiawatha (move from 'generic' to each 'domain'config)
- emulate index files and permalink in urltoolkit for hiawatha

### 6.5.1.a-2013092302.mr (Mon Sep 23 2013)

- add for 'lost' replace_between function
- change to better dnsnotify.pl and mod dnsnotify
- mod dnsnotify.pl for detail info
- add replace for maradns
- add backend-bind for pdns
- add error log for php.ini.tpl
- use faatcgi+php-fpm instead ruid2 as default httpd in install process

### 6.5.1.a-2013092202.mr (Sun Sep 22 2013)

- optimize config and process ('reload/restart' process) for dns
- fix restart-all (have a problem when php-fpm restart before web server restart)
- mod pdns.sql
- fix wrong djbdns tpl
- fix maradns tpl (but change to ip4_bind_address script still not ready)
- maradns.init include change default 'ip4_bind_address' to hostname ip
- add 'notify=yes' in bind
- mod to small dns config because using 'origin' based
- fix action login in update dns config process
- remove 'srv record' in djbdns
- mod mararc for accept modified for xfr and zone list
- fix issue in dns switch (need stop server before unistall; found issue in maradns)
- back to use read db instead call var for__var_mmaillist in web__lib.php
- fix missing parameter in createListNlist
- add list.transfered.conf.tpl for maradns
- fix list.master.conf.tpl in maradns
- fix uninstall dns (wrong var)
- add 'dnsnotify' in maradns and djbdns with external script
- add supermasters process in pdns
- mod README.md

### 6.5.1.a-2013091903.mr (Thu Sep 19 2013)

- fix nsd (add dns_nsdlib.php and disable include for slave conf)

### 6.5.1.a-2013091902.mr (Thu Sep 19 2013)

- add double quote for 'txt record' of pdns
- fix issue fail install pdns-backend-mysql after install pdns
- mod pdns.sql for optimize to innodb
- maradns ready
- add and use setRpmRemovedViaYum for dns drivers
- disable process xfr on maradns
- fix maradns domain config
- try to use '0.0.0.0' for maradns ip bind
- prepare for NSD dns server
- convert all 'cname record' to 'a record' in dns server config
- mod watchdog list
- add 'nsd' in 'reserved', 'dns' and 'driver' list
- set for 'nsd' dns server
- fix latest nginx (cache dir)
- still using '0.0.0.0' for 'nsd' notify/provide-xfr

### 6.5.1.a-2013091704.mr (Tue Sep 17 2013)

- add convert to utf8 charset for mysql-convert
- automatically add 'SPF record' beside 'A record' for 'SPF'
- fix pdns for addon-domain
- fix warning when spam switch

### 6.5.1.a-2013091702.mr (Tue Sep 17 2013)

- fix detect primary ip for hostname
- disable dnssec for powerdns because still not work
- add 'create database' in pdns.sql
- install pdns also install pdns-backend-mysql
- fix calling from powerdns to pdns

### 6.5.1.a-2013091603.mr (Mon Sep 16 2013)

- move fix/mod '/etc/hosts' from setup.ah/installer.sh to lib.php
- remove fixmail-all in tmpupdatecleanup.php (because duplicate)
- create powerdns database ready if switch to powerdns or running cleanup
- fix hiawatha process in cleanup
- change name driver from 'powerdns' to 'pdns'
- fix ugly button and other not 'standard' html tag in 'default' theme
- fix isPhpModuleInstalled() var
- include new features (no exists in 6.5.0)

### 6.5.0.f-2013091202.mr (Thu Sep 12 2013)

- add option server/client/domain in fixdomainkey
- fix installer process (conflict between mysql from centalt and ius)
- fix php-fpm tpl for deprecated commenting
- fix html code for display/theme

### 6.5.0.f-2013091101.mr (Wed Sep 11 2013)

- change procedural to object style of MySQLi API
- fix link in langfunctionlib.php

### 6.5.0.f-2013090904.mr (Mon Sep 09 2013)

- fix some display/theme
- add testing for reset-mysql-root-password
- fix insert 'universal' hostname
- install mariadb if exist instead mysql55
- fix installer (because php52s must install after mysql55 and php53u)
- fix getRpmVersion()

### 6.5.0.f-2013090903.mr (Mon Sep 09 2013)

- fix some bug on installer.php
- change install mysql55 instead mysql (from centos) because have trouble with MySQLi API in 5.0.x
- fix for php52s (add install net-snmp)
- adjutment installer.sh to match with setup.sh

### 6.5.0.f-2013090901.mr (Mon Sep 09 2013)

- change 'Kloxo' title to 'Kloxo-MR'
- beside 'fs.file-max' also add others (like 'vm.swappiness') to optimize
- instead warning for 'hostname', add 'universal' hostname to '/etc/hosts' in install process

### 6.5.0.f-2013090801.mr (Sun Sep 08 2013)

- mod service list
- mod reset-mysql-root-password
- remove 'javascript:' except for 'href'
- fix select all for client list
- add another var to sysctl.conf (for minimize buffers and cached memory)

### 6.5.0.f-2013090704.mr (Sat Sep 07 2013)

- fix install process (especially in centos 5)
- fix qmail-toaster initial
- fix/better update process
- chkconfig off for php-fpm when install (because using ruid2 as 'default' php-type)

### 6.5.0.f-2013090702.mr (Sat Sep 07 2013)

- fix identify hostname (use 'hostname' instead 'hostname -f')
- remove unused code
- fix updatelib.php for install process
- fix for ruid2 (need php.conf) for 'default' php-type

### 6.5.0.f-2013090701.mr (Sat Sep 07 2013)

- move hostname checking from installer.php to setup.sh/installer.sh

### 6.5.0.f-2013090607.mr (Fri Sep 06 2013)

- add function for checking hostname and stop install process if not qualified
- remove libmhash to checking
- no need check old.program.pem
- fix/better lxphp.exe checking when running upcp
- add '-y' to force to 'reinstall'
- fix setup.sh/installer.sh/upcp script for install process

### 6.5.0.f-2013090603.mr (Fri Sep 06 2013)

- add parse_ini.inc (prepare for kloxo config in ini format)
- fix 'default' default.conf
- mod fixdomainkey execute dns subaction for domain instead full_update
- change listen ip-port to socket in php-fpm.conf (for php 5.2)
- fix upcp script for fresh install
- fix installer.php for 'default' web using ruid2 (need enable php.conf)

### 6.5.0.f-2013090302.mr (Mon Sep 02 2013)

- make install setup (run 'sh /script/upcp' instead '/usr/local/lxlabs/kloxo/install'
- fix mysqli_query for webmail database
- better reset-mysql-root password and mysql-convert code

### 6.5.0.f-2013090302.mr (Mon Sep 02 2013)

- testing for 6.5.1.a
- convert mysql to mysqli API in Kloxo-MR code
- fix display/theme
- add/mod hash/bucket because nginx not started in certain conditions
- change lxphp to php52s in desclib.php

### 6.5.0.f-2013090201.mr (Mon Sep 02 2013)

- fix display/theme (restore no domain list; wrong button title)
- add/mod hash/bucket for nginx.conf (nginx not start in certain conditions)
- add changelog content of first release 6.5.0.f

### 6.5.0.f-2013082704.mr (Tue Aug 27 2013)

- taken code from 6.5.1.a (but maradns and hiawatha still disable)
- convert cname to a record for djbdns (because cname work work)
- fix error/warning for debug panel
- fix htmllib
- fix hiawatha service not execute after cleanup
- fix old link to /script
- fix web drivers list
- add hiawatha, maradns and powerdns in update services in cleanup

### 6.5.0.f-2013082602.mr (Mon Aug 26 2013)

- fix html tags especially for deprecated tag like 

### 6.5.0.f-2013082601.mr (Mon Aug 26 2013)

- make fixdns faster (synchronize and allowed_transfer change to per-client)
- add 'accept-charset="utf-8"' for


### 6.5.0.f-2013082401.mr (Sat Aug 24 2013)

- fix panel port (back to 7778/7777 from 37778/37777)

### 6.5.0.f-2013082302.mr (Fri Aug 23 2013)

- fix clientmail.php (missing ';')

### 6.5.0.f-2013082301.mr (Thu Aug 22 2013)

- set root:root to installatron symlink
- add graph for load average
- move files inside script to pscript
- fix readsmtpLog for read smtp.log to maillog
- fix mail forward with disable detect mail account
- get client list from db directly instead from client object

### 6.5.0.f-2013082201.mr (Thu Aug 22 2013)

- fix dns config issue (update config not work)
- mod/change fix-all (include fixftpuser instead fixftp)
- add process for delete /etc/pure-ftpd/pureftpd.passwd.tmp (unfinish loop for cleanup)

### 6.5.0.f-2013082102.mr (Wed Aug 21 2013)

- fix dns config (make faster and no memory leak if running fixdns/cleanup)
- fix installatron-install script

### 6.5.0.f-2013082002.mr (Tue Aug 20 2013)

- fix mysql-to-mariadb bug
- better getRpmVersion
- use lib.php from dev but disable mariadb/powerdns/hiawatha initial
- mod suphp configs
- better apache tpl
- better getRpmVersionViaYum function

### 6.5.0.f-2013081801.mr (Sun Aug 18 2013)

- php*-gd, -bcmath and -pgsql also detect when running cleanup
- all languages including in core (still compile separately)

### 6.5.0.f-2013081701.mr (Sat Aug 17 2013)

- fix/add packages listing on 'services' and 'component list'
- make cp address as additional panel

### 6.5.0.f-2013081601.mr (Fri Aug 16 2013)

- fix detect ftp for lxguard (because think as using syslog but possible using rsyslog)
- fix restart scripts (because old script not work for other then english
- add php*-gd and php*-pdo (because repo not make as 'default') as default ext
- add config for microcache for nginx

### 6.5.0.f-2013081402.mr (Wed Aug 14 2013)

- update customize fastcgi_param for ngix
- add init file checking for dns initial
- no convert cname to a record for local domain
- fix remove lxphp.exe for lxphp (because change to php52s)

### 6.5.0.f-2013081304.mr (Tue Aug 13 2013)

- fix error 500 on kloxo-hiawatha (back to use TimeForCGI)

### 6.5.0.f-2013081303.mr (Tue Aug 13 2013)

- fix upload issue (increasing MaxRequestSize TimeForRequest and MaxKeepAlive)
- fix/mod restart scripts

### 6.5.0.f-2013081208.mr (Mon Aug 12 2013)

- add allowed-transfer script for dns server (make possible dns server as 'master')
- fix some minor bugs for dns template
- fix some minor bugs for install process
- mod/add restart/clearcache script

### 6.5.0.f-2013080701.mr (Wed Aug 07 2013)

- fix bind dns config (bind work now like djbsns)

### 6.5.0.f-2013080605.mr (Tue Aug 06 2013)

- simple execute for djbdns list.master.conf.tpl
- fix 'make' execute for axfrdns of djbdns
- fix no conf directory issues when using djbdns (cleanup will be create this dirs)
- fix bind domains.conf.tpl (problem with ns declare)
- add 'make' install when install kloxo (djbdns need it)
- add 'sock' dir for php-fpm socket when running cleanup

### 6.5.0.f-2013080602.mr (Tue Aug 06 2013)

- fix dns config especially 'server alias' issue
- switch to djbns also execute djbdns 'setup'

### 6.5.0.f-2013080601.mr (Tue Aug 06 2013)

- bugfix for dns config (wrong ns and cname)
- bugfix for access panel via https/7777
- mod sysctl.conf when running cleanup

### 6.5.0.f-2013080502.mr (Mon Aug 05 2013)

- based on until 6.5.1.a-2013080502
- change timestamp from 20130318xx to real timestamp release
- change lxphp + lxlighttpd to php52s + hiawatha (the first cp using it!)
- template-based for dns server (bind and djbdns)
- bugfix for add ip
- remove unwanted files (related to os detect/specific)
- because using hiawatha, socket error already fixed (related to php-cli wrapper)
- using closeinput instead closeallinput (no different effect found)
- remove unwanted skin images
- change /restart or /backendrestart to /load-wrapper (related to socket error issue)
- change helpurl from forum.lxcenter.org to forum.mr ----------atwork.com
- exclude bind from centalt because something trouble when using it
- add error page for panel
- remove lxphp-module-install and change to php5Xs-extension-install
- add/change set-secondary-php script

### 6.5.0.f-2013031828.mr (Thu Jul 11 2013)

- based on until 6.5.1.a-2013071102
- disable mysql51 and mysql55 from ius (make conflict)
- improve mysql-convert and mysql-optimize
- modified kloxo-mr.repo
- make setup process until 3x if kloxo database not created (normally enough 1-2x)

### 6.5.0.f-2013031827.mr (Wed Jul 10 2013)

- based on until 6.5.1.a-2013071001
- disable mysql from ius repo (make conflict) when install process
- change kloxo-mr.repo related to disable mysql from ius
- mysql-convert script will convert all database for storage-engine target
- move certain parameter of nginx from 'location /' to 'server'
- disable 'php_admin_value[memory_limit]' on php-fpm template
- restart will be execute start if not running for qmail service
- rename custom qmail run/log run of qmail-toaster
- increase value of TopCountries and others for webalizer
- fix web config, expecially for add/delete domain.

### 6.5.0.f-2013031826.mr (Thu Jun 27 2013)

- based on until 6.5.1.a-2013062801
- fix install process (need running setup.sh 2x in certain condition)
- fix wrong message for afterlogic when running cleanup/fixwebmail/fixmail-all
- back to use 'wget' instead 'wget -r' in how-to-install
- disable mirror for repo and just using for emergency

### 6.5.0.f-2013031825.mr (Thu Jun 27 2013)

- based on until 6.5.1.a-2013062701
- remove double install process for mysql and httpd
- fix conflict of mysql install
- set php53u and mysql51/mysql55 as default install
- fix telaen config copy
- fix webmail detect

### 6.5.0.f-2013031824.mr (Wed Jun 26 2013)

- based on until 6.5.1.a-2013062602
- fix restore message
- prepare for qmail-toaster custom-based run/log run

### 6.5.0.f-2013031823.mr (Thu Jun 20 2013)

- based on until 6.5.1.a-2013062301
- restart kloxo if found 'server 7779' not connected
- move maillog from /var/log/kloxo to /var/log
- remove smtp.log and courier.log
- dual log (multilog and splogger) for qmail-toaster
- remove unwanted files (espacially related to qmail-toaster)
- bug fix for reset-mysql-root-password script
- change to apache:apache for dirprotect dir
- fix segfault when install
- change kloxo sql without engine=myisam

### 6.5.0.f-2013031822.mr (Sun Jun 16 2013)

- fix clearcache script
- remove certain qmail config fix (becuase logic and code move to rpm)

### 6.5.0.f-2013031821.mr (Sat Jun 15 2013)

- based on until 6.5.1.a-2013061501
- back to disable mariadb from centalt (still have a problem install Kloxo-MR on centos 6 32bit)
- fix diprotect path for apache
- not need softlimit change (already set inside qmail-toaster)
- fix clearcache script for openvz host
- fix function.sh and lxphpcli.sh (add exec)
- back to use restart function instead stop and start for restart

### 6.5.0.f-2013031820.mr (Tue Jun 11 2013)

- based on until 6.5.1.a-2013061101
- install without asking 'master/slave' (always as 'master'; run make-slave for change to slave)
- more info backup/restore
- mod smtp-ssl_run for rblsmtpd/blacklist
- remove double process for softlimit change
- fix issue when install on openvz host
- enable gateway when add ip
- modified nginx config for dualstack ip (ipv4+ipv6)

### 6.5.0.f-2013031819.mr (Tue Jun 04 2013)

- based on until 6.5.1.a-2013060402
- fix fixmail-all ('cp' weird behaviour for copy dir)
- add info in sysinfo

### 6.5.0.f-2013031818.mr (Mon Jun 03 2013)

- based on until 6.5.1.a-2013060301
- fix web config for www-redirect and wildcards
- create mail account automatically create subscribe folders
- fix smtp issue
- possible customize qmail run script

### 6.5.0.f-2013031817.mr (Fri May 31 2013)

- fix restart-services
- fix userlist with exist checking
- fix mail config (smtp and submission already work!)
- remove for exlude mariadb from centalt repo
- based on until 6.5.1.a-2013053102

### 6.5.0.f-2013031816.mr (Sun May 26 2013)

- fix qmail init
- based on until 6.5.1.a-2013052101

### 6.5.0.f-2013031815.mr (Sun May 19 2013)

- fix kloxo database path
- based on until 6.5.1.a-2013051901

### 6.5.0.f-2013031814.mr (Sat May 18 2013)

- fix install process and reset password from ssh
- fix wildcards for website
- based on until 6.5.1.a-2013051804

### 6.5.0.f-2013031813.mr (Thu May 16 2013)

- fix sh permission to 755
- fix www redirect
- make simple awstats link
- add mariadb in mysql branch
- disable mariadb from centalt repo (conflict when install)
- based on 6.5.1.a-2013050502 and 6.5.1.a-2013051601

### 6.5.0.f-2013031812.mr (Sun May 05 2013)

- update suphp config (fix for possible security issue) and remove delete spamassassin dirs
- based on 6.5.1.a-2013050501 and 6.5.1.a-2013050502

### 6.5.0.f-2013031811.mr (Fri Apr 26 2013)

- fix packer.sh (remove lang except en-us)
- use ionice for du
- based on 6.5.1.a-2013042601 and 6.5.1.a-2013042602

### 6.5.0.f-2013031810.mr (Sun Apr 21 2013)

- fix some script based-on 6.5.1.a-2013042001 and 6.5.1.a-2013042101

### 6.5.0.f-2013031809.mr (Mon Apr 08 2013)

- fix some script based-on 6.5.1.a-2013040801

### 6.5.0.f-2013031808.mr (Sat Mar 30 2013)

- fix install issue on openvz

### 6.5.0.f-2013031807.mr (Wed Mar 27 2013)

- fix traffic issue and installer.sh/installer.php
- add some scripts

### 6.5.0.f-2013031806.mr (Mon Mar 25 2013)

- no need cleanup on installer/setup also change mysqli to mysql on reset password

### 6.5.0.f-2013031805.mr (Mon Mar 25 2013)

- no need running full installer.sh twice just function step2 if running setup.sh

### 6.5.0.f-2013031804.mr (Mon Mar 25 2013)

- fix bugs relate to install/setup

### 6.5.0.f-2013031803.mr (Sat Mar 23 2013)

- remove php modules (except php-pear) because conflict between centos and other repos

### 6.5.0.f-2013031802.mr (Sat Mar 23 2013)

- fix critical bug (don't install php-mysqli on install/setup process)

### 6.5.0.f-2013031801.mr (Mon Mar 18 2013)

- first release of Kloxo-MR
- FIX - Security bug (possible sql-injection on login and switch 'safe' and 'unsafe' mode)
- FIX - Backup and restore (no worry about 'could_not_zip' and 'could_not_unzip')
- FIX - No password prompt when install spamdyke
- FIX - Add missing fetchmail when install
- FEATURE - Add Nginx, Nginx-proxy and Lighttpd-proxy
- FEATURE - Possible using different 'Php Branch' (for Php version 5.2, 5.3 and 5.4)
- FEATURE - Possible enable/disable 'Secondary Php' (using lxphp and suphp)
- FEATURE - More 'Php-type' (mod_php, suphp, fcgid and php-fpm) with different apache mpm
- FEATURE - Template-based web, php and php-fpm configs (use 'inline-php') and possible to customize
- FEATURE - Reverse DNS always appear
- FEATURE - Add select 'Ssl Key Bits' (2048, 1024 and 512) for 'Add Ssl Certificate'
- FEATURE - More logs on 'Log Manager'
- FEATURE - Enable logrotate
- FEATURE - Support for Centos 5 and 6 on 32bit or 64bit
- FEATURE - Possible install on Yum-based Linux OS (Fedora, ScientificLinux, CloudLinux and etc)
- FEATURE - Based-on multiple repo (Kloxo-MR owned, CentAlt, IUS, Epel and etc)
- FEATURE - Support different 'Mysql Branch' and MariaDB
- FEATURE - Add 'sysinfo' script to support purpose
- FEATURE - Add 'lxphp-module-install' script for installing module for lxphp
- FEATURE - Add and modified some scripts (convert-to-qmailtoaster, fix-qmail-assign, fixvpop and fixmail) for mail services
- FEATURE - Faster and better change mysql root password
- FEATURE - Add new webmail (afterlogic Webmail lite, T-Dah and Squirrelmail)
- FEATURE - Automatic add webmail when directory create inside /home/kloxo/httpd/webmail
- FEATURE - Change components to rpm format (addon, webmail, phpmyadmin and etc)
- FEATURE - Possible access FTP via ssl port
- FEATURE - Automatic install RKHunter and add log to 'Log Manager'
- CHANGE - Use qmail-toaster instead qmail-lxcenter (with script for convert)
- CHANGE - New interface for login and 'defaults' pages
- CHANGE - Use Kloxo-MR logo instead Kloxo logo
- CHANGE - Remove xcache, zend, ioncube and output compressed from 'Php Configs'
- CHANGE - Use php-fpm instead fastcgi or spawn-cgi for Lighttpd
- CHANGE - Use 'en-us' instead 'en' type for language
- CHANGE - Remove unwanted files and or code for windows os target
- CHANGE - Use '*' (wildcard) instead 'real' ip for web config and then no issue related to 'ip not found'
- CHANGE - Use 'apache:apache' instead 'lxlabs:lxlabs' ownership for '/home/kloxo/httpd' ('defaults' page')
- CHANGE - Use local data for 'Release Note' instead download
- CHANGE - Use tar.gz instead zip for compressing Kloxo-MR
- PATCH - bug fix for installer.sh (installer.sh for 'dev' step and yum install/update + setup.sh for final step)
- PATCH - remove php modules (except php-pear) because conflict between centos with other repos